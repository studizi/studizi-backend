const types = require('pg').types;
const moment = require('moment-timezone');
const tstzrangeParser = require('../utils/tstzrange.parser');
const TSTZRANGE_OID = 3910;
types.setTypeParser(TSTZRANGE_OID, tstzrangeParser.parseTstzrange);
const TIMESTAMP_OID = 1184;
const parseTimestamp = function (val) {
  return val ? moment(val).toISOString() : null;
};
types.setTypeParser(TIMESTAMP_OID, parseTimestamp);

const ARRAY_TSTZRANGE_OID = 3911;
const parseArrayTstzrange = function (val) {
  // eslint-disable-next-line no-useless-escape
  const outerRegex = /[\(\[]\\"[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}\+[0-9]{2}\\",\\"[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}\+[0-9]{2}\\"[\)\]]/g;
  const arrayTstzRange = val.match(outerRegex);

  const innerRegex = /:[0-9]{2}\+[0-9]{2}/g;

  for (let i = 0; i < arrayTstzRange.length; i++) {
    arrayTstzRange[i] = arrayTstzRange[i]
      .replaceAll(innerRegex, '')
      .replaceAll('\\"', '');
  }

  return arrayTstzRange;
};
types.setTypeParser(ARRAY_TSTZRANGE_OID, parseArrayTstzrange);

const NUMERIC_OID = 1700;
const parseNumeric = function (val) {
  return parseFloat(val);
};
types.setTypeParser(NUMERIC_OID, parseNumeric);
const knex = require('knex')({
  client: 'pg',
  connection: {
    host: process.env.POSTGRES_HOST,
    user: process.env.POSTGRES_USER,
    port: process.env.POSTGRES_PORT,
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DB
  }
});

module.exports = knex;
