/* eslint-disable camelcase */
const passport = require('passport');
const SHA256 = require('js-sha256');
const LocalStrategy = require('passport-local').Strategy;
const User = require('../database/models/user.model');
const { isAValidInstance } = require('../utils/schema.validator');
const loginSchema = require('../database/schemas/login.schema.json');
module.exports = function (app) {
  passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
  },
  async (email, password, done) => {
    if (!isAValidInstance({
      email: email,
      password: password
    }, loginSchema)) { return done(null, false); }
    const user_id = await User.getUserIdIfExist(email, SHA256(password));
    if (user_id == null) { return done(null, false); }
    const user = await User.getUser(user_id);
    if (!user.verified) { return done(null, false); }
    return done(null, user_id);
  }
  ));

  passport.serializeUser((user_id, done) => {
    return done(null, user_id);
  });
  passport.deserializeUser(async function (user_id, done) {
    const user = await User.getUser(
      user_id
    );
    return done(null, user);
  });
  app.use(passport.initialize());
  app.use(passport.session());

  return passport;
};
