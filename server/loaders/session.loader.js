const bodyParser = require('body-parser');
const session = require('express-session');

const options = {
  sessionSecret: 'TODO'
};

module.exports = function (app) {
  app.use(bodyParser.json());
  app.use(
    session({
      secret: options.sessionSecret,
      resave: false,
      saveUninitialized: false,
      cookie: { path: '/', httpOnly: true, maxAge: null }
    })
  );
};
