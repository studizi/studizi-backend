/* eslint-disable camelcase */
const CronJob = require('cron').CronJob;
const Offer = require('../database/models/offer.model');
const Meeting = require('../database/models/meeting.model');
const Stud = require('../database/models/stud.model');
const Discord = require('./discord.service');
const moment = require('moment');
// eslint-disable-next-line no-useless-escape
const tstzrangeRegex = /^([\[\(])(['\"])((((19|20)([2468][048]|[13579][26]|0[48])|2000)-02-29|((19|20)[0-9]{2}-(0[4678]|1[02])-(0[1-9]|[12][0-9]|30)|(19|20)[0-9]{2}-(0[1359]|11)-(0[1-9]|[12][0-9]|3[01])|(19|20)[0-9]{2}-02-(0[1-9]|1[0-9]|2[0-8])))\s([01][0-9]|2[0-3]):([012345][0-9]):([012345][0-9])[\+\-]([0-9]{2}))(['\"]),(['\"])((((19|20)([2468][048]|[13579][26]|0[48])|2000)-02-29|((19|20)[0-9]{2}-(0[4678]|1[02])-(0[1-9]|[12][0-9]|30)|(19|20)[0-9]{2}-(0[1359]|11)-(0[1-9]|[12][0-9]|3[01])|(19|20)[0-9]{2}-02-(0[1-9]|1[0-9]|2[0-8])))\s([01][0-9]|2[0-3]):([012345][0-9]):([012345][0-9])[\+\-]([0-9]{2}))(['\"])([\]\)])$/;

async function deleteExpiredMeeting () {
  console.log('Deleting expired channels');
  const offers = await Offer.getOffersWithScheduledStatus();
  if (!offers) {
    return;
  }
  const now = moment.utc();
  for (const offer of offers) {
    const meeting = await Meeting.getMeetingByOfferId(offer.offer_id);
    if (!meeting) {
      // todo
      continue;
    }
    const groups = offer.meeting_date.match(tstzrangeRegex);
    const end_time = moment.utc(groups[23]).format();
    if (now.diff(end_time) < 0) {
      continue;
    }
    const deleted = await Discord.deleteChannel(meeting.channel_id);
    if (deleted) {
      const stud = await Stud.getStud(offer.stud_id);
      stud.status = 'FINISHED';
      offer.status = 'FINISHED';
      await Stud.updateStud(stud);
      await Offer.updateOffer(offer);
      console.log(`Channel : '${meeting.channel_id}' has been deleted`);
    }
  }
}

const deleteChannelsJob = new CronJob('00 00/5 * * * *', deleteExpiredMeeting, null, true, 'Europe/London', null, true);

module.exports = {
  deleteExpiredMeeting,
  deleteChannelsJob
};
