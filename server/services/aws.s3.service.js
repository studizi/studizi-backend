/* eslint-disable camelcase */
const AWS = require('aws-sdk');
AWS.config.update({ region: 'eu-west-1' });
const S3 = require('aws-sdk/clients/s3');
const fs = require('fs');

async function addFileToS3 (file) {
  const upload = new S3.ManagedUpload({
    params: {
      Bucket: 'studizi-user-files',
      Key: file.originalname,
      Body: fs.readFileSync(__dirname + `/../../${file.path}`),
      ACL: 'public-read'
    }
  });

  const promise = upload.promise();

  const data = await promise;
  return data.Location;
}

module.exports = {
  addFileToS3
};
