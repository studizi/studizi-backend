const nodemailer = require('nodemailer');
const evt = require('../utils/environment');

let transporter = null;

function createTransporter () {
  transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: evt('EMAIL_ADDRESS'),
      pass: evt('EMAIL_PASSWORD')
    }
  });
}

function sendInvitationMail (email, discordChannelLink) {
  if (evt('ENV') !== 'PROD') {
    return;
  }
  const mailOptions = {
    from: evt('EMAIL_ADDRESS'),
    to: email,
    subject: 'Invitation to your new STUDIZI',
    text: `Here is the link to discord channel: ${discordChannelLink}`
  };
  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log(error);
    }
  });
}

function sendSignupMail (email, name, verificationLink) {
  const mailOptions = {
    from: evt('EMAIL_ADDRESS'),
    to: email,
    subject: 'Invitation to your new STUDIZI',
    html: `Hello ${name},<br><br>
    Thank you for joining us.<br>
    The last thing you should do before benefit of all our services is confirming your email by clicking <a href=${verificationLink}>here</a>.<br><br>
    Have a nice day!`
  };
  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log(error);
    }
  });
}

module.exports = {
  createTransporter,
  sendInvitationMail,
  sendSignupMail
};
