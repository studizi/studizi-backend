/* eslint-disable camelcase */
const evt = require('../utils/environment');
const Discord = require('discord.js');
const guildId = evt('GUILD_ID');
let client;

async function createDiscordClient () {
  client = new Discord.Client();
  await client.login(evt('BOT_TOKEN'));
  client.on('ready', () => {
    console.log('Discord client is connected!');
  });
}

async function createChannel (participants) {
  const guild = await client.guilds.fetch(guildId);
  const newChannel = await guild.channels.create(participants, {
    type: 'voice'
  });
  const category = guild.channels.cache.find(
    (c) => c.name === 'Tutoring' && c.type === 'category'
  );
  newChannel.setParent(category.id);
  const channelInfo = {
    link: (await newChannel.createInvite()).url,
    id: newChannel.id
  };
  return channelInfo;
};

async function deleteChannel (channelId) {
  try {
    const channel = await client.channels.fetch(channelId);
    channel.delete();
    return true;
  } catch (err) {
    return false;
  }
}

function closeConnection () {
  client.destroy();
}

module.exports = {
  createChannel,
  deleteChannel,
  createDiscordClient,
  closeConnection
};
