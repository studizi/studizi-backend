/* eslint-disable camelcase */
const knex = require('../../loaders/knex.loader');
const logger = require('../../log/winston.logger');
const logLevels = require('../../log/log.levels.json').levels;
const moment = require('moment');
const Credit = require('./credit.model');
const initialCredit = require('../../utils/constants.json').credits;
const tableNames = require('../../utils/constants.json').tables;
const schema = require('../../utils/constants.json').SCHEMA;
const evt = require('../../utils/environment');

/**
 * User model. Interacts with your database service.
 * @typedef {Object} UserModel
 * @property {undefined | Number} user_id
 * @property {String} first_name
 * @property {String} last_name
 * @property {String} email
 * @property {undefined | String} password
 * @property {String} username
 * @property {Date} birthdate
 * @property {Array.<String>} alert_tags
 * @property {String} profile_picture
 */
class User {
  /**
   * @param {UserModel} user
   * @returns {Promise<UserModel> || Promise<null>}
   */
  static cleanUser (user) {
    delete user.password;
    user.birthdate = moment(user.birthdate).format('YYYY-MM-DD');
    return user;
  }

  /**
   * Creates a user.
   * @param {UserModel} user
   * @returns {Promise<UserModel> || Promise<null>}
   */
  async createUser (user) {
    try {
      if (evt('ENV') !== 'PROD') {
        user.verified = true;
      }
      const created = await knex
        .withSchema(schema)
        .insert(user, '*')
        .into(tableNames.USER);
      if (created.length) {
        await Credit.createCredit({
          user_id: created[0].user_id,
          silver_credit: initialCredit.initial_silver_credit,
          gold_credit: initialCredit.initial_gold_credit
        });
      }
      return created.length ? User.cleanUser(created[0]) : null;
    } catch (err) {
      logger('createUser', logLevels.error, err);
      return null;
    }
  }

  /**
   * Deletes a user by its `user_id`.
   * @param {Number} user_id
   * @returns {Promise<UserModel> || Promise<null>}
   */
  async deleteUser (user_id) {
    try {
      const deleted = await knex(tableNames.USER)
        .withSchema(schema)
        .where('user_id', user_id)
        .delete('*');
      return deleted.length ? User.cleanUser(deleted[0]) : null;
    } catch (err) {
      logger('deleteUser', logLevels.error, err);
      return null;
    }
  }

  /**
   * Searches for a user by its `user_id`.
   * @param {Number} user_id
   * @returns {Promise<UserModel> || Promise<null>}
   */
  async getUser (user_id) {
    try {
      const selected = await knex
        .withSchema(schema)
        .select('*')
        .from(tableNames.USER)
        .where('user_id', user_id);
      return selected.length ? User.cleanUser(selected[0]) : null;
    } catch (err) {
      logger('getUser', logLevels.error, err);
      return null;
    }
  }

  /**
   * Checks whether a user exists or not.
   * @param {Number} user_id
   * @returns {Promise<Boolean>}
   */
  async userExists (user_id) {
    try {
      const selected = await knex
        .withSchema(schema)
        .select('user_id')
        .from(tableNames.USER)
        .where('user_id', user_id);
      return !!selected.length;
    } catch (err) {
      logger('userExists', logLevels.error, err);
      return false;
    }
  }

  /**
   * Searches for a user by its `email`.
   * @param {String} email
   * @param {String} password
   * @returns {Promise<Number> || Promise<null>}
   */
  async getUserIdIfExist (email, password) {
    try {
      const selected = await knex
        .withSchema(schema)
        .select('user_id')
        .from(tableNames.USER)
        .where('email', email)
        .andWhere('password', password);
      return selected.length ? selected[0].user_id : null;
    } catch (err) {
      logger('getUserIdIfExist', logLevels.error, err);
      return null;
    }
  }

  /**
   * Searches for a user with its ratings by its `user_id`.
   * @param {Number} user_id
   * @returns {Promise<{user_id: Number, avg_helper_rating: Number, avg_seeker_rating: Number}>}
   */
  async getRatedUser (user_id) {
    try {
      const rated = await knex
        .withSchema(schema)
        .select('*')
        .from('AVERAGE_RATING')
        .where('user_id', user_id);
      return rated[0] ? rated[0] : null;
      // return rated[0] ? rated[0] : null;
    } catch (err) {
      logger('getRatedUser', logLevels.error, err);
      return null;
    }
  }

  /**
   * Updates a user.
   * @param {UserModel} user
   * @returns {Promise<UserModel> || Promise<null>}
   */
  async updateUser (user) {
    try {
      const updated = await knex
        .withSchema(schema)
        .update(user, '*')
        .from(tableNames.USER)
        .where('user_id', user.user_id);
      return updated.length ? User.cleanUser(updated[0]) : null;
    } catch (err) {
      logger('updateUser', logLevels.error, err);
      return null;
    }
  }
}
module.exports = new User();
