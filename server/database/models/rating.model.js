/* eslint-disable camelcase */
const knex = require('../../loaders/knex.loader');
const logger = require('../../log/winston.logger');
const logLevels = require('../../log/log.levels.json').levels;
const tableNames = require('../../utils/constants.json').tables;
const schema = require('../../utils/constants.json').SCHEMA;

/**
 * Rating model. Interracts with your database service.
 * @typedef {Object} RatingModel
 * @property {undefined | Number} rating_id
 * @property {Number} rating
 * @property {String} comment
 * @property {Number} rated_user_id
 * @property {Number} rating_user_id
 * @property {String} type
 */
class Rating {
  /**
   * Creates an rating.
   * @param {RatingModel} rating
   * @returns {Promise<RatingModel>}
   */
  async createRating (rating) {
    try {
      const created = await knex
        .withSchema(schema)
        .insert(rating, '*')
        .into(tableNames.RATING);
      return created.length ? created[0] : null;
    } catch (err) {
      logger('createRating', logLevels.error, err);
      return null;
    }
  }

  /**
   * Deletes an rating by its `rating_id`.
   * @param {Number} rating_id
   * @returns {Promise<RatingModel>}
   */
  async deleteRating (rating_id) {
    try {
      const deleted = await knex(tableNames.RATING)
        .withSchema(schema)
        .where('rating_id', rating_id)
        .delete('*');
      return deleted.length ? deleted[0] : null;
    } catch (err) {
      logger('deleteRating', logLevels.error, err);
      return null;
    }
  }

  /**
   * Searches for an rating by its `rating_id`.
   * @param {Number} rating_id
   * @returns {Promise<RatingModel>}
   */
  async getRating (rating_id) {
    try {
      const selected = await knex
        .withSchema(schema)
        .select('*')
        .from(tableNames.RATING)
        .where('rating_id', rating_id);
      return selected.length ? selected[0] : null;
    } catch (err) {
      logger('getRating', logLevels.error, err);
      return null;
    }
  }

  /**
   * @param {Number} offer_id
   * @param {Number} rating_user_id
   * @param {Number} rated_user_id
   * @returns {Promise<Array<RatingModel>>}
   */
  async getRatingList (
    offer_id = -1,
    rating_user_id = -1,
    rated_user_id = -1,
    limit = 10,
    offset = 0) {
    try {
      const ratings = await knex
        .withSchema(schema)
        .select()
        .from(tableNames.RATING)
        .andWhere(builder => {
          if (offer_id !== -1) {
            builder.where('offer_id', offer_id);
          }
        })
        .andWhere(builder => {
          if (rating_user_id !== -1) {
            builder.where('rating_user_id', rating_user_id);
          }
        })
        .andWhere(builder => {
          if (rated_user_id !== -1) {
            builder.where('rated_user_id', rated_user_id);
          }
        })
        .limit(limit)
        .offset(offset);
      return ratings;
    } catch (err) {
      logger('getRatingList', logLevels.error, err);
      return null;
    }
  }

  /**
   * Checks whether a rating exists or not.
   * @param {Number} rating_id
   * @returns {Promise<Boolean>}
   */
  async ratingExists (rating_id) {
    try {
      const selected = await knex
        .withSchema(schema)
        .select('rating_id')
        .from(tableNames.RATING)
        .where('rating_id', rating_id);
      return !!selected.length;
    } catch (error) {
      logger('ratingExists', logLevels.error, error);
      return false;
    }
  }

  /**
   * Updates an rating.
   * @param {RatingModel} rating
   * @returns {Promise<RatingModel>}
   */
  async updateRating (rating) {
    try {
      const updated = await knex
        .withSchema(schema)
        .update(rating, '*')
        .from(tableNames.RATING)
        .where('rating_id', rating.rating_id);
      return updated.length ? updated[0] : null;
    } catch (err) {
      logger('updateRating', logLevels.error, err);
      return null;
    }
  }
}

module.exports = new Rating();
