/* eslint-disable camelcase */
const knex = require('../../loaders/knex.loader');
const logger = require('../../log/winston.logger');
const logLevels = require('../../log/log.levels.json').levels;
const tableNames = require('../../utils/constants.json').tables;
const schema = require('../../utils/constants.json').SCHEMA;

/**
 * EmailVerification model. Interracts with your database service.
 * @property {Number} user_id
 * @property {String} hash
 */

class EmailVerification {
  /**
   * Creates a stud.
   * @param {Number} user_id
   * @returns {Promise<EmailVerificationModel> | Promise<null>}
   */
  async createEmailVerification (user_id) {
    try {
      const randomCode = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
      const created = await knex
        .withSchema(schema)
        .insert({
          user_id: user_id,
          verification_code: randomCode
        }, '*')
        .into(tableNames.EMAIL_VERIFICATION);
      return created.length ? created[0] : null;
    } catch (err) {
      logger('createEmailVerification', logLevels.error, err);
      return null;
    }
  }

  async verificationCodeIsValid (verification_code) {
    try {
      const foundCode = await knex
        .withSchema(schema)
        .select('*')
        .from(tableNames.EMAIL_VERIFICATION)
        .where('verification_code', verification_code);
      return foundCode.length ? foundCode[0] : null;
    } catch (err) {
      logger('verificationCodeIsValid', logLevels.error, err);
      return null;
    }
  }

  async deleteVerificationCode (user_id) {
    try {
      const deleted = await knex(tableNames.EMAIL_VERIFICATION)
        .withSchema(schema)
        .where('user_id', user_id)
        .delete('*');
      return deleted.length ? deleted[0] : null;
    } catch (err) {
      logger('deleteVerificationCode', logLevels.error, err);
      return null;
    }
  }
}

module.exports = new EmailVerification();
