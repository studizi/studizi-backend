/* eslint-disable camelcase */
const knex = require('../../loaders/knex.loader');
const logger = require('../../log/winston.logger');
const logLevels = require('../../log/log.levels.json').levels;
const tableNames = require('../../utils/constants.json').tables;
const schema = require('../../utils/constants.json').SCHEMA;
const Offer = require('./offer.model');

/**
 * Meeting model. Interracts with your database service.
 * @typedef {Object} MeetingModel
 * @property {Number} offer_id
 * @property {String} channel_link
 * @property {String} channel_id
 * @property {String} creation_timestamp
 */
class Meeting {
  /**
   * Creates an meeting.
   * @param {MeetingModel} meeting
   * @returns {Promise<MeetingModel>}
   */
  async createMeeting (meeting) {
    try {
      const created = await knex
        .withSchema(schema)
        .insert(meeting, '*')
        .into(tableNames.MEETING);
      return created.length ? created[0] : null;
    } catch (err) {
      logger('createMeeting', logLevels.error, err);
      return null;
    }
  }

  /**
   * Deletes an meeting by its `offer_id`.
   * @param {Number} offer_id
   * @returns {Promise<MeetingModel>}
   */
  async deleteMeeting (offer_id) {
    try {
      const deleted = await knex(tableNames.MEETING)
        .withSchema(schema)
        .where('offer_id', offer_id)
        .delete('*');
      return deleted.length ? deleted[0] : null;
    } catch (err) {
      logger('deleteMeeting', logLevels.error, err);
      return null;
    }
  }

  /**
   * @param {Number} offer_id
   * @returns {Promise<MeetingModel>}
   */
  async getMeetingByOfferId (offer_id) {
    try {
      const selected = await knex
        .withSchema(schema)
        .select('*')
        .from(tableNames.MEETING)
        .where('offer_id', offer_id);
      return selected.length ? selected[0] : null;
    } catch (err) {
      logger('getMeetingByOfferId', logLevels.error, err);
      return null;
    }
  }

  /**
   * Fetches all meetings from various parameters.
   * @param {Number} offer_id
   * @param {Number} helper_user_id
   * @param {Number} stud_owner_id
   * @param {Number} limit
   * @param {Number} offset
   * @returns {Promise.<Array.<{offer_id: Number, stud_id: Number, channel_link: String, meeting_date: String, helper_user_id: Number, stud_owner_id: Number, creation_timestamp: String}>>}
   */
  async getMeetingList (
    creation_timestamp,
    limit = 10,
    offset = 0,
    offer_id = -1,
    helper_user_id = -1,
    stud_owner_id = -1
  ) {
    const columns = [
      'offer_id',
      'stud_id',
      'channel_link',
      'meeting_date',
      'helper_user_id',
      'stud_owner_id',
      'creation_timestamp'
    ];
    try {
      const selected = await knex
        .withSchema(schema)
        .column(columns)
        .select()
        .from(tableNames.MEETING_INFO)
        .where('creation_timestamp', '<', creation_timestamp)
        .andWhere(builder => {
          if (offer_id !== -1) {
            builder.where('offer_id', offer_id);
          }
        })
        .andWhere(builder => {
          if (helper_user_id !== -1) {
            builder.where('helper_user_id', helper_user_id);
          }
        })
        .andWhere(builder => {
          if (stud_owner_id !== -1) {
            builder.where('stud_owner_id', stud_owner_id);
          }
        })
        .orderBy('creation_timestamp', 'desc')
        .limit(limit)
        .offset(offset);
      return selected;
    } catch (err) {
      logger('getMeetingList', logLevels.error, err);
      return null;
    }
  }

  /**
   * Checks whether a meeting exists or not.
   * @param {Number} offer_id
   * @returns {Promise<Boolean>}
   */
  async meetingExists (offer_id) {
    try {
      const selected = await knex
        .withSchema(schema)
        .select('offer_id')
        .from(tableNames.MEETING)
        .where('offer_id', offer_id);
      return !!selected.length;
    } catch (error) {
      logger('meetingExists', logLevels.error, error);
      return false;
    }
  }

  /**
   * Updates an meeting.
   * @param {MeetingModel} meeting
   * @returns {Promise<MeetingModel>}
   */
  async updateMeeting (meeting) {
    try {
      const updated = await knex
        .withSchema(schema)
        .update(meeting, '*')
        .from(tableNames.MEETING)
        .where('offer_id', meeting.offer_id);
      return updated.length ? updated[0] : null;
    } catch (err) {
      logger('updateMeeting', logLevels.error, err);
      return null;
    }
  }
}

module.exports = new Meeting();
