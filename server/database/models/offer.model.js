/* eslint-disable camelcase */
const knex = require('../../loaders/knex.loader');
const logger = require('../../log/winston.logger');
const logLevels = require('../../log/log.levels.json').levels;
const tableNames = require('../../utils/constants.json').tables;
const schema = require('../../utils/constants.json').SCHEMA;

/**
 * Offer model. Interracts with your database service.
 * @typedef {Object} OfferModel
 * @property {undefined | Number} offer_id
 * @property {Number} stud_id
 * @property {Number} helper_user_id
 * @property {tstzrange} meeting_date
 * @property {String} status
 * @property {String} creation_timestamp
 */
class Offer {
  /**
   * Creates an offer.
   * @param {OfferModel} offer
   * @returns {Promise<OfferModel>}
   */
  async createOffer (offer) {
    try {
      const created = await knex
        .withSchema(schema)
        .insert(offer, '*')
        .into(tableNames.OFFER);
      return created.length ? created[0] : null;
    } catch (err) {
      logger('createOffer', logLevels.error, err);
      return null;
    }
  }

  /**
   * Deletes an offer by its `offer_id`.
   * @param {Number} offer_id
   * @returns {Promise<OfferModel>}
   */
  async deleteOffer (offer_id) {
    try {
      const deleted = await knex(tableNames.OFFER)
        .withSchema(schema)
        .where('offer_id', offer_id)
        .delete('*');
      return deleted.length ? deleted[0] : null;
    } catch (err) {
      logger('deleteOffer', logLevels.error, err);
      return null;
    }
  }

  /**
   * Searches for an offer by its `offer_id`.
   * @param {Number} offer_id
   * @returns {Promise<OfferModel>}
   */
  async getOffer (offer_id) {
    try {
      const selected = await knex
        .withSchema(schema)
        .select('*')
        .from(tableNames.OFFER)
        .where('offer_id', offer_id);
      return selected.length ? selected[0] : null;
    } catch (err) {
      logger('getOffer', logLevels.error, err);
      return null;
    }
  }

  /**
   * @param {String} creation_timestamp
   * @param {Number} helper_or_sekker_user_id
   * @param {Number} limit
   * @param {Number} offset
   * @param {String} status
   * @returns {Promise.<Array.<OfferModel>>}
   */
  async getOfferList (
    creation_timestamp,
    helper_or_sekker_user_id = -1,
    limit = 50,
    offset = 0,
    status = ''
  ) {
    try {
      const selected = await knex
        .withSchema(schema)
        .select(tableNames.OFFER + '.*')
        .from(tableNames.OFFER)
        .innerJoin(
          tableNames.STUD,
          tableNames.OFFER + '.stud_id',
          tableNames.STUD + '.stud_id'
        )
        .where(
          tableNames.OFFER + '.creation_timestamp',
          '<',
          creation_timestamp
        )
        .andWhere(builder => {
          if (helper_or_sekker_user_id !== -1) {
            builder
              .where('helper_user_id', helper_or_sekker_user_id)
              .orWhere('stud_owner_id', helper_or_sekker_user_id);
          }
        })
        .andWhere(builder => {
          if (status !== '') {
            builder.where(tableNames.OFFER + '.status', status);
          }
        })
        .orderBy(tableNames.OFFER + '.creation_timestamp', 'desc')
        .limit(limit)
        .offset(offset);
      return selected;
    } catch (err) {
      logger('getOfferList', logLevels.error, err);
      return null;
    }
  }

  /**
   * Get the list of offers that have been scheduled
   * @returns {null | Promise<Array<OfferModel>>}
   */
  async getOffersWithScheduledStatus () {
    try {
      const selected = await knex
        .withSchema(schema)
        .select('*')
        .from(tableNames.OFFER)
        .where('status', 'SCHEDULED');
      return selected.length ? selected : null;
    } catch (err) {
      logger('getOffer', logLevels.error, err);
      return null;
    }
  }

  /**
   * Searches for offers by their `stud_id`.
   * @param {Number} stud_id
   * @returns {Promise<Array.<OfferModel>>}
   */
  async getOffersByStudId (stud_id) {
    try {
      const selected = await knex
        .withSchema(schema)
        .select('*')
        .from(tableNames.OFFER)
        .where('stud_id', stud_id);
      return selected.length ? selected : null;
    } catch (err) {
      logger('getOffersByStudId', logLevels.error, err);
      return null;
    }
  }

  /**
   * Checks whether an offer exists or not.
   * @param {Number} offer_id
   * @returns {Promise<Boolean>}
   */
  async offerExists (offer_id) {
    try {
      const selected = await knex
        .withSchema(schema)
        .select('offer_id')
        .from(tableNames.OFFER)
        .where('offer_id', offer_id);
      return !!selected.length;
    } catch (err) {
      logger('offerExists', logLevels.error, err);
      return false;
    }
  }

  /**
   * Updates an offer.
   * @param {OfferModel} offer
   * @returns {Promise<OfferModel>}
   */
  async updateOffer (offer) {
    try {
      const updated = await knex
        .withSchema(schema)
        .update(offer, '*')
        .from(tableNames.OFFER)
        .where('offer_id', offer.offer_id);
      return updated.length ? updated[0] : null;
    } catch (err) {
      logger('updateOffer', logLevels.error, err);
      return null;
    }
  }
}

module.exports = new Offer();
