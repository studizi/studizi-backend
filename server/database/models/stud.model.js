/* eslint-disable camelcase */
const knex = require('../../loaders/knex.loader');
const logger = require('../../log/winston.logger');
const logLevels = require('../../log/log.levels.json').levels;
const tableNames = require('../../utils/constants.json').tables;
const schema = require('../../utils/constants.json').SCHEMA;
const credits = require('./credit.model');
/**
 * Stud model. Interracts with your database service.
 * @typedef {Object} StudModel
 * @property {undefined | Number} stud_id
 * @property {String} title
 * @property {String} degree_level
 * @property {Array.<String>} tags
 * @property {String} problem_description
 * @property {Array.<String>} attachments
 * @property {timestamptz} deadline
 * @property {Array.<tstzrange>} availability_list
 * @property {Number} stud_owner_id
 * @property {Number} silver_credit_cost
 * @property {Number} gold_credit_cost
 * @property {String} status
 * @property {timestamptz} creation_timestamp
 */

class Stud {
  /**
   * Creates a stud.
   * @param {StudModel} stud
   * @returns {Promise<StudModel> | Promise<null>}
   */
  async createStud (stud) {
    try {
      if (stud.gold_credit_cost === 0 && stud.silver_credit_cost === 0) {
        const message = "the cost of stud can't be equal to zero";
        logger('createStud', logLevels.error, message);
        return null;
      }
      const created = await knex
        .withSchema(schema)
        .insert(stud, '*')
        .into(tableNames.STUD);
      if (created !== null) {
        const credit = await credits.removeCredit(
          created[0].stud_owner_id,
          created[0].silver_credit_cost,
          created[0].gold_credit_cost);

        if (credit === null) {
          this.deleteStud(created[0].stud_id);
          throw new Error("Can't remove credits from user");
        }
      }
      return created.length ? created[0] : null;
    } catch (error) {
      logger('createStud', logLevels.error, error);
      return null;
    }
  }

  /**
   * Deletes a stud by its `stud_id`.
   * @param {Number} stud_id
   * @returns {Promise<StudModel> | Promise<null>}
   */
  async deleteStud (stud_id) {
    try {
      const deleted = await knex
        .withSchema(schema)
        .from(tableNames.STUD)
        .where('stud_id', stud_id)
        .delete('*');
      return deleted.length ? deleted[0] : null;
    } catch (error) {
      logger('deleteStud', logLevels.error, error);
      return null;
    }
  }

  /**
   * Searches studs strictly older than specified timestamp
   * @param {String} creationTimestamp YYYY-MM-DDTHH24:MM:SS:MSZ (UTC)
   * @param {Number} limit
   * @param {Number} offset
   * @param {Array.<String>} tags
   * @param {Number} stud_owner_id
   * @returns {Promise<Array.<{stud_id: Number, stud_owner_id: Number, problem_description: String, silver_credit_cost: Number, gold_credit_cost: Number, title: String, tags: Array.<String>, deadline: timestamptz, status: String, creation_timestamp: timestamptz}>> | Promise<null>}
   */
  async getStudList (
    creationTimestamp,
    limit = 50,
    offset = 0,
    tags = [],
    stud_owner_id = -1
  ) {
    const columns = [
      'stud_id',
      'creation_timestamp',
      'problem_description',
      'stud_owner_id',
      'title',
      'tags',
      'deadline',
      'status',
      'silver_credit_cost',
      'gold_credit_cost'
    ];
    try {
      const selected = await knex
        .withSchema(schema)
        .column(columns)
        .select()
        .from(tableNames.STUD)
        .where('creation_timestamp', '<', creationTimestamp)
        .andWhere(builder => {
          if (tags.length) {
            builder.whereRaw('tags && ?', [tags]);
          }
        })
        .andWhere(builder => {
          if (stud_owner_id !== -1) {
            builder.where('stud_owner_id', stud_owner_id);
          }
        })
        .orderBy('creation_timestamp', 'desc')
        .limit(limit)
        .offset(offset);
      return selected;
    } catch (err) {
      logger('getStudList', logLevels.error, err);
      return null;
    }
  }

  /**
   * Searches for a stud by its `stud_id`.
   * @param {Number} stud_id
   * @returns {Promise<StudModel> | Promise<null>}
   */
  async getStud (stud_id) {
    try {
      const selected = await knex
        .withSchema(schema)
        .select('*')
        .from(tableNames.STUD)
        .where('stud_id', stud_id);
      return selected.length ? selected[0] : null;
    } catch (error) {
      logger('getStud', logLevels.error, error);
      return null;
    }
  }

  /**
   * Checks whether a stud exists or not
   * @param {Number} stud_id
   * @returns {Promise<Boolean>}
   */
  async studExists (stud_id) {
    try {
      const selected = await knex
        .withSchema(schema)
        .select('stud_id')
        .from(tableNames.STUD)
        .where('stud_id', stud_id);
      return !!selected.length;
    } catch (error) {
      logger('studExists', logLevels.error, error);
      return false;
    }
  }

  /**
   * Updates a stud.
   * @param {StudModel} stud
   * @returns {Promise<StudModel> | Promise<null>}
   */
  async updateStud (stud) {
    let old;
    let oldCredit;
    try {
      oldCredit = await credits.getCredit(stud.stud_owner_id);
      old = await this.getStud(stud.stud_id);
      if (stud.gold_credit_cost === 0 && stud.silver_credit_cost === 0) {
        const message = "the cost of stud can't be equal to zero";
        logger('updateStud', logLevels.error, message);
        return null;
      }
      const silver_cost_difference = stud.silver_credit_cost - old.silver_credit_cost;
      const gold_cost_difference = stud.gold_credit_cost - old.gold_credit_cost;
      if (silver_cost_difference < 0) {
        await credits.addCredit(
          old.stud_owner_id,
          -silver_cost_difference,
          0
        );
      } else if (silver_cost_difference > 0) {
        const credit = await credits.removeCredit(
          old.stud_owner_id,
          silver_cost_difference,
          0
        );
        if (credit === null) {
          throw new Error("Can't remove silver credit from user");
        }
      }
      if (gold_cost_difference < 0) {
        await credits.addCredit(
          old.stud_owner_id,
          0,
          -gold_cost_difference
        );
      } else if (gold_cost_difference > 0) {
        const credit = await credits.removeCredit(
          old.stud_owner_id,
          0,
          gold_cost_difference
        );
        if (credit === null) {
          throw new Error("Can't remove credits from user");
        }
      }
      const updated = await knex
        .withSchema(schema)
        .update(stud, '*')
        .from(tableNames.STUD)
        .where('stud_id', stud.stud_id);
      return updated.length ? updated[0] : null;
    } catch (error) {
      if (old) {
        await knex
          .withSchema(schema)
          .update(old, '*')
          .from(tableNames.STUD)
          .where('stud_id', old.stud_id);
      }
      if (oldCredit) {
        await knex
          .withSchema(schema)
          .update(oldCredit, '*')
          .from(tableNames.CREDIT)
          .where('user_id', stud.stud_owner_id);
      }
      logger('updateStud', logLevels.error, error);
      return null;
    }
  }
}

module.exports = new Stud();
