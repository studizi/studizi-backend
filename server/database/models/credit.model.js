/* eslint-disable camelcase */
const knex = require('../../loaders/knex.loader');
const logger = require('../../log/winston.logger');
const logLevels = require('../../log/log.levels.json').levels;
const tableNames = require('../../utils/constants.json').tables;
const schema = require('../../utils/constants.json').SCHEMA;

/**
 * Credit model. Interracts with your database service.
 * @typedef {Object} CreditModel
 * @property {Number} user_id
 * @property {Number} silver_credit
 * @property {Number} gold_credit
 */

class Credit {
  /**
   * Creates associated credit to user.
   * @param {CreditModel} credit
   * @returns {Promise<CreditModel> | Promise<null>}
   */

  async createCredit (credit) {
    try {
      const created = await knex
        .withSchema(schema)
        .insert(credit, '*')
        .into(tableNames.CREDIT);
      return created.length ? created[0] : null;
    } catch (err) {
      logger('createCredit', logLevels.error, err);
      return null;
    }
  }

  /**
   * Searches for a user's credit by its `user_id`.
   * @param {Number} user_id
   * @returns {Promise<CreditModel> | Promise<null>}
   */
  async getCredit (user_id) {
    try {
      const foundUserCredit = await knex(tableNames.CREDIT)
        .withSchema(schema)
        .select('*')
        .where('user_id', user_id);
      return foundUserCredit.length ? foundUserCredit[0] : null;
    } catch (err) {
      logger('getCredit', logLevels.error, err);
      return null;
    }
  }

  /**
   * Checks whether a credit exists or not.
   * @param {Number} user_id
   * @returns {Promise<Boolean>}
   */
  async creditExists (user_id) {
    try {
      const selected = await knex
        .withSchema(schema)
        .select('user_id')
        .from(tableNames.CREDIT)
        .where('user_id', user_id);
      return !!selected.length;
    } catch (error) {
      logger('creditExists', logLevels.error, error);
      return false;
    }
  }

  /**
   * Add credit to current credit object
   * @param {Number} user_id - Relevant credit
   * @param {Number} addedSilverCredit - Amount of silver credit to add
   * @param {Number} addedGoldCredit - Amount of gold credit to add
   * @returns {Promise<CreditModel> | Promise<null>}
   */
  async addCredit (user_id, addedSilverCredit, addedGoldCredit = 0) {
    try {
      const updated = await knex
        .withSchema('stdz')
        .update({
          silver_credit: knex.raw(`silver_credit + ${addedSilverCredit}`),
          gold_credit: knex.raw(`gold_credit + ${addedGoldCredit}`)
        }, '*')
        .from(tableNames.CREDIT)
        .where('user_id', user_id);
      return updated.length ? updated[0] : null;
    } catch (err) {
      logger('addCredit', logLevels.error, err);
      return null;
    }
  }

  /**
   * Removing credit from current credit object
   * @param {Number} user_id - Relevant credit
   * @param {Number} removedSilverCredit - Amount of silver credit to remove
   * @param {Number} removedGoldCredit - Amount of gold credit to remove
   * @returns {Promise<CreditModel> | Promise<null>}
   */
  async removeCredit (user_id, removedSilverCredit, removedGoldCredit = 0) {
    try {
      const updated = await knex
        .withSchema('stdz')
        .update({
          silver_credit: knex.raw(`silver_credit - ${removedSilverCredit}`),
          gold_credit: knex.raw(`gold_credit - ${removedGoldCredit}`)
        }, '*')
        .from(tableNames.CREDIT)
        .where('user_id', user_id);
      return updated.length ? updated[0] : null;
    } catch (err) {
      logger('removeCredit', logLevels.error, err);
      return null;
    }
  }
}

module.exports = new Credit();
