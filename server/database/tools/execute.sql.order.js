const knex = require('../../loaders/knex.loader');
const fs = require('fs');
const path = require('path');
const logger = require('../../log/winston.logger');
const logLevels = require('../../log/log.levels.json').levels;

/**
 * @returns {Promise<Boolean>}
 */
async function dbExists () {
  const schema = await knex.select('schema_name').from('information_schema.schemata').where('schema_name', 'stdz');
  const exists = !!schema[0];
  return exists;
}
/**
 * @param {String} filePath
 */
async function executeScriptFile (filePath) {
  try {
    const exists = await dbExists();
    const script = fs.readFileSync(filePath, { encoding: 'utf-8' });
    if (
      filePath.includes('drop.all.sql') ||
      filePath.includes('delete.all.sql')
    ) {
      if (!exists) {
        logger('executeScriptFile', logLevels.warn, "Database doesn't exist.");
        return;
      }
    } else if (filePath.includes('init.sql')) {
      if (exists) {
        logger('executeScriptFile', logLevels.warn, 'Database already exists.');
        return;
      }
    }
    await knex.raw(script);
    logger('executeScriptFile', logLevels.verbose, 'SQL order executed.');
  } catch (err) {
    logger('executeScriptFile', logLevels.error, err);
  }
}

module.exports = {
  dbExists: async () => {
    return await dbExists();
  },
  initDb: async () =>
    await executeScriptFile(path.join(__dirname, '../scripts/init.sql')),
  deleteDb: async () => {
    await executeScriptFile(path.join(__dirname, '../scripts/delete.all.sql'));
    await knex.destroy();
  },
  dropDb: async () =>
    await executeScriptFile(path.join(__dirname, '../scripts/drop.all.sql'))
};
