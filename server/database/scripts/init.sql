CREATE SCHEMA "stdz";
CREATE TABLE "stdz"."USER" (
    "user_id" serial NOT NULL,
    "first_name" text NOT NULL,
    "last_name" text NOT NULL,
    "email" text NOT NULL UNIQUE,
    "password" text NOT NULL,
    "username" text NOT NULL,
    "birthdate" timestamptz NOT NULL CHECK ("birthdate" < CURRENT_DATE),
    "alert_tags" text [] NOT NULL,
    "profile_picture" text NOT NULL,
    "verified" boolean NOT NULL DEFAULT FALSE,
    CONSTRAINT "PK_user" PRIMARY KEY ("user_id")
);
CREATE TABLE "stdz"."EMAIL_VERIFICATION" (
    "user_id" integer NOT NULL,
    "verification_code" text NOT NULL,
    CONSTRAINT "PK_email_verification" PRIMARY KEY ("user_id"),
    CONSTRAINT "FK_94" FOREIGN KEY ("user_id") REFERENCES "stdz"."USER" ("user_id") ON DELETE CASCADE
);
CREATE TABLE "stdz"."CREDIT" (
    "user_id" integer NOT NULL,
    "silver_credit" decimal NOT NULL CHECK ("silver_credit" >= 0),
    "gold_credit" decimal NOT NULL CHECK ("gold_credit" >= 0),
    CONSTRAINT "PK_credit" PRIMARY KEY ("user_id"),
    CONSTRAINT "FK_94" FOREIGN KEY ("user_id") REFERENCES "stdz"."USER" ("user_id") ON DELETE CASCADE
);
CREATE INDEX "fkIdx_95" ON "stdz"."CREDIT" ("user_id");
CREATE TYPE "stdz"."STUD_STATUS" AS ENUM (
    'AVAILABLE',
    'SCHEDULED',
    'FINISHED'
);
CREATE TABLE "stdz"."STUD" (
    "stud_id" serial NOT NULL,
    "title" text NOT NULL,
    "degree_level" text NOT NULL,
    "tags" text [] NOT NULL,
    "problem_description" text NOT NULL,
    "attachments" text [] NULL,
    "deadline" timestamptz NOT NULL CHECK ("deadline" >= CURRENT_TIMESTAMP),
    "availability_list" tstzrange [] NOT NULL,
    "stud_owner_id" integer NOT NULL,
    "silver_credit_cost" decimal NOT NULL CHECK ("silver_credit_cost" >= 0) DEFAULT 0,
    "gold_credit_cost" decimal NOT NULL CHECK ("gold_credit_cost" >= 0) DEFAULT 0,
    "status" "stdz"."STUD_STATUS" NOT NULL DEFAULT 'AVAILABLE',
    "creation_timestamp" timestamptz DEFAULT NOW(),
    CONSTRAINT "PK_stud" PRIMARY KEY ("stud_id"),
    CONSTRAINT "FK_42" FOREIGN KEY ("stud_owner_id") REFERENCES "stdz"."USER" ("user_id") ON DELETE CASCADE
);
CREATE INDEX "fkIdx_43" ON "stdz"."STUD" ("stud_owner_id");
CREATE TYPE "stdz"."OFFER_STATUS" AS ENUM (
    'PENDING',
    'ACCEPTED',
    'SCHEDULED',
    'FINISHED',
    'REJECTED'
);
CREATE TABLE "stdz"."OFFER" (
    "offer_id" serial NOT NULL,
    "stud_id" integer NOT NULL,
    "helper_user_id" integer NOT NULL,
    "meeting_date" tstzrange NOT NULL,
    "status" "stdz"."OFFER_STATUS" NOT NULL DEFAULT 'PENDING',
    "creation_timestamp" timestamptz DEFAULT NOW(),
    CONSTRAINT "PK_offer" PRIMARY KEY ("offer_id"),
    CONSTRAINT "FK_55" FOREIGN KEY ("stud_id") REFERENCES "stdz"."STUD" ("stud_id") ON DELETE CASCADE,
    CONSTRAINT "FK_59" FOREIGN KEY ("helper_user_id") REFERENCES "stdz"."USER" ("user_id") ON DELETE CASCADE
);
CREATE INDEX "fkIdx_56" ON "stdz"."OFFER" ("stud_id");
CREATE INDEX "fkIdx_60" ON "stdz"."OFFER" ("helper_user_id");
CREATE TABLE "stdz"."MEETING" (
    "offer_id" integer NOT NULL,
    "channel_link" text NOT NULL,
    "channel_id" text NOT NULL,
    "creation_timestamp" timestamptz DEFAULT NOW(),
    CONSTRAINT "PK_meeting" PRIMARY KEY ("offer_id"),
    CONSTRAINT "FK_87" FOREIGN KEY ("offer_id") REFERENCES "stdz"."OFFER" ("offer_id") ON DELETE CASCADE
);
CREATE INDEX fkIdx_88 ON "stdz"."MEETING" ("offer_id");
CREATE TYPE "stdz"."RATING_TYPE" AS ENUM ('HELPER', 'SEEKER');
CREATE TABLE "stdz"."RATING" (
    "rating_id" serial NOT NULL,
    "rating" int NOT NULL CHECK (
        "rating" BETWEEN 1 AND 5
    ),
    "comment" text NOT NULL,
    "offer_id" int NOT NULL,
    "rated_user_id" integer NOT NULL,
    "rating_user_id" integer NOT NULL,
    "type" "stdz"."RATING_TYPE" NOT NULL,
    CONSTRAINT "PK_ratings" PRIMARY KEY ("rating_id"),
    CONSTRAINT "FK_113" FOREIGN KEY ("rated_user_id") REFERENCES "stdz"."USER" ("user_id") ON DELETE CASCADE,
    CONSTRAINT "FK_116" FOREIGN KEY ("rating_user_id") REFERENCES "stdz"."USER" ("user_id") ON DELETE CASCADE,
    CONSTRAINT "FK_999" FOREIGN KEY ("offer_id") REFERENCES "stdz"."OFFER" ("offer_id") ON DELETE CASCADE
);
CREATE INDEX "fkIdx_114" ON "stdz"."RATING" ("rated_user_id");
CREATE INDEX "fkIdx_117" ON "stdz"."RATING" ("rating_user_id");
CREATE VIEW "stdz"."AVERAGE_RATING" AS
SELECT A."user_id",
    AVG(B."rating") AS "avg_helper_rating",
    AVG(C."rating") AS "avg_seeker_rating"
FROM "stdz"."USER" A
    LEFT JOIN "stdz"."RATING" B ON A."user_id" = B."rated_user_id"
    AND B."type" = 'HELPER'
    LEFT JOIN "stdz"."RATING" C ON A."user_id" = C."rated_user_id"
    AND C."type" = 'SEEKER'
GROUP BY A."user_id";
CREATE VIEW "stdz"."MEETING_INFO" AS
SELECT A."offer_id",
    B."stud_id",
    A."channel_link",
    A."creation_timestamp",
    B."meeting_date",
    B."helper_user_id",
    C."stud_owner_id"
FROM "stdz"."MEETING" A
    INNER JOIN "stdz"."OFFER" B ON A."offer_id" = B."offer_id"
    INNER JOIN "stdz"."STUD" C ON B."stud_id" = C."stud_id"