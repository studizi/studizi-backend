const ResponseError = require('../responses/response.error');
const logger = require('../log/winston.logger');
const logLevels = require('../log/log.levels.json').levels;

/**
 * Encapsulate server error, centralize logs and sends a response error.
 * @param {Function} cb
 */
function middleware (cb) {
  return async (req, res, next) => {
    const origin = `${req.method} ${req.originalUrl}`;
    try {
      logger(origin, logLevels.http, null);
      await cb(req, res);
    } catch (err) {
      ResponseError.internalServerError(res);
      logger(origin, logLevels.error, err);
    }
  };
}

module.exports = middleware;
