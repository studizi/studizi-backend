const { isAValidInstance } = require('../utils/schema.validator');
const logger = require('../log/winston.logger');
const ResponseError = require('../responses/response.error');
const logLevels = require('../log/log.levels.json').levels;
const schemas = {
  offer: require('../database/schemas/offer.schema.json')
};

/**
 * @param {import('express').Request} req
 * @param  {import('express').Response} res
 * @param  {import('express').NextFunction} next
 * @param {JSON} schema
 */
function validatesSchema (req, res, next, schema) {
  if (!isAValidInstance(req.body, schema)) {
    logger(req.method + ' ' + req.route, logLevels.warn, 'Invalid body');
    ResponseError.badRequest(res);
    return;
  }
  next();
}
/**
 * @param {import('express').Request} req
 * @param  {import('express').Response} res
 * @param  {import('express').NextFunction} next
 */
function validatesOfferSchema (req, res, next) {
  validatesSchema(req, res, next, schemas.offer);
}

module.exports = { validatesOfferSchema };
