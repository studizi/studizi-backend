const logger = require('../log/winston.logger');
const responseError = require('../responses/response.error');
const Offers = require('../database/models/offer.model');
/**
 * @param {import('express').Request} req
 * @param  {import('express').Response} res
 * @param  {import('express').NextFunction} next
 */
async function handleOfferBody (req, res, next) {
  if (!req.body) {
    responseError.badRequest(req);
    return;
  }
  if (req.method === 'PUT') {
    const offer = await Offers.getOffer(req.body.offer_id);
    req.body.helper_user_id = offer.helper_user_id;
  } else {
    req.body.helper_user_id = req.user.user_id;
  }

  next();
}

/**
 * @param {import('express').Request} req
 * @param  {import('express').Response} res
 * @param  {import('express').NextFunction} next
 */
function handleCreditBody (req, res, next) {
  if (!req.body) {
    responseError.badRequest(req);
    return;
  }
  req.body.user_id = req.user.user_id;
  next();
}

module.exports = {
  handleOfferBody,
  handleCreditBody
};
