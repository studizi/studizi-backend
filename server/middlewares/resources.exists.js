const responseError = require('../responses/response.error');
const Studs = require('../database/models/stud.model');
const Users = require('../database/models/user.model');
const Meetings = require('../database/models/meeting.model');
const Offers = require('../database/models/offer.model');
const Credits = require('../database/models/credit.model');
const Ratings = require('../database/models/rating.model');
const logger = require('../log/winston.logger');
const logLevels = require('../log/log.levels.json').levels;

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 * @param {Function} checkFunction
 * @param {String} toCheck
 */
async function resourceExists (req, res, next, checkFunction, toCheck) {
  const found = req.body[toCheck]
    ? req.body[toCheck]
    : req.params[toCheck]
      ? req.params[toCheck]
      : null;
  if (!found) {
    logger('resourceExists', logLevels.error, toCheck + ' not found');
    responseError.badRequest(res);
    return;
  }
  if (isNaN(found)) {
    logger('resourceExists', logLevels.error, toCheck + ` ${found} not a number`);
    responseError.badRequest(res);
    return;
  }
  const exists = await checkFunction(found);
  if (!exists) {
    logger('resourceExists', logLevels.error, toCheck + ' doesnt exist');
    responseError.notFound(res);
    return;
  }
  next();
}

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
async function studExists (req, res, next) {
  await resourceExists(req, res, next, Studs.studExists, 'stud_id');
}

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
async function userExists (req, res, next) {
  await resourceExists(req, res, next, Users.userExists, 'user_id');
}

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
async function meetingExists (req, res, next) {
  await resourceExists(req, res, next, Meetings.meetingExists, 'offer_id');
}

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
async function ratingExists (req, res, next) {
  await resourceExists(req, res, next, Ratings.ratingExists, 'rating_id');
}

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
async function creditExists (req, res, next) {
  await resourceExists(req, res, next, Credits.creditExists, 'user_id');
}

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
async function offerExists (req, res, next) {
  await resourceExists(req, res, next, Offers.offerExists, 'offer_id');
}

module.exports = { studExists, userExists, ratingExists, creditExists, offerExists, meetingExists };
