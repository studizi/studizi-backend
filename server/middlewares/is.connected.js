const ResponseError = require('../responses/response.error');
const logger = require('../log/winston.logger');
const logLevels = require('../log/log.levels.json').levels;

/**
 * @param {import('express').Request} req
 * @param  {import('express').Response} res
 * @param  {import('express').NextFunction} next
 */
function isConnected (req, res, next) {
  if (req.user) {
    next();
    return;
  }
  logger('isConnected', logLevels.error, 'user not logged');
  ResponseError.unauthorized(res);
}

module.exports = { isConnected };
