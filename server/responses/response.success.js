const ResponseModels = require('./response.models.json');

class ResponseSuccess {
  /**
   * @param {import('express').Response} res
   * @param {JSON | null} body
   */
  success (res, body = null) {
    const model = ResponseModels.OK;
    res.status(model.statusCode).json(body);
  }
}

module.exports = new ResponseSuccess();
