const ResponseModels = require('./response.models.json');

/**
 * HTTP error response handler
 */
class ResponseError {
  /**
   * @param {import('express').Response} res
   */
  badRequest (res) {
    this.sendResponse(res, 'Bad Request');
  }

  /**
   * @param {import('express').Response} res
   */
  unauthorized (res) {
    this.sendResponse(res, 'Unauthorized');
  }

  /**
   * @param {import('express').Response} res
   */
  forbidden (res) {
    this.sendResponse(res, 'Forbidden');
  }

  /**
   * @param {import('express').Response} res
   */
  notFound (res) {
    this.sendResponse(res, 'Not Found');
  }

  /**
   * @param {import('express').Response} res
   */
  internalServerError (res) {
    this.sendResponse(res, 'Internal Server Error');
  }

  /**
   * @param {import('express').Response} res
   * @param {String} responseModel
   */
  sendResponse (res, responseModel) {
    const model = ResponseModels[responseModel];
    res.status(model.statusCode).json(model.errorMessage);
  }
}

module.exports = new ResponseError();
