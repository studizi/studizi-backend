const validate = require('jsonschema').validate;

/**
 * Validates an object against a schema.
 * @param {Object} instance Object to validate
 * @param {JSON} schema JSON SCHEMA
 * @returns {Boolean}
 */
function isAValidInstance (instance, schema) {
  const isValid = validate(instance, schema);
  return isValid.valid;
}
module.exports = { isAValidInstance };
