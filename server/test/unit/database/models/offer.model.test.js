/* eslint-disable no-unused-expressions */
/* eslint-disable no-undef */
/* eslint-disable camelcase */
const chai = require('chai');
const User = require('../../../../database/models/user.model');
const Stud = require('../../../../database/models/stud.model');
const Offer = require('../../../../database/models/offer.model');
const emailGenerator = require('../../../utils/email.generator').emailGenerator;
const dbTools = require('../../../../database/tools/execute.sql.order');
const Credit = require('../../../../database/models/credit.model');
const expect = chai.expect;
chai.use(require('chai-like'));
chai.use(require('chai-things'));
const moment = require('moment');
const knex = require('../../../../loaders/knex.loader');

const dummyUser = {
  first_name: 'test',
  last_name: 'db',
  password: 'pass',
  username: 'user',
  birthdate: '2020-01-01',
  alert_tags: ['java', 'c++'],
  profile_picture: 'google.com'
};
const dummyOffer = {
  meeting_date: '["2021-01-22 14:30:00+00","2021-01-22 15:30:00+00"]',
  status: 'ACCEPTED'
};
const dummyOffer2 = {
  meeting_date: '["2021-01-22 14:30:00+00","2021-01-22 15:30:00+00"]',
  status: 'ACCEPTED'
};
const smartOffer = {
  meeting_date: '["2021-01-22 14:30:00+00","2021-01-22 15:30:00+00"]',
  status: 'REJECTED'
};
const dummyStud = {
  title: 'test',
  degree_level: 'M2',
  tags: ['programming', 'math', 'english'],
  problem_description:
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
  attachments: ['test.test.fr'],
  deadline: moment()
    .add(1, 'day')
    .toISOString(),
  availability_list: [
    '[2021-01-21 14:30, 2021-01-21 15:30)',
    '[2021-01-22 14:30, 2021-01-22 15:30]'
  ],
  stud_owner_id: 0,
  silver_credit_cost: 0.5,
  gold_credit_cost: 0.1,
  status: 'AVAILABLE'
};

beforeEach(() => {
  dummyUser.email = emailGenerator();
});

async function create2UsersandStad (cb) {
  const user = await createUser(dummyUser);
  dummyUser.email = emailGenerator();
  const user2 = await createUser(dummyUser);
  dummyStud.stud_owner_id = user.user_id;
  const stud = await Stud.createStud(dummyStud);
  const stud2 = await Stud.createStud(dummyStud);
  dummyOffer.stud_id = stud.stud_id;
  dummyOffer.helper_user_id = user2.user_id;

  dummyOffer2.stud_id = stud2.stud_id;

  await cb(dummyOffer);
}

async function createUser (newUser) {
  const user = await User.createUser(newUser);
  await Credit.addCredit(user.user_id, 0, 50);
  return user;
}

describe('Create Offer', async () => {
  it('should create an offer using the function createOffer', async () => {
    await create2UsersandStad(async dummyOffer => {
      const offer = await Offer.createOffer(dummyOffer);

      expect(offer.offer_id)
        .to.be.a('number')
        .and.to.be.greaterThan(0);

      delete offer.offer_id;
      delete offer.creation_timestamp;
      expect(offer)
        .to.be.an('object')
        .that.eql(dummyOffer);
    });
  });
  it('should return null if status is not in OFFER_STATUS', async () => {
    await create2UsersandStad(async dummyOffer => {
      const copy = Object.assign({}, dummyOffer);
      copy.status = 'test';
      const offer = await Offer.createOffer(copy);
      expect(offer).to.be.null;
    });
  });
  it('should parse an injected sql query in status', async () => {
    await create2UsersandStad(async dummyOffer => {
      const copy = Object.assign({}, dummyOffer);
      copy.status = '"(DELETE FROM "stdz"."MEETING")';
      const offer = await Offer.createOffer(copy);
      expect(offer).to.be.a.null;
    });
  });
  it('should return null if stud_id does not exist', async () => {
    await create2UsersandStad(async dummyOffer => {
      const copy = Object.assign({}, dummyOffer);
      copy.stud_id = Number.MAX_SAFE_INTEGER;
      const offer = await Offer.createOffer(copy);
      expect(offer).to.be.null;
    });
  });
  it('should return null if stud_id is not int', async () => {
    await create2UsersandStad(async dummyOffer => {
      const copy = Object.assign({}, dummyOffer);
      copy.stud_id = 'test';
      const offer = await Offer.createOffer(copy);
      expect(offer).to.be.null;
    });
  });
  it('should return null if helper_user_id does not exist', async () => {
    await create2UsersandStad(async dummyOffer => {
      const copy = Object.assign({}, dummyOffer);
      copy.helper_user_id = Number.MAX_SAFE_INTEGER;
      const offer = await Offer.createOffer(copy);
      expect(offer).to.be.null;
    });
  });
  it('should return null if helper_user_id is not int', async () => {
    await create2UsersandStad(async dummyOffer => {
      const copy = Object.assign({}, dummyOffer);
      copy.helper_user_id = 'test';
      const offer = await Offer.createOffer(copy);
      expect(offer).to.be.null;
    });
  });
  it('should return null if meeting_date is not tstzrange', async () => {
    await create2UsersandStad(async dummyOffer => {
      const copy = Object.assign({}, dummyOffer);
      copy.meeting_date = 'test';
      const offer = await Offer.createOffer(copy);
      expect(offer).to.be.null;
    });
  });
  it('should not create an offer that does not have all fields', async () => {
    await create2UsersandStad(async dummyOffer => {
      for (const [key] of Object.entries(dummyOffer)) {
        const copy = Object.assign({}, dummyOffer);
        copy[key] = null;
        const offer = await Offer.createOffer(copy);
        expect(offer).to.be.null;
      }
    });
  });
});

describe('Delete Offer', async () => {
  it('should delete an offer using the function deleteOffer', async () => {
    await create2UsersandStad(async dummyOffer => {
      const offer = await Offer.createOffer(dummyOffer);
      const deleted = await Offer.deleteOffer(offer.offer_id);
      expect(deleted)
        .to.be.an('object')
        .that.eql(offer);
    });
  });
  it('should return null if offer does not exist', async () => {
    const deleted = await Offer.deleteOffer(Number.MAX_SAFE_INTEGER);
    expect(deleted).to.be.a.null;
  });
  it('should return null if offer_id is not int', async () => {
    const deleted = await Offer.deleteOffer('test');
    expect(deleted).to.be.a.null;
  });
});

describe('Get Offer', async () => {
  it('should get an offer using the function getOfferList', async () => {
    await create2UsersandStad(async dummyOffer => {
      const nb = 4;
      const offers = [];
      for (let i = 0; i < nb; i++) {
        offers.push(await Offer.createOffer(dummyOffer));
        if (i % 2 === 0) {
          offers[i].status = 'PENDING';
          offers[i] = await Offer.updateOffer(offers[i]);
        }
      }

      let returned = await Offer.getOfferList(moment.utc().toISOString(), dummyOffer.helper_user_id, nb);
      expect(returned.sort((a, b) => a.offer_id - b.offer_id)).to.be.eql(
        offers.sort((a, b) => a.offer_id - b.offer_id)
      );

      returned = await Offer.getOfferList(
        moment.utc().toISOString(),
        dummyOffer.helper_user_id,
        parseInt(nb / 2),
        0,
        'PENDING'
      );
      expect(returned.sort((a, b) => a.offer_id - b.offer_id)).to.be.eql(
        offers
          .filter(a => a.status === 'PENDING')
          .sort((a, b) => a.offer_id - b.offer_id)
      );
    });
  });
  it('should get an empty list for inexistant user', async () => {
    await create2UsersandStad(async dummyOffer => {
      const selected = await Offer.getOfferList(moment.utc().toISOString(), 2147483647);
      expect(selected)
        .to.be.eql([]);
    });
  });
  it('should get an offer using the function getOffer', async () => {
    await create2UsersandStad(async dummyOffer => {
      const offer = await Offer.createOffer(dummyOffer);
      const selected = await Offer.getOffer(offer.offer_id);
      expect(selected)
        .to.be.an('object')
        .that.eql(offer);
    });
  });
  it('should return null if offer does not exist', async () => {
    const selected = await Offer.getOffer(Number.MAX_SAFE_INTEGER);
    expect(selected).to.be.a.null;
  });
  it('should return null if offer_id is not int', async () => {
    const selected = await Offer.getOffer('test');
    expect(selected).to.be.a.null;
  });
});

describe('Get Offer By Stud Id', async () => {
  it('should get an offerlist using the function getOffersByStudId', async () => {
    await create2UsersandStad(async dummyOffer => {
      const offer = await Offer.createOffer(dummyOffer);
      const offerBis = await Offer.createOffer(dummyOffer);
      const selected = await Offer.getOffersByStudId(offer.stud_id);
      expect(selected)
        .to.be.an('array')
        .that.eql([offer, offerBis]);
    });
  });
  it('should return null if no offer for stud', async () => {
    await knex
      .withSchema('stdz')
      .delete('*')
      .from('OFFER')
      .where('stud_id', dummyOffer.stud_id);

    const selected = await Offer.getOffersByStudId(dummyOffer.stud_id);
    expect(selected).to.be.null;
  });
  it('should return null if stud doesnt exist', async () => {
    const selected = await Offer.getOffersByStudId(Number.MAX_SAFE_INTEGER);
    expect(selected).to.be.null;
  });
});

describe('Update Offer', async () => {
  it('should update an offer using the function updateOffer', async () => {
    await create2UsersandStad(async dummyOffer => {
      const offer = await Offer.createOffer(dummyOffer);
      offer.meeting_date = smartOffer.meeting_date;
      offer.status = smartOffer.status;
      const updated = await Offer.updateOffer(offer);

      expect(updated)
        .to.be.an('object')
        .and.to.eql(offer);
    });
  });
  it('should return null if status is not in OFFER_STATUS', async () => {
    await create2UsersandStad(async dummyOffer => {
      const offer = await Offer.createOffer(dummyOffer);
      offer.status = 'test';
      const updated = await Offer.updateOffer(offer);
      expect(updated).to.be.null;
    });
  });
  it('should return null if status is not in OFFER_STATUS', async () => {
    await create2UsersandStad(async dummyOffer => {
      const offer = await Offer.createOffer(dummyOffer);
      offer.status = 'test';
      const updated = await Offer.updateOffer(offer);
      expect(updated).to.be.null;
    });
  });
  it('should parse an injected sql query in status', async () => {
    await create2UsersandStad(async dummyOffer => {
      const offer = await Offer.createOffer(dummyOffer);
      offer.status = '"(DELETE FROM "stdz"."MEETING")';
      const updated = await Offer.updateOffer(offer);
      expect(updated).to.be.null;
    });
  });
  it('should return null if stud_id does not exist', async () => {
    await create2UsersandStad(async dummyOffer => {
      const offer = await Offer.createOffer(dummyOffer);
      offer.stud_id = Number.MAX_SAFE_INTEGER;
      const updated = await Offer.updateOffer(offer);
      expect(updated).to.be.null;
    });
  });
  it('should return null if stud_id is not int', async () => {
    await create2UsersandStad(async dummyOffer => {
      const offer = await Offer.createOffer(dummyOffer);
      offer.stud_id = 'test';
      const updated = await Offer.updateOffer(offer);
      expect(updated).to.be.null;
    });
  });
  it('should return null if helper_user_id does not exist', async () => {
    await create2UsersandStad(async dummyOffer => {
      const offer = await Offer.createOffer(dummyOffer);
      offer.helper_user_id = Number.MAX_SAFE_INTEGER;
      const updated = await Offer.updateOffer(offer);
      expect(updated).to.be.null;
    });
  });
  it('should return null if helper_user_id is not int', async () => {
    await create2UsersandStad(async dummyOffer => {
      const offer = await Offer.createOffer(dummyOffer);
      offer.helper_user_id = 'test';
      const updated = await Offer.updateOffer(offer);
      expect(updated).to.be.null;
    });
  });
  it('should return null if meeting_date is not tstzrange', async () => {
    await create2UsersandStad(async dummyOffer => {
      const offer = await Offer.createOffer(dummyOffer);
      offer.meeting_date = 'test';
      const updated = await Offer.updateOffer(offer);
      expect(updated).to.be.null;
    });
  });
  it('should not update a offer that does not have all fields', async () => {
    await create2UsersandStad(async dummyOffer => {
      const offer = await Offer.createOffer(dummyOffer);
      for (const [key] of Object.entries(offer)) {
        if (key === 'creation_timestamp') {
          continue;
        }
        const copy = Object.assign({}, offer);
        copy[key] = null;
        const updated = await Offer.updateOffer(copy);
        expect(updated).to.be.null;
      }
    });
  });
});

describe('offerExists', () => {
  it('should return true if offer exists', async () => {
    await create2UsersandStad(async dummyOffer => {
      const offer = await Offer.createOffer(dummyOffer);
      const exists = await Offer.offerExists(offer.offer_id);
      expect(exists).to.be.true;
    });
  });
  it('should return false if offer doesnt exist', async () => {
    const exists = await Offer.offerExists(Number.MAX_SAFE_INTEGER);
    expect(exists).to.be.false;
  });
});
