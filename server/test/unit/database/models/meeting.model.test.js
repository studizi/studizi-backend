// @ts-nocheck
/* eslint-disable no-unused-expressions */
/* eslint-disable no-undef */
/* eslint-disable camelcase */
const chai = require('chai');
const User = require('../../../../database/models/user.model');
const Stud = require('../../../../database/models/stud.model');
const Offer = require('../../../../database/models/offer.model');
const Meeting = require('../../../../database/models/meeting.model');
const Credit = require('../../../../database/models/credit.model');
const emailGenerator = require('../../../utils/email.generator').emailGenerator;
const moment = require('moment');
const expect = chai.expect;
chai.use(require('chai-like'));
chai.use(require('chai-things'));

const dummyUser = {
  first_name: 'test',
  last_name: 'db',
  password: 'pass',
  username: 'user',
  birthdate: '2020-01-01',
  alert_tags: ['java', 'c++'],
  profile_picture: 'google.com'
};
const dummyStud = {
  title: 'test',
  degree_level: 'M2',
  tags: ['programming', 'math', 'english'],
  problem_description:
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
  attachments: ['test.test.fr'],
  deadline: moment()
    .add(1, 'day')
    .toISOString(),
  availability_list: [
    '[2021-01-21 14:30:00+00,2021-01-21 15:30:00+00)',
    '[2021-01-22 14:30,2021-01-22 15:30]'
  ],
  stud_owner_id: 0,
  silver_credit_cost: 0.5,
  gold_credit_cost: 0.1,
  status: 'AVAILABLE'
};
const dummyOffer = {
  meeting_date: '["2021-01-21 14:30:00+00","2021-01-21 15:30:00+00")',
  status: 'ACCEPTED'
};
const dummyMeeting = {
  channel_link: 'google.com',
  channel_id: '1234'
};

const smartMeeting = {
  channel_link: 'google.fr',
  channel_id: '1234'
};

beforeEach(() => {
  dummyUser.email = emailGenerator();
});

async function create2UsersStudAndOffer (cb) {
  const user = await createUser(dummyUser);
  dummyUser.email = emailGenerator();
  const user2 = await createUser(dummyUser);
  dummyStud.stud_owner_id = user.user_id;
  const stud = await Stud.createStud(dummyStud);
  dummyOffer.stud_id = stud.stud_id;
  dummyOffer.helper_user_id = user2.user_id;
  const offer = await Offer.createOffer(dummyOffer);
  dummyMeeting.offer_id = offer.offer_id;
  await cb(dummyMeeting, stud, offer, user, user2);
}

async function createUser (newUser) {
  const user = await User.createUser(newUser);
  await Credit.addCredit(user.user_id, 0, 50);
  return user;
}

describe('Create Meeting', async () => {
  it('should create a meeting using the function createMeeting', async () => {
    await create2UsersStudAndOffer(async dummyMeeting => {
      const meeting = await Meeting.createMeeting(dummyMeeting);

      delete meeting.creation_timestamp;
      expect(meeting.offer_id)
        .to.be.a('number')
        .and.to.be.greaterThan(0);

      expect(meeting)
        .to.be.an('object')
        .that.eql(dummyMeeting);
    });
  });
  it('should parse an injected sql query in channel_link', async () => {
    await create2UsersStudAndOffer(async dummyMeeting => {
      const copy = Object.assign({}, dummyMeeting);
      copy.channel_link = '"(DELETE FROM "stdz"."MEETING")';
      const meeting = await Meeting.createMeeting(copy);
      expect(meeting).to.be.an('object');
    });
  });
  it('should return null if offer_id does not exist', async () => {
    await create2UsersStudAndOffer(async dummyMeeting => {
      const copy = Object.assign({}, dummyMeeting);
      copy.offer_id = Number.MAX_SAFE_INTEGER;
      const meeting = await Meeting.createMeeting(copy);
      expect(meeting).to.be.null;
    });
  });
  it('should return null if offer_id is not int', async () => {
    await create2UsersStudAndOffer(async dummyMeeting => {
      const copy = Object.assign({}, dummyMeeting);
      copy.offer_id = 'test';
      const meeting = await Meeting.createMeeting(copy);
      expect(meeting).to.be.null;
    });
  });
  it('should not create a meeting that does not have all fields', async () => {
    await create2UsersStudAndOffer(async dummyMeeting => {
      for (const [key] of Object.entries(dummyMeeting)) {
        const copy = Object.assign({}, dummyMeeting);
        copy[key] = null;
        const meeting = await Meeting.createMeeting(copy);
        expect(meeting).to.be.null;
      }
    });
  });
});

describe('Delete Meeting', async () => {
  it('should delete a meeting using the function deleteMeeting', async () => {
    await create2UsersStudAndOffer(async dummyMeeting => {
      const meeting = await Meeting.createMeeting(dummyMeeting);
      const deleted = await Meeting.deleteMeeting(meeting.offer_id);
      expect(deleted)
        .to.be.an('object')
        .that.eql(meeting);
    });
  });
  it('should return null if meeting does not exist', async () => {
    const deleted = await Meeting.deleteMeeting(Number.MAX_SAFE_INTEGER);
    expect(deleted).to.be.a.null;
  });
  it('should return null if offer_id is not int', async () => {
    const deleted = await Meeting.deleteMeeting('test');
    expect(deleted).to.be.a.null;
  });
});

describe('Get Meeting', async () => {
  it('should get an Meeting using the function getMeeting', async () => {
    await create2UsersStudAndOffer(async dummyMeeting => {
      const meeting = await Meeting.createMeeting(dummyMeeting);
      const selected = await Meeting.getMeetingByOfferId(meeting.offer_id);
      expect(selected)
        .to.be.an('object')
        .that.eql(meeting);
    });
  });
  it('should return null if Meeting does not exist', async () => {
    const selected = await Meeting.getMeetingByOfferId(Number.MAX_SAFE_INTEGER);
    expect(selected).to.be.a.null;
  });
  it('should return null if offer_id is not int', async () => {
    const selected = await Meeting.getMeetingByOfferId('test');
    expect(selected).to.be.a.null;
  });
  it('should get list of all meetings (without additional params)', async () => {
    await create2UsersStudAndOffer(
      async (dummyMeeting, stud, offer, user, user2) => {
        const nb = 5;
        const studs = [];
        const offers = [];
        const meetings = [];
        for (let i = 0; i < nb; i++) {
          if (i % 2 === 0) {
            dummyStud.stud_owner_id = user.user_id;
            dummyOffer.helper_user_id = user2.user_id;
          } else {
            dummyStud.stud_owner_id = user2.user_id;
            dummyOffer.helper_user_id = user.user_id;
          }
          const tmpStud = await Stud.createStud(dummyStud);
          studs.push(tmpStud);
          dummyOffer.stud_id = tmpStud.stud_id;
          const tmpOffer = await Offer.createOffer(dummyOffer);
          offers.push(tmpOffer);
          dummyMeeting.offer_id = tmpOffer.offer_id;
          dummyMeeting.creation_timestamp = moment.utc().toISOString();
          meetings.push(await Meeting.createMeeting(dummyMeeting));
        }

        const expectedResult = meetings.map(m => {
          const offer = offers.filter(o => o.offer_id === m.offer_id)[0];
          const stud = studs.filter(s => s.stud_id === offer.stud_id)[0];
          return {
            channel_link: m.channel_link,
            creation_timestamp: m.creation_timestamp,
            helper_user_id: offer.helper_user_id,
            meeting_date: offer.meeting_date,
            offer_id: offer.offer_id,
            stud_id: stud.stud_id,
            stud_owner_id: stud.stud_owner_id
          };
        });
        const res = await Meeting.getMeetingList(
          moment.utc().toISOString(),
          nb
        );
        expect(res.sort((a, b) => a.offer_id - b.offer_id)).to.be.eql(expectedResult.sort((a, b) => a.offer_id - b.offer_id));
      }
    );
  });
  it('should get list of all meetings relevant to a specific helper', async () => {
    await create2UsersStudAndOffer(
      async (dummyMeeting, stud, offer, user, user2) => {
        const nb = 5;
        const studs = [];
        const offers = [];
        const meetings = [];
        for (let i = 0; i < nb; i++) {
          if (i % 2 === 0) {
            dummyStud.stud_owner_id = user.user_id;
            dummyOffer.helper_user_id = user2.user_id;
          } else {
            dummyStud.stud_owner_id = user2.user_id;
            dummyOffer.helper_user_id = user.user_id;
          }
          const tmpStud = await Stud.createStud(dummyStud);
          studs.push(tmpStud);
          dummyOffer.stud_id = tmpStud.stud_id;
          const tmpOffer = await Offer.createOffer(dummyOffer);
          offers.push(tmpOffer);
          dummyMeeting.offer_id = tmpOffer.offer_id;
          dummyMeeting.creation_timestamp = moment.utc().toISOString();
          meetings.push(await Meeting.createMeeting(dummyMeeting));
        }
        const helperOffers = offers.filter(o => (o.helper_user_id === user.user_id));
        const expectedResult = helperOffers.map(offer => {
          const meeting = meetings.filter(m => m.offer_id === offer.offer_id)[0];
          const stud = studs.filter(s => s.stud_id === offer.stud_id)[0];
          return {
            channel_link: meeting.channel_link,
            creation_timestamp: meeting.creation_timestamp,
            helper_user_id: offer.helper_user_id,
            meeting_date: offer.meeting_date,
            offer_id: offer.offer_id,
            stud_id: stud.stud_id,
            stud_owner_id: stud.stud_owner_id
          };
        });
        const res = await Meeting.getMeetingList(
          moment.utc().toISOString(),
          nb,
          0,
          -1,
          user.user_id
        );
        expect(res.sort((a, b) => a.offer_id - b.offer_id)).to.be.eql(expectedResult.sort((a, b) => a.offer_id - b.offer_id));
      }
    );
  });
  it('should get list of all meetings relevant to a specific seeker', async () => {
    await create2UsersStudAndOffer(
      async (dummyMeeting, stud, offer, user, user2) => {
        const nb = 5;
        const studs = [];
        const offers = [];
        const meetings = [];
        for (let i = 0; i < nb; i++) {
          if (i % 2 === 0) {
            dummyStud.stud_owner_id = user.user_id;
            dummyOffer.helper_user_id = user2.user_id;
          } else {
            dummyStud.stud_owner_id = user2.user_id;
            dummyOffer.helper_user_id = user.user_id;
          }
          const tmpStud = await Stud.createStud(dummyStud);
          studs.push(tmpStud);
          dummyOffer.stud_id = tmpStud.stud_id;
          const tmpOffer = await Offer.createOffer(dummyOffer);
          offers.push(tmpOffer);
          dummyMeeting.offer_id = tmpOffer.offer_id;
          dummyMeeting.creation_timestamp = moment.utc().toISOString();
          meetings.push(await Meeting.createMeeting(dummyMeeting));
        }
        const seekerStuds = studs.filter(s => s.stud_owner_id === user.user_id);
        const expectedResult = seekerStuds.map(stud => {
          const offer = offers.filter(o => o.stud_id === stud.stud_id)[0];
          const meeting = meetings.filter(m => m.offer_id === offer.offer_id)[0];
          return {
            channel_link: meeting.channel_link,
            creation_timestamp: meeting.creation_timestamp,
            helper_user_id: offer.helper_user_id,
            meeting_date: offer.meeting_date,
            offer_id: offer.offer_id,
            stud_id: stud.stud_id,
            stud_owner_id: stud.stud_owner_id
          };
        });
        const res = await Meeting.getMeetingList(
          moment.utc().toISOString(),
          nb,
          0,
          -1,
          -1,
          user.user_id
        );
        expect(res.sort((a, b) => a.offer_id - b.offer_id)).to.be.eql(expectedResult.sort((a, b) => a.offer_id - b.offer_id));
      }
    );
  });
  it('should get a meeting relevant to a specific offer', async () => {
    await create2UsersStudAndOffer(
      async (dummyMeeting, stud, offer, user, user2) => {
        const nb = 5;
        const studs = [];
        const offers = [];
        const meetings = [];
        for (let i = 0; i < nb; i++) {
          if (i % 2 === 0) {
            dummyStud.stud_owner_id = user.user_id;
            dummyOffer.helper_user_id = user2.user_id;
          } else {
            dummyStud.stud_owner_id = user2.user_id;
            dummyOffer.helper_user_id = user.user_id;
          }
          const tmpStud = await Stud.createStud(dummyStud);
          studs.push(tmpStud);
          dummyOffer.stud_id = tmpStud.stud_id;
          const tmpOffer = await Offer.createOffer(dummyOffer);
          offers.push(tmpOffer);
          dummyMeeting.offer_id = tmpOffer.offer_id;
          dummyMeeting.creation_timestamp = moment.utc().toISOString();
          meetings.push(await Meeting.createMeeting(dummyMeeting));
        }
        meetings.forEach(async (meeting) => {
          const offer = offers.filter(o => o.offer_id === meeting.offer_id)[0];
          const stud = studs.filter(s => s.stud_id === offer.stud_id)[0];
          const expectedResult = {
            channel_link: meeting.channel_link,
            creation_timestamp: meeting.creation_timestamp,
            helper_user_id: offer.helper_user_id,
            meeting_date: offer.meeting_date,
            offer_id: offer.offer_id,
            stud_id: stud.stud_id,
            stud_owner_id: stud.stud_owner_id
          };
          const res = await Meeting.getMeetingList(
            moment.utc().toISOString(),
            nb,
            0,
            offer.offer_id
          );
          expect(res.sort((a, b) => a.offer_id - b.offer_id)).to.be.eql(expectedResult);
        });
      }
    );
  });
});

describe('Update Meeting', async () => {
  it('should update a meeting using the function updateMeeting', async () => {
    await create2UsersStudAndOffer(async dummyMeeting => {
      const meeting = await Meeting.createMeeting(dummyMeeting);
      meeting.channel_link = smartMeeting.channel_link;
      const updated = await Meeting.updateMeeting(meeting);

      expect(updated)
        .to.be.an('object')
        .and.to.eql(meeting);
    });
  });
  it('should parse an injected sql query in channel_link', async () => {
    await create2UsersStudAndOffer(async dummyMeeting => {
      const meeting = await Meeting.createMeeting(dummyMeeting);
      meeting.channel_link = '"(DELETE FROM "stdz"."MEETING")';
      const updated = await Meeting.updateMeeting(meeting);
      expect(updated).to.be.an('object');
    });
  });
  it('should return null if offer_id does not exist', async () => {
    await create2UsersStudAndOffer(async dummyMeeting => {
      const meeting = await Meeting.createMeeting(dummyMeeting);
      meeting.offer_id = Number.MAX_SAFE_INTEGER;
      const updated = await Meeting.updateMeeting(meeting);
      expect(updated).to.be.null;
    });
  });
  it('should return null if offer_id is not int', async () => {
    await create2UsersStudAndOffer(async dummyMeeting => {
      const meeting = await Meeting.createMeeting(dummyMeeting);
      meeting.offer_id = 'test';
      const updated = await Meeting.updateMeeting(meeting);
      expect(updated).to.be.null;
    });
  });
  it('should not update a meeting that does not have all fields', async () => {
    await create2UsersStudAndOffer(async dummyMeeting => {
      const meeting = await Meeting.createMeeting(dummyMeeting);
      for (const [key] of Object.entries(meeting)) {
        if (key === 'creation_timestamp') {
          continue;
        }
        const copy = Object.assign({}, meeting);
        copy[key] = null;
        const updated = await Meeting.updateMeeting(copy);
        expect(updated).to.be.null;
      }
    });
  });
});
describe('meetingExists', () => {
  it('should return true if meeting exists', async () => {
    await create2UsersStudAndOffer(async dummyMeeting => {
      const meeting = await Meeting.createMeeting(dummyMeeting);
      const exists = await Meeting.meetingExists(meeting.offer_id);
      expect(exists).to.be.true;
    });
  });
  it('should return false if meeting doesnt exist', async () => {
    const exists = await Meeting.meetingExists(Number.MAX_SAFE_INTEGER);
    expect(exists).to.be.false;
  });
});
