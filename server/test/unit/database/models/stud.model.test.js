// @ts-nocheck
/* eslint-disable no-unused-expressions */
/* eslint-disable no-undef */
/* eslint-disable camelcase */
const chai = require('chai');
const Stud = require('../../../../database/models/stud.model');
const Credit = require('../../../../database/models/credit.model');
const User = require('../../../../database/models/user.model');
const emailGenerator = require('../../../utils/email.generator').emailGenerator;
const expect = chai.expect;
const moment = require('moment');
const knex = require('../../../../loaders/knex.loader');
chai.use(require('chai-like'));
chai.use(require('chai-things'));

const dummyUser = {
  first_name: 'test',
  last_name: 'db',
  password: 'pass',
  username: 'user',
  birthdate: '2020-01-01',
  alert_tags: ['java', 'c++'],
  profile_picture: 'google.com'
};

const dummyStud = {
  title: 'test',
  degree_level: 'M2',
  tags: ['programming', 'math', 'english'],
  problem_description:
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
  attachments: ['test.test.fr'],
  deadline: moment()
    .add(1, 'day')
    .toISOString(),
  availability_list: [
    '[2021-01-21 14:30,2021-01-21 15:30)',
    '[2021-01-22 14:30,2021-01-22 15:30]'
  ],
  stud_owner_id: 0,
  silver_credit_cost: 0.5,
  gold_credit_cost: 0.1,
  status: 'AVAILABLE'
};

const smartStud = {
  title: 'notest',
  degree_level: 'L3',
  tags: ['c++', 'math', 'java'],
  problem_description:
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
  attachments: ['test.tes.fr'],
  deadline: moment()
    .add(1, 'day')
    .toISOString(),
  availability_list: [
    '[2021-01-22 14:30,2021-01-22 15:30)',
    '[2021-01-23 14:30,2021-01-23 15:30]'
  ],
  stud_owner_id: 0,
  silver_credit_cost: 0.8,
  gold_credit_cost: 0.2,
  status: 'AVAILABLE'
};

beforeEach(() => {
  dummyUser.email = emailGenerator();
});

async function createUser (newUser) {
  const user = await User.createUser(newUser);
  await Credit.addCredit(user.user_id, 0, 50);
  return user;
}

describe('Create Stud', async () => {
  it('should create a stud using the function createStud', async () => {
    const user = await createUser(dummyUser);
    dummyStud.stud_owner_id = user.user_id;
    const stud = await Stud.createStud(dummyStud);

    expect(stud.stud_id)
      .to.be.a('number')
      .and.to.be.greaterThan(0);

    delete stud.stud_id;
    delete stud.creation_timestamp;

    expect(stud)
      .to.be.an('object')
      .and.eql(dummyStud);
  });
  it('should create a stud without attachments', async () => {
    const user = await createUser(dummyUser);
    dummyStud.stud_owner_id = user.user_id;
    dummyStud.attachments = null;
    const stud = await Stud.createStud(dummyStud);

    expect(stud.stud_id)
      .to.be.a('number')
      .and.to.be.greaterThan(0);

    delete stud.stud_id;
    delete stud.creation_timestamp;

    expect(stud)
      .to.be.an('object')
      .and.eql(dummyStud);
  });
  it('should not create a stud that does not have all fields', async () => {
    for (const [key] of Object.entries(dummyStud)) {
      if (key === 'attachments') {
        continue;
      }
      const copy = Object.assign({}, dummyStud);
      copy[key] = null;
      const stud = await Stud.createStud(copy);
      expect(stud).to.be.null;
    }
  });
  it('should not create a stud if the deadline is not date', async () => {
    const copy = Object.assign({}, dummyStud);
    copy.deadline = 'string';
    const stud = await Stud.createStud(copy);
    expect(stud).to.be.null;
  });
  it('should not create a stud if the value inside the availability_list is not daterange', async () => {
    const copy = Object.assign({}, dummyStud);
    copy.availability_list = ['string'];
    const stud = await Stud.createStud(copy);
    expect(stud).to.be.null;
  });
  it('should not create a stud if credit equals zero', async () => {
    const copy = Object.assign({}, dummyStud);
    copy.gold_credit_cost = 0;
    copy.silver_credit_cost = 0;
    const stud = await Stud.createStud(copy);
    expect(stud).to.be.null;
  });
  it('should not create a stud if the tags is not array of strings', async () => {
    const copy = Object.assign({}, dummyStud);
    copy.tags = 'string';
    const stud = await Stud.createStud(copy);
    expect(stud).to.be.null;
  });
  it('should not create a stud if the attachments is not array of strings', async () => {
    const copy = Object.assign({}, dummyStud);
    copy.attachments = 'string';
    const stud = await Stud.createStud(copy);
    expect(stud).to.be.null;
  });
  it('should parse an injected sql query in any field', async () => {
    for (const [key] of Object.entries(dummyStud)) {
      const copy = Object.assign({}, dummyStud);
      if (key === 'tags' || key === 'attachments') {
        copy[key] = ['"(DELETE FROM "stdz"."MEETING")'];
      } else if (
        key === 'title' ||
        key === 'degree_level' ||
        key === 'problem_description'
      ) {
        copy[key] = '"(DELETE FROM "stdz"."MEETING")';
      } else {
        continue;
      }
      const stud = await Stud.createStud(copy);

      expect(stud).to.be.an('object');
      expect(stud[key]).to.eql(copy[key]);
    }
  });
  it('should not create a stud if the user does not exist', async () => {
    dummyStud.stud_owner_id = -1;
    const stud = await Stud.createStud(dummyStud);
    expect(stud).to.be.null;
  });
});

describe('Delete Stud', async () => {
  it('should delete a stud using the function deleteStud', async () => {
    const user = await createUser(dummyUser);
    dummyStud.stud_owner_id = user.user_id;
    const stud = await Stud.createStud(dummyStud);
    const deleted = await Stud.deleteStud(stud.stud_id);
    expect(deleted)
      .to.be.an('object')
      .and.to.eql(stud);
    const existing = await Stud.deleteStud(stud.stud_id);
    expect(existing).to.be.null;
  });
  it('should return null if stud does not exist', async () => {
    const selected = await Stud.deleteStud(Number.MAX_SAFE_INTEGER);
    expect(selected).to.be.a.null;
  });
  it('should return null if stud_id is not int', async () => {
    const user = await createUser(dummyUser);
    dummyStud.stud_owner_id = user.user_id;
    const stud = await Stud.createStud(dummyStud);
    const selected = await Stud.deleteStud(stud.stud_id + 'test');
    expect(selected).to.be.a.null;
  });
});

describe('Get Stud', async () => {
  it('should get a stud using the function getStud', async () => {
    const user = await createUser(dummyUser);
    dummyStud.stud_owner_id = user.user_id;
    const stud = await Stud.createStud(dummyStud);
    const selected = await Stud.getStud(stud.stud_id);
    expect(selected)
      .to.be.an('object')
      .and.to.eql(stud);
  });
  it('should return null if stud does not exist', async () => {
    const selected = await Stud.getStud(Number.MAX_SAFE_INTEGER);
    expect(selected).to.be.a.null;
  });
  it('should return null if stud_id is not int', async () => {
    const user = await createUser(dummyUser);
    dummyStud.stud_owner_id = user.user_id;
    const stud = await Stud.createStud(dummyStud);
    const selected = await Stud.getStud(stud.stud_id + 'test');
    expect(selected).to.be.a.null;
  });
  it('should get a list of stud using getStudList', async () => {
    const user = await createUser(dummyUser);
    const studs = [];
    dummyStud.stud_owner_id = user.user_id;
    for (let i = 0; i < 4; i++) {
      dummyStud.creation_timestamp = moment.utc().toISOString();
      const tmp = await Stud.createStud(dummyStud);
      delete tmp.attachments;
      delete tmp.availability_list;
      // delete tmp.creation_timestamp;
      // delete tmp.deadline;
      delete tmp.degree_level;
      // delete tmp.gold_credit_cost;
      // delete tmp.problem_description;
      // delete tmp.silver_credit_cost;
      // delete tmp.status;
      // delete tmp.stud_id;
      // delete tmp.stud_owner_id;
      // delete tmp.tags;
      // delete tmp.title;

      studs.push(tmp);
    }
    const timestamp = moment.utc().toISOString();
    let returned = await Stud.getStudList(timestamp, 4, 0);
    expect(returned.sort((a, b) => a.stud_id - b.stud_id)).to.be.eql(
      studs.sort((a, b) => a.stud_id - b.stud_id)
    );

    returned = await Stud.getStudList(timestamp, 4, 0, studs[0].tags);
    expect(returned.sort((a, b) => a.stud_id - b.stud_id)).to.be.eql(
      studs.sort((a, b) => a.stud_id - b.stud_id)
    );

    returned = await Stud.getStudList(timestamp, 4, 0, [], user.user_id);
    expect(returned.sort((a, b) => a.stud_id - b.stud_id)).to.be.eql(
      studs.sort((a, b) => a.stud_id - b.stud_id)
    );
  });
});

describe('Update Stud', async () => {
  it('should update a stud using the function updateStud', async () => {
    const user = await createUser(dummyUser);
    const stud = await Stud.createStud(dummyStud);
    smartStud.stud_owner_id = user.user_id;
    smartStud.stud_id = stud.stud_id;

    const updated = await Stud.updateStud(smartStud);
    delete updated.creation_timestamp;
    expect(updated)
      .to.be.an('object')
      .and.to.eql(smartStud);
  });
  it('should update a stud without attachments', async () => {
    const user = await createUser(dummyUser);
    const stud = await Stud.createStud(dummyStud);
    smartStud.stud_owner_id = user.user_id;
    smartStud.stud_id = stud.stud_id;

    smartStud.attachments = null;
    const updated = await Stud.updateStud(smartStud);
    delete updated.creation_timestamp;
    expect(updated)
      .to.be.an('object')
      .and.eql(smartStud);
  });
  it('should not update a stud that does not have all fields', async () => {
    const user = await createUser(dummyUser);
    const stud = await Stud.createStud(dummyStud);
    smartStud.stud_owner_id = user.user_id;
    smartStud.stud_id = stud.stud_id;

    for (const [key] of Object.entries(smartStud)) {
      if (key === 'attachments') {
        continue;
      }
      const copy = Object.assign({}, smartStud);
      copy[key] = null;
      const updated = await Stud.updateStud(copy);
      expect(updated).to.be.null;
    }
  });
  it('should not update a stud if the deadline is not date', async () => {
    const user = await createUser(dummyUser);
    const stud = await Stud.createStud(dummyStud);
    smartStud.stud_owner_id = user.user_id;
    smartStud.stud_id = stud.stud_id;

    const copy = Object.assign({}, smartStud);
    copy.deadline = 'string';
    const updated = await Stud.updateStud(copy);
    expect(updated).to.be.null;
  });
  it('should not update a stud if the value inside the availability_list is not daterange', async () => {
    const user = await createUser(dummyUser);
    const stud = await Stud.createStud(dummyStud);
    smartStud.stud_owner_id = user.user_id;
    smartStud.stud_id = stud.stud_id;

    const copy = Object.assign({}, smartStud);
    copy.availability_list = ['string'];
    const updated = await Stud.updateStud(copy);
    expect(updated).to.be.null;
  });
  it('should not update a stud if credit equal zero', async () => {
    const user = await createUser(dummyUser);
    const stud = await Stud.createStud(dummyStud);
    smartStud.stud_owner_id = user.user_id;
    smartStud.stud_id = stud.stud_id;

    const copy = Object.assign({}, smartStud);
    copy.gold_credit_cost = 0;
    copy.silver_credit_cost = 0;
    const updated = await Stud.updateStud(copy);
    expect(updated).to.be.null;
  });
  it('should not update a stud if the tags is not array of strings', async () => {
    const user = await createUser(dummyUser);
    const stud = await Stud.createStud(dummyStud);
    smartStud.stud_owner_id = user.user_id;
    smartStud.stud_id = stud.stud_id;

    const copy = Object.assign({}, smartStud);
    copy.tags = 'string';
    const updated = await Stud.updateStud(copy);
    expect(updated).to.be.null;
  });
  it('should not update a stud if the attachments is not array of strings', async () => {
    const user = await createUser(dummyUser);
    const stud = await Stud.createStud(dummyStud);
    smartStud.stud_owner_id = user.user_id;
    smartStud.stud_id = stud.stud_id;

    const copy = Object.assign({}, smartStud);
    copy.attachments = 'string';
    const updated = await Stud.updateStud(copy);
    expect(updated).to.be.null;
  });
  it('should parse an injected sql query in any field', async () => {
    const user = await createUser(dummyUser);
    const stud = await Stud.createStud(dummyStud);
    smartStud.stud_owner_id = user.user_id;
    smartStud.stud_id = stud.stud_id;

    for (const [key] of Object.entries(smartStud)) {
      const copy = Object.assign({}, smartStud);
      if (key === 'tags' || key === 'attachments') {
        copy[key] = ['"(DELETE FROM "stdz"."MEETING")'];
      } else if (
        key === 'title' ||
        key === 'degree_level' ||
        key === 'problem_description'
      ) {
        copy[key] = '"(DELETE FROM "stdz"."MEETING")';
      } else {
        continue;
      }
      const updated = await Stud.updateStud(copy);

      expect(updated).to.be.an('object');
      expect(updated[key]).to.eql(copy[key]);
    }
  });
  it('should not update a stud if the user does not exist', async () => {
    const stud = await Stud.createStud(dummyStud);
    smartStud.stud_owner_id = -1;
    smartStud.stud_id = stud.stud_id;

    const updated = await Stud.updateStud(smartStud);
    expect(updated).to.be.null;
  });
});
describe('studsExists', () => {
  it('should return true if stud exists', async () => {
    const stud = await Stud.createStud(dummyStud);
    const exists = await Stud.studExists(stud.stud_id);
    expect(exists).to.be.true;
  });
  it('should return false if stud doesnt exist', async () => {
    const exists = await Stud.studExists(Number.MAX_SAFE_INTEGER);
    expect(exists).to.be.false;
  });
});
describe('Stud exists', () => {
  it('should return true if exists', async () => {
    const stud = await Stud.createStud(dummyStud);
    const exists = await Stud.studExists(stud.stud_id);
    expect(exists).to.be.true;
  });
  it('should return false if doesnt exist', async () => {
    const queryResult = await knex.raw(
      'SELECT MAX(stud_id)+1 AS "id" FROM "stdz"."STUD"'
    );
    const exists = await Stud.studExists(queryResult.rows[0].id);
    expect(exists).to.be.false;
  });
});
