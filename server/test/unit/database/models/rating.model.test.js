// @ts-nocheck
/* eslint-disable no-unused-expressions */
/* eslint-disable no-undef */
/* eslint-disable camelcase */
const chai = require('chai');
const User = require('../../../../database/models/user.model');
const Rating = require('../../../../database/models/rating.model');
const Offer = require('../../../../database/models/offer.model');
const Meeting = require('../../../../database/models/meeting.model');
const Stud = require('../../../../database/models/stud.model');
const moment = require('moment');
const emailGenerator = require('../../../utils/email.generator').emailGenerator;
const expect = chai.expect;
chai.use(require('chai-like'));
chai.use(require('chai-things'));
const usersDataset = require('../../../resources/users.dataset.json');
const studsDataset = require('../../../resources/studs.dataset.json');
const offersDataset = require('../../../resources/offers.dataset.json');
const ratingDataset = require('../../../resources/rating.dataset.json');
const meetingDataset = require('../../../resources/meeting.dataset.json');

let user, user2, stud, offer, meeting;

const dummyUser = usersDataset.success[0].data;
delete dummyUser.user_id;

const dummyStud = studsDataset.success[2].data;
delete dummyStud.stud_id;

const dummyOffer = offersDataset.success[0].data;
delete dummyOffer.offer_id;

const dummyMeeting = meetingDataset.success[0].data;
delete dummyMeeting.offer_id;

const dummyRating = { ...ratingDataset.success[0].data };
const smartRating = ratingDataset.success[1].data;

beforeEach(() => {
  dummyUser.email = emailGenerator();
});

async function createUsersAndStud () {
  dummyUser.email = emailGenerator();
  user = await User.createUser(dummyUser);
  dummyUser.email = emailGenerator();
  user2 = await User.createUser(dummyUser);
  dummyStud.stud_owner_id = user.user_id;
  dummyStud.deadline = moment()
    .add(1, 'day')
    .toISOString();
  dummyStud.status = 'AVAILABLE';
  stud = await Stud.createStud(dummyStud);
}

async function createDummyMeeting (cb) {
  await createUsersAndStud();
  dummyOffer.stud_id = stud.stud_id;
  dummyOffer.helper_user_id = user2.user_id;
  offer = await Offer.createOffer(dummyOffer);
  dummyMeeting.offer_id = offer.offer_id;
  meeting = await Meeting.createMeeting(dummyMeeting);
  dummyRating.offer_id = meeting.offer_id;
  dummyRating.rated_user_id = offer.helper_user_id;
  dummyRating.rating_user_id = stud.stud_owner_id;
  await cb(dummyRating);
}

async function create2UsersStudAndOffer (cb) {
  const user = await User.createUser(dummyUser);
  dummyUser.email = emailGenerator();
  const user2 = await User.createUser(dummyUser);
  dummyStud.stud_owner_id = user.user_id;
  const stud = await Stud.createStud(dummyStud);
  dummyOffer.stud_id = stud.stud_id;
  dummyOffer.helper_user_id = user2.user_id;
  const offer = await Offer.createOffer(dummyOffer);
  dummyMeeting.offer_id = offer.offer_id;
  meeting = await Meeting.createMeeting(dummyMeeting);
  dummyRating.offer_id = meeting.offer_id;
  dummyRating.rated_user_id = offer.helper_user_id;
  dummyRating.rating_user_id = stud.stud_owner_id;
  const dummyRated = { ...ratingDataset.success[0].data };
  dummyRated.offer_id = meeting.offer_id;
  dummyRated.rated_user_id = stud.stud_owner_id;
  dummyRated.rating_user_id = offer.helper_user_id;
  dummyRated.type = 'SEEKER';
  await cb(dummyRating, dummyRated, user, user2);
}

describe('Create Rating', async () => {
  it('should create a rating using the function createRating', async () => {
    await createDummyMeeting(async dummyRating => {
      const rating = await Rating.createRating(dummyRating);

      expect(rating.rating_id)
        .to.be.a('number')
        .and.to.be.greaterThan(0);

      delete rating.rating_id;

      expect(rating)
        .to.be.an('object')
        .that.eql(dummyRating);
    });
  });
  it('should parse an injected sql query in comment', async () => {
    await createDummyMeeting(async dummyRating => {
      const copy = Object.assign({}, dummyRating);
      copy.comment = '"(DELETE FROM "stdz"."MEETING")';
      const rating = await Rating.createRating(copy);
      expect(rating).to.be.an('object');
    });
  });
  it('should return null if rating, rated_user_id or rating_user_id are not int', async () => {
    await createDummyMeeting(async dummyRating => {
      const keys = ['rating', 'rated_user_id', 'rated_user_id'];
      for (key of keys) {
        const copy = Object.assign({}, dummyRating);
        copy[key] = 'test';
        const rating = await Rating.createRating(copy);
        expect(rating).to.be.null;
      }
    });
  });
  it('should return null if rated_user_id or rating_user_id do not exist', async () => {
    await createDummyMeeting(async dummyRating => {
      const keys = ['rated_user_id', 'rated_user_id'];
      for (key of keys) {
        const copy = Object.assign({}, dummyRating);
        copy[key] = Number.MAX_SAFE_INTEGER;
        const rating = await Rating.createRating(copy);
        expect(rating).to.be.null;
      }
    });
  });
  it('should return null if rating is less than 1', async () => {
    await createDummyMeeting(async dummyRating => {
      const copy = Object.assign({}, dummyRating);
      copy.rating = 0;
      const rating = await Rating.createRating(copy);
      expect(rating).to.be.null;
    });
  });
  it('should return null if rating is greater than 5', async () => {
    await createDummyMeeting(async dummyRating => {
      const copy = Object.assign({}, dummyRating);
      copy.rating = 6;
      const rating = await Rating.createRating(copy);
      expect(rating).to.be.null;
    });
  });
  it('should return null if status is not in type', async () => {
    await createDummyMeeting(async dummyRating => {
      const copy = Object.assign({}, dummyRating);
      copy.type = 'test';
      const rating = await Rating.createRating(copy);
      expect(rating).to.be.null;
    });
  });
  it('should parse an injected sql query in status', async () => {
    await createDummyMeeting(async dummyRating => {
      const copy = Object.assign({}, dummyRating);
      copy.type = '"(DELETE FROM "stdz"."MEETING")';
      const rating = await Rating.createRating(copy);
      expect(rating).to.be.null;
    });
  });
  it('should not create an offer that does not have all fields', async () => {
    await createDummyMeeting(async dummyRating => {
      for (const [key] of Object.entries(dummyRating)) {
        if (key === 'comment') {
          continue;
        }
        const copy = Object.assign({}, dummyRating);
        copy[key] = null;
        const rating = await Rating.createRating(copy);
        expect(rating).to.be.null;
      }
    });
  });
});
describe('Delete Rating', async () => {
  it('should delete a rating using the function deleteRating', async () => {
    await createDummyMeeting(async dummyRating => {
      const rating = await Rating.createRating(dummyRating);
      const deleted = await Rating.deleteRating(rating.rating_id);
      expect(deleted)
        .to.be.an('object')
        .that.eql(rating);
    });
  });
  it('should return null if rating does not exist', async () => {
    const deleted = await Rating.deleteRating(Number.MAX_SAFE_INTEGER);
    expect(deleted).to.be.a.null;
  });
  it('should return null if rating_id is not int', async () => {
    const deleted = await Rating.deleteRating('test');
    expect(deleted).to.be.a.null;
  });
});
describe('Get Rating', async () => {
  it('should get a rating using the function getRating', async () => {
    await createDummyMeeting(async dummyRating => {
      const rating = await Rating.createRating(dummyRating);
      const selected = await Rating.getRating(rating.rating_id);
      expect(selected)
        .to.be.an('object')
        .that.eql(rating);
    });
  });
  it('should return null if rating does not exist', async () => {
    const deleted = await Rating.getRating(Number.MAX_SAFE_INTEGER);
    expect(deleted).to.be.a.null;
  });
  it('should return null if rating_id is not int', async () => {
    const deleted = await Rating.getRating('test');
    expect(deleted).to.be.a.null;
  });
  it('should return null if offer_id is not int', async () => {
    const deleted = await Rating.getRatingList('test');
    expect(deleted).to.be.a.null;
  });
  it('Should return all ratings with offer_id', async () => {
    await create2UsersStudAndOffer(
      async (dummyRating, dummyRated, user, user2) => {
        await Rating.createRating(dummyRating);
        await Rating.createRating(dummyRated);
        const result = await Rating.getRatingList(dummyRating.offer_id);
        const ratings = [];
        for (const res of result) {
          delete res.rating_id;
        }
        ratings.push(dummyRated);
        ratings.push(dummyRating);
        expect(result.sort((a, b) => a.rated_user_id - b.rated_user_id)).to.be.eql(ratings.sort((a, b) => a.rated_user_id - b.rated_user_id));
      }
    );
  });
  it('Should return ratings relevant to rating_user_id', async () => {
    await create2UsersStudAndOffer(
      async (dummyRating, dummyRated, user, user2) => {
        await Rating.createRating(dummyRating);
        await Rating.createRating(dummyRated);
        const result = await Rating.getRatingList(-1, user.user_id);
        for (const res of result) {
          delete res.rating_id;
        }
        const ratings = [];
        ratings.push(dummyRating);
        expect(result.sort((a, b) => a.rated_user_id - b.rated_user_id)).to.be.eql(ratings.sort((a, b) => a.rated_user_id - b.rated_user_id));
      }
    );
  });
  it('Should return ratings relevant to rated_user_id', async () => {
    await create2UsersStudAndOffer(
      async (dummyRating, dummyRated, user, user2) => {
        await Rating.createRating(dummyRating);
        await Rating.createRating(dummyRated);
        const result = await Rating.getRatingList(-1, -1, user.user_id);
        for (const res of result) {
          delete res.rating_id;
        }
        const ratings = [];
        ratings.push(dummyRated);
        expect(result.sort((a, b) => a.rated_user_id - b.rated_user_id)).to.be.eql(ratings.sort((a, b) => a.rated_user_id - b.rated_user_id));
      }
    );
  });
});
describe('Update Rating', async () => {
  it('should update a rating using the function getRating', async () => {
    await createDummyMeeting(async dummyRating => {
      const rating = await Rating.createRating(dummyRating);
      for (const [key] of Object.entries(smartRating)) {
        rating[key] = smartRating[key];
      }
      const updated = await Rating.updateRating(rating);
      expect(updated)
        .to.be.an('object')
        .and.to.eql(rating);
    });
  });
  it('should parse an injected sql query in comment', async () => {
    await createDummyMeeting(async dummyRating => {
      const rating = await Rating.createRating(dummyRating);
      rating.comment = '"(DELETE FROM "stdz"."MEETING")';
      const updated = await Rating.updateRating(rating);
      expect(updated)
        .to.be.an('object')
        .and.to.eql(rating);
    });
  });
  it('should return null if rating, rated_user_id or rating_user_id are not int', async () => {
    await createDummyMeeting(async dummyRating => {
      const rating = await Rating.createRating(dummyRating);
      const keys = ['rating', 'rated_user_id', 'rated_user_id'];
      for (key of keys) {
        const copy = Object.assign({}, rating);
        copy[key] = 'test';
        const updated = await Rating.updateRating(copy);
        expect(updated).to.be.null;
      }
    });
  });
  it('should return null if rated_user_id or rating_user_id do not exist', async () => {
    await createDummyMeeting(async dummyRating => {
      const rating = await Rating.createRating(dummyRating);
      const keys = ['rated_user_id', 'rated_user_id'];
      for (key of keys) {
        const copy = Object.assign({}, rating);
        copy[key] = Number.MAX_SAFE_INTEGER;
        const updated = await Rating.updateRating(copy);
        expect(updated).to.be.null;
      }
    });
  });
  it('should return null if rating is less than 1', async () => {
    await createDummyMeeting(async dummyRating => {
      const rating = await Rating.createRating(dummyRating);
      rating.rating = 0;
      const updated = await Rating.updateRating(rating);
      expect(updated).to.be.null;
    });
  });
  it('should return null if rating is greater than 5', async () => {
    await createDummyMeeting(async dummyRating => {
      const rating = await Rating.createRating(dummyRating);
      rating.rating = 6;
      const updated = await Rating.updateRating(rating);
      expect(updated).to.be.null;
    });
  });
  it('should return null if status is not in type', async () => {
    await createDummyMeeting(async dummyRating => {
      const rating = await Rating.createRating(dummyRating);
      rating.type = 'test';
      const updated = await Rating.updateRating(rating);
      expect(updated).to.be.null;
    });
  });
  it('should parse an injected sql query in status', async () => {
    await createDummyMeeting(async dummyRating => {
      const rating = await Rating.createRating(dummyRating);
      rating.type = '"(DELETE FROM "stdz"."MEETING")';
      const updated = await Rating.updateRating(rating);
      expect(updated).to.be.null;
    });
  });
  it('should not update a rating that does not have all fields', async () => {
    await createDummyMeeting(async dummyRating => {
      const rating = await Rating.createRating(dummyRating);
      for (const [key] of Object.entries(rating)) {
        if (key === 'comment') {
          continue;
        }
        const copy = Object.assign({}, rating);
        copy[key] = null;
        const updated = await Rating.updateRating(copy);
        expect(updated).to.be.null;
      }
    });
  });
});
describe('ratingExists', () => {
  it('should return true if rating exists', async () => {
    await createDummyMeeting(async dummyRating => {
      const rating = await Rating.createRating(dummyRating);
      const exists = await Rating.ratingExists(rating.rating_id);
      expect(exists).to.be.true;
    });
  });
  it('should return false if rating doesnt exist', async () => {
    const exists = await Rating.ratingExists(Number.MAX_SAFE_INTEGER);
    expect(exists).to.be.false;
  });
});
