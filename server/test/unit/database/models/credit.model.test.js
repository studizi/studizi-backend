/* eslint-disable no-unused-expressions */
/* eslint-disable no-undef */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
const chai = require('chai');
const User = require('../../../../database/models/user.model');
const Credit = require('../../../../database/models/credit.model');
const expect = chai.expect;
const initialCredit = require('../../../../utils/constants.json').credits;
const emailGenerator = require('../../../utils/email.generator').emailGenerator;
const dbTools = require('../../../../database/tools/execute.sql.order');
chai.use(require('chai-like'));
chai.use(require('chai-things'));
let user;
const dummyUser = {
  first_name: 'test',
  last_name: 'db',
  password: 'pass',
  username: 'user',
  birthdate: '2020-01-01',
  alert_tags: ['java', 'c++'],
  profile_picture: 'google.com'
};

before(async () => {
  await dbTools.initDb();
});

beforeEach(async () => {
  dummyUser.email = emailGenerator();
});

after(async function () {
  await dbTools.dropDb();
});

describe('Create/Get credit', async () => {
  it('should return the initial credit once a user is created', async () => {
    user = await User.createUser(dummyUser);
    const userCredit = await Credit.getCredit(user.user_id);
    expect(userCredit.silver_credit).to.equal(initialCredit.initial_silver_credit);
    expect(userCredit.gold_credit).to.equal(initialCredit.initial_gold_credit);
  });

  it('should not return credit if user_id does not exist', async () => {
    user = await User.createUser(dummyUser);
    const foundCredit = await Credit.getCredit(Number.MAX_SAFE_INTEGER - 1);
    expect(foundCredit).to.be.null;
  });

  it('should return null if user_id isn\'t an int', async () => {
    user = await User.createUser(dummyUser);
    const foundCredit = await Credit.getCredit('test');
    expect(foundCredit).to.be.null;
  });

  it('should parse SQL script', async () => {
    user = await User.createUser(dummyUser);
    const foundCredit = await Credit.getCredit('"(DELETE FROM "stdz"."CREDIT")');
    expect(foundCredit).to.be.null;
  });
});

describe('Add credit', async () => {
  it('should return updated amount', async () => {
    user = await User.createUser(dummyUser);
    const foundCredit = await Credit.getCredit(user.user_id);
    await Credit.addCredit(user.user_id, 50, 50);
    const updatedCredit = await Credit.getCredit(user.user_id);
    expect(updatedCredit.silver_credit).to.be.eql(
      foundCredit.silver_credit + 50
    );
    expect(updatedCredit.gold_credit).to.be.eql(foundCredit.gold_credit + 50);
  });
  it('should return updated amount if i send only silver credit', async () => {
    user = await User.createUser(dummyUser);
    const foundCredit = await Credit.getCredit(user.user_id);
    await Credit.addCredit(user.user_id, 50);
    const updatedCredit = await Credit.getCredit(user.user_id);
    expect(updatedCredit.silver_credit).to.be.eql(
      foundCredit.silver_credit + 50
    );
  });
  it('should return error if sending text instead of int', async () => {
    user = await User.createUser(dummyUser);
    const foundCredit = await Credit.getCredit(user.user_id);
    await Credit.addCredit(user.user_id, 'test');
    const updatedCredit = await Credit.getCredit(user.user_id);
    expect(updatedCredit).to.be.eql(foundCredit);
  });
  it('should return error if sending text (twice) instead of int', async () => {
    user = await User.createUser(dummyUser);
    const foundCredit = await Credit.getCredit(user.user_id);
    const updatedCredit = await Credit.getCredit(user.user_id);
    expect(updatedCredit).to.be.eql(foundCredit);
  });
  it('should parse SQL as text and not execute it', async () => {
    user = await User.createUser(dummyUser);
    const foundCredit = await Credit.getCredit(user.user_id);
    await Credit.addCredit(user.user_id, '"(DELETE FROM "stdz"."CREDIT")');
    const updatedCredit = await Credit.getCredit(user.user_id);
    expect(updatedCredit).to.be.eql(foundCredit);
  });
  it('should parse SQL as text (twice) and not execute it', async () => {
    user = await User.createUser(dummyUser);
    const foundCredit = await Credit.getCredit(user.user_id);
    await Credit.addCredit(user.user_id, '"(DELETE FROM "stdz"."CREDIT")');
    const updatedCredit = await Credit.getCredit(user.user_id);
    expect(updatedCredit).to.be.eql(foundCredit);
  });
});

describe('Remove credit', async () => {
  it('should return updated amount, normal amount', async () => {
    user = await User.createUser(dummyUser);
    const foundCredit = await Credit.getCredit(user.user_id);
    await Credit.removeCredit(user.user_id, 25);
    const updatedCredit = await Credit.getCredit(user.user_id);
    expect(updatedCredit.silver_credit).to.be.eql(
      foundCredit.silver_credit - 25
    );
  });

  it('should return updated amount, too much', async () => {
    user = await User.createUser(dummyUser);
    await Credit.removeCredit(user.user_id, 51, 52);
    const updatedCredit = await Credit.getCredit(user.user_id);
    expect(updatedCredit.silver_credit).to.be.eql(50);
  });
  it('should return error if sending text instead of int', async () => {
    user = await User.createUser(dummyUser);
    const foundCredit = await Credit.getCredit(user.user_id);
    await Credit.removeCredit(user.user_id, 'test');
    const updatedCredit = await Credit.getCredit(user.user_id);
    expect(updatedCredit).to.be.eql(foundCredit);
  });
  it('should return error if sending text (twice) instead of int', async () => {
    user = await User.createUser(dummyUser);
    const foundCredit = await Credit.getCredit(user.user_id);
    await Credit.removeCredit(user.user_id, 'test', 'test');
    const updatedCredit = await Credit.getCredit(user.user_id);
    expect(updatedCredit).to.be.eql(foundCredit);
  });
  it('should parse SQL as text and not execute it', async () => {
    user = await User.createUser(dummyUser);
    const foundCredit = await Credit.getCredit(user.user_id);
    await Credit.removeCredit(user.user_id, '"(DELETE FROM "stdz"."CREDIT")');
    const updatedCredit = await Credit.getCredit(user.user_id);
    expect(updatedCredit).to.be.eql(foundCredit);
  });
  it('should parse SQL as text (twice) and not execute it', async () => {
    user = await User.createUser(dummyUser);
    const foundCredit = await Credit.getCredit(user.user_id);
    await Credit.removeCredit(user.user_id, '"(DELETE FROM "stdz"."CREDIT")');
    const updatedCredit = await Credit.getCredit(user.user_id);
    expect(updatedCredit).to.be.eql(foundCredit);
  });
});
describe('creditExists', () => {
  it('should return true if credit exists', async () => {
    const user = await User.createUser(dummyUser);
    const foundCredit = await Credit.getCredit(user.user_id);
    const exists = await Credit.creditExists(user.user_id);
    expect(exists).to.be.true;
  });
  it('should return false if credit doesnt exist', async () => {
    const exists = await Credit.creditExists(Number.MAX_SAFE_INTEGER);
    expect(exists).to.be.false;
  });
});
