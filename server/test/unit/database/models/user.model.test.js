/* eslint-disable no-unused-expressions */
/* eslint-disable no-undef */
/* eslint-disable camelcase */
const chai = require('chai');
const User = require('../../../../database/models/user.model');
const dbTools = require('../../../../database/tools/execute.sql.order');
const emailGenerator = require('../../../utils/email.generator').emailGenerator;
const expect = chai.expect;
const knex = require('../../../../loaders/knex.loader');
chai.use(require('chai-like'));
chai.use(require('chai-things'));

const dummyUser = {
  first_name: 'test',
  last_name: 'db',
  password: 'pass',
  username: 'user',
  birthdate: '2020-01-01',
  alert_tags: ['java', 'c++'],
  profile_picture: 'google.com'
};
const smartUser = {
  user_id: 0,
  first_name: 'notest',
  last_name: 'mocha',
  password: 'mlll',
  username: 'ml',
  birthdate: '2020-01-02',
  alert_tags: ['java', 'c#'],
  profile_picture: 'google.fr'
};

beforeEach(() => {
  dummyUser.email = emailGenerator();
  smartUser.email = emailGenerator();
});

describe('Create User', async () => {
  it('should create a user using the function createUser', async () => {
    const copy = Object.assign({}, dummyUser);
    const user = await User.createUser(copy);

    expect(user.user_id)
      .to.be.a('number')
      .and.to.be.greaterThan(0);

    delete user.user_id;
    delete copy.password;

    expect(user)
      .to.be.an('object')
      .that.eql(copy);
  });
  it('should not create a user that does not have all fields', async () => {
    for (const [key] of Object.entries(dummyUser)) {
      const copy = Object.assign({}, dummyUser);
      copy[key] = null;
      const user = await User.createUser(copy);
      expect(user).to.be.null;
    }
  });
  it('should not create a user if the birthdate is not date', async () => {
    const copy = Object.assign({}, dummyUser);
    copy.birthdate = 'string';
    const user = await User.createUser(copy);
    expect(user).to.be.null;
  });
  it('should not create a user if the alert_tags is not array of strings', async () => {
    const copy = Object.assign({}, dummyUser);
    copy.alert_tags = 'string';
    const user = await User.createUser(copy);
    expect(user).to.be.null;
  });

  it('should parse an injected sql query in any field', async () => {
    for (const [key] of Object.entries(dummyUser)) {
      const copy = Object.assign({}, dummyUser);
      copy.email = emailGenerator();
      if (key === 'birthdate' || key === 'email') {
        continue;
      } else if (key === 'alert_tags') {
        copy[key] = ['"(DELETE FROM "stdz"."MEETING")'];
      } else {
        copy[key] = '"(DELETE FROM "stdz"."MEETING")';
      }
      const user = await User.createUser(copy);
      expect(user).to.be.an('object');
      if (key === 'password') {
        continue;
      }
      expect(user[key]).to.eql(copy[key]);
    }
  });
});

describe('Get User', async () => {
  it('should get a user using the function getUser', async () => {
    const user = await User.createUser(dummyUser);
    const selected = await User.getUser(user.user_id);
    expect(selected)
      .to.be.an('object')
      .and.to.eql(user);
  });
  it('should return null if user does not exist', async () => {
    const selected = await User.getUser(Number.MAX_SAFE_INTEGER);
    expect(selected).to.be.a.null;
  });
  it('should return null if user_id is not int', async () => {
    const user = await User.createUser(dummyUser);
    const selected = await User.getUser(user.user_id + 'test');
    expect(selected).to.be.a.null;
  });
});

describe('Get user_id if exist', async () => {
  it('should get a user using the function getUser', async () => {
    const user = await User.createUser(dummyUser);
    const selected_id = await User.getUserIdIfExist(
      dummyUser.email,
      dummyUser.password
    );
    expect(selected_id)
      .to.be.a('number')
      .that.equal(user.user_id);
  });
  it('should return null if password is not correct', async () => {
    await User.createUser(dummyUser);
    const selected_id = await User.getUserIdIfExist(
      dummyUser.email,
      dummyUser.password + '.'
    );
    expect(selected_id).to.be.a.null;
  });
  it('should return null if user does not exist', async () => {
    const selected_id = await User.getUserIdIfExist('test', 'test');
    expect(selected_id).to.be.a.null;
  });
  it('should return null if email and password are null', async () => {
    const selected_id = await User.getUserIdIfExist(null, null);
    expect(selected_id).to.be.a.null;
  });
  it('should parse an injected sql query in email and password', async () => {
    const query = '"(DELETE FROM "stdz"."MEETING")';
    const selected_id = await User.getUserIdIfExist(query, query);
    expect(selected_id).to.be.a.null;
  });
});

describe('Delete User', async () => {
  it('should delete a user using the function deleteUser', async () => {
    const user = await User.createUser(dummyUser);
    const deleted = await User.deleteUser(user.user_id);
    expect(deleted)
      .to.be.an('object')
      .and.to.eql(user);
    const existing = await User.deleteUser(user.user_id);
    expect(existing).to.be.null;
  });
  it('should return null if user does not exist', async () => {
    const selected = await User.deleteUser(Number.MAX_SAFE_INTEGER);
    expect(selected).to.be.a.null;
  });
  it('should return null if user_id is not int', async () => {
    const user = await User.createUser(dummyUser);
    const selected = await User.deleteUser(user.user_id + 'test');
    expect(selected).to.be.a.null;
  });
});

describe('Get Rated User', async () => {
  it('should update a user using the function getRatedUser', async () => {
    const user = await User.createUser(dummyUser);
    delete user.password;
    const rated = await User.getRatedUser(user.user_id);
    expect(rated)
      .to.be.an('object')
      .and.to.eql({
        user_id: user.user_id,
        avg_helper_rating: null,
        avg_seeker_rating: null
      });
  });
  it('should return null if user does not exist', async () => {
    const selected = await User.getRatedUser(Number.MAX_SAFE_INTEGER);
    expect(selected).to.be.a.null;
  });
  it('should return null if user_id is not int', async () => {
    const user = await User.createUser(dummyUser);
    const selected = await User.getRatedUser(user.user_id + 'test');
    expect(selected).to.be.a.null;
  });
});

describe('Update User', async () => {
  it('should update a user using the function updateUser', async () => {
    const user = await User.createUser(dummyUser);
    smartUser.user_id = user.user_id;

    const updated = await User.updateUser(smartUser);
    delete smartUser.password;
    smartUser.verified = true;

    expect(updated)
      .to.be.an('object')
      .and.to.eql(smartUser);
  });
  it('should not update a user that does not have all fields', async () => {
    const user = await User.createUser(dummyUser);
    for (const [key] of Object.entries(user)) {
      user[key] = null;
      const updated = await User.updateUser(user);
      expect(updated).to.be.null;
    }
  });
  it('should not update a user if the birthdate is not date', async () => {
    const user = await User.createUser(dummyUser);
    user.birthdate = 'string';
    const updated = await User.updateUser(user);
    expect(updated).to.be.null;
  });
  it('should not update a user if the alert_tags is not array of strings', async () => {
    const user = await User.createUser(dummyUser);
    user.alert_tags = 'string';
    const updated = await User.updateUser(user);
    expect(updated).to.be.null;
  });
  it('should return null if user_id is not int', async () => {
    const user = await User.createUser(dummyUser);
    const selected = await User.updateUser(user.user_id + 'test');
    expect(selected).to.be.a.null;
  });
  it('should parse an injected sql query in any field', async () => {
    const user = await User.createUser(dummyUser);
    for (const [key] of Object.entries(user)) {
      // eslint-disable-next-line no-use-before-define
      const copy = Object.assign({}, user);
      if (key === 'birthdate' || key === 'user_id' || key === 'verified') {
        continue;
      } else if (key === 'alert_tags') {
        copy[key] = ['"(DELETE FROM "stdz"."MEETING")'];
      } else {
        copy[key] = '"(DELETE FROM "stdz"."MEETING")';
      }
      const updated = await User.updateUser(copy);
      expect(updated).to.be.an('object');
      expect(updated[key]).to.eql(copy[key]);
    }
  });
});
describe('userExists', () => {
  it('should return true if user exists', async () => {
    const user = await User.createUser(dummyUser);
    const exists = await User.userExists(user.user_id);
    expect(exists).to.be.true;
  });
  it('should return false if user doesnt exist', async () => {
    const exists = await User.userExists(Number.MAX_SAFE_INTEGER);
    expect(exists).to.be.false;
  });
});
