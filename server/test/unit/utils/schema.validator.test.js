/* eslint-disable no-undef */
/* eslint-disable no-unused-expressions */
const chai = require('chai');
const expect = chai.expect;
const offersDataset = require('../../resources/offers.dataset.json');
const { isAValidInstance } = require('../../../utils/schema.validator');
const offerSchema = require('../../../database/schemas/offer.schema.json');

describe('Offer Schema', () => {
  it('Valid Inputs', () => {
    offersDataset.success.forEach(success => {
      expect(isAValidInstance(success.data, offerSchema)).to.be.true;
    });
  });
  it('Invalid Inputs', () => {
    offersDataset.failures.forEach(failure => {
      expect(isAValidInstance(failure.data, offerSchema)).to.be.false;
    });
  });
});

describe('Credit Schema', () => {
  // TODO
});
