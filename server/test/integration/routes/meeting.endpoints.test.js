// @ts-nocheck
/* eslint-disable no-undef */
/* eslint-disable camelcase */
const { expect } = require('chai');
const chai = require('chai');
chai.use(require('chai-like'));
chai.use(require('chai-things'));
const emailGenerator = require('../../utils/email.generator').emailGenerator;
const ApiTools = require('../../utils/agent.tools');
const Offer = require('../../../database/models/offer.model');
const Meeting = require('../../../database/models/meeting.model');
const User = require('../../../database/models/user.model');
const Stud = require('../../../database/models/stud.model');
const usersDataset = require('../../resources/users.dataset.json');
const studsDataset = require('../../resources/studs.dataset.json');
const offersDataset = require('../../resources/offers.dataset.json');
const meetingsDataset = require('../../resources/meeting.dataset.json');
const moment = require('moment');
beforeEach(() => {
  usersDataset.success[2].data.email = emailGenerator();
});
let user2, stud;

const dummyUser = usersDataset.success[0].data;
delete dummyUser.user_id;

const dummyStud = studsDataset.success[2].data;
dummyStud.deadline = moment()
  .add(1, 'day')
  .toISOString();

const dummyOffer = offersDataset.success[0].data;
delete dummyOffer.offer_id;

const dummyMeeting = meetingsDataset.success[0].data;

async function createUsersAndStud (user) {
  dummyUser.email = emailGenerator();
  user2 = await User.createUser(dummyUser);
  dummyStud.stud_owner_id = user.user_id;
  stud = await Stud.createStud(dummyStud);
  dummyOffer.stud_id = stud.stud_id;
  dummyOffer.helper_user_id = user2.user_id;
}

async function createDummyMeeting (agent, user) {
  await createUsersAndStud(user);
  const offer = await Offer.createOffer(dummyOffer);
  dummyMeeting.offer_id = offer.offer_id;
  const meeting = await Meeting.createMeeting(dummyMeeting);
  return meeting;
}

async function create2UsersStudAndOffer (cb) {
  const user = await User.createUser(dummyUser);
  dummyUser.email = emailGenerator();
  const user2 = await User.createUser(dummyUser);
  dummyStud.stud_owner_id = user.user_id;
  const stud = await Stud.createStud(dummyStud);
  dummyOffer.stud_id = stud.stud_id;
  dummyOffer.helper_user_id = user2.user_id;
  const offer = await Offer.createOffer(dummyOffer);
  dummyMeeting.offer_id = offer.offer_id;
  await cb(dummyMeeting, stud, offer, user, user2);
}

describe('GET /:offer_id', async () => {
  it('Return meeting if it exists', async () => {
    await ApiTools.createUserAndLogin(async (agent, user) => {
      const meeting = await createDummyMeeting(agent, user);
      await agent.get(`/api/v1/meeting/${meeting.offer_id}`).expect(200);
    });
  });

  it('Don\'t return meeting if meeting does not exist', async () => {
    await ApiTools.createUserAndLogin(async (agent, user) => {
      const offer_id = Number.MAX_SAFE_INTEGER;
      const result = await agent.get(`/api/v1/meeting/${offer_id}`).expect(404);
      expect(result.body.error).to.be.eql('Not Found');
    });
  });

  it('Don\'t return meeting if offer_id is a string', async () => {
    await ApiTools.createUserAndLogin(async (agent, user) => {
      const offer_id = 'Farès';
      const result = await agent.get(`/api/v1/meeting/${offer_id}`).expect(400);
      expect(result.body.error).to.be.eql('Bad Request');
    });
  });

  it('should return 401 if user does not have a cookie', async () => {
    await ApiTools.check401Unauthorized(async (agent) => {
      return await agent.get('/api/v1/meeting/' + Number.MAX_SAFE_INTEGER)
        .set('Accept', 'application/json');
    });
  });
});

describe('DELETE /:offer_id', async () => {
  it('Deletes meeting if meeting exists', async () => {
    await ApiTools.createUserAndLogin(async (agent, user) => {
      const meeting = await createDummyMeeting(agent, user);
      await agent.delete(`/api/v1/meeting/${meeting.offer_id}`).expect(200);
    });
  });

  it('Does not delete meeting if meeting does not exist', async () => {
    await ApiTools.createUserAndLogin(async (agent, user) => {
      const offer_id = Number.MAX_SAFE_INTEGER;
      await agent.delete(`/api/v1/meeting/${offer_id}`).expect(404);
    });
  });

  it('Does not delete meeting if offer_id is a string', async () => {
    await ApiTools.createUserAndLogin(async (agent, user) => {
      const offer_id = 'alexandre';
      await agent.delete(`/api/v1/meeting/${offer_id}`).expect(400);
    });
  });

  it('should return 401 if user does not have a cookie', async () => {
    await ApiTools.check401Unauthorized(async (agent) => {
      return await agent.delete('/api/v1/meeting/' + Number.MAX_SAFE_INTEGER)
        .set('Accept', 'application/json');
    });
  });
});

describe('GET /api/v1/list', async () => {
  it('Fetches all meetings if we do not provide additional params', async () => {
    await ApiTools.createUserAndLogin(async (agent, loggedInUser) => {
      await create2UsersStudAndOffer(
        async (dummyMeeting, stud, offer, user, user2) => {
          const nb = 5;
          const studs = [];
          const offers = [];
          const meetings = [];
          for (let i = 0; i < nb; i++) {
            if (i % 2 === 0) {
              dummyStud.stud_owner_id = user.user_id;
              dummyOffer.helper_user_id = user2.user_id;
            } else {
              dummyStud.stud_owner_id = user2.user_id;
              dummyOffer.helper_user_id = user.user_id;
            }
            let tmpStud = await agent.post('/api/v1/stud/').send(dummyStud).expect(200);
            tmpStud = tmpStud.body;
            studs.push(tmpStud);
            dummyOffer.stud_id = tmpStud.stud_id;
            let tmpOffer = await agent.post('/api/v1/offer/').send(dummyOffer).expect(200);
            tmpOffer = tmpOffer.body;
            offers.push(tmpOffer);
            dummyMeeting.offer_id = tmpOffer.offer_id;
            dummyMeeting.creation_timestamp = moment.utc().toISOString();
            meetings.push(await Meeting.createMeeting(dummyMeeting));
          }
          const expectedResult = meetings.map(m => {
            const offer = offers.filter(o => o.offer_id === m.offer_id)[0];
            const stud = studs.filter(s => s.stud_id === offer.stud_id)[0];
            return {
              channel_link: m.channel_link,
              creation_timestamp: m.creation_timestamp,
              helper_user_id: offer.helper_user_id,
              meeting_date: offer.meeting_date,
              offer_id: offer.offer_id,
              stud_id: stud.stud_id,
              stud_owner_id: stud.stud_owner_id
            };
          });
          let res = await agent.get(`/api/v1/meeting/list?limit=${nb}`).expect(200);
          res = res.body;
          expect(res.sort((a, b) => a.offer_id - b.offer_id)).to.be.eql(expectedResult.sort((a, b) => a.offer_id - b.offer_id));
        }
      );
    });
  });
  it('Fetches all meetings if we provide the helper_user_id', async () => {
    await ApiTools.createUserAndLogin(async (agent, loggedInUser) => {
      await create2UsersStudAndOffer(
        async (dummyMeeting, stud, offer, user, user2) => {
          const nb = 5;
          const studs = [];
          const offers = [];
          const meetings = [];
          for (let i = 0; i < nb; i++) {
            if (i % 2 === 0) {
              dummyStud.stud_owner_id = user.user_id;
              dummyOffer.helper_user_id = user2.user_id;
            } else {
              dummyStud.stud_owner_id = user2.user_id;
              dummyOffer.helper_user_id = user.user_id;
            }
            let tmpStud = await agent.post('/api/v1/stud/').send(dummyStud).expect(200);
            tmpStud = tmpStud.body;
            studs.push(tmpStud);
            dummyOffer.stud_id = tmpStud.stud_id;
            let tmpOffer = await agent.post('/api/v1/offer/').send(dummyOffer).expect(200);
            tmpOffer = tmpOffer.body;
            offers.push(tmpOffer);
            dummyMeeting.offer_id = tmpOffer.offer_id;
            dummyMeeting.creation_timestamp = moment.utc().toISOString();
            meetings.push(await Meeting.createMeeting(dummyMeeting));
          }
          const helperOffers = offers.filter(o => (o.helper_user_id === user.user_id));
          const expectedResult = helperOffers.map(offer => {
            const meeting = meetings.filter(m => m.offer_id === offer.offer_id)[0];
            const stud = studs.filter(s => s.stud_id === offer.stud_id)[0];
            return {
              channel_link: meeting.channel_link,
              creation_timestamp: meeting.creation_timestamp,
              helper_user_id: offer.helper_user_id,
              meeting_date: offer.meeting_date,
              offer_id: offer.offer_id,
              stud_id: stud.stud_id,
              stud_owner_id: stud.stud_owner_id
            };
          });
          let res = await agent.get(`/api/v1/meeting/list?limit=${nb}&helper_user_id=${user.user_id}`).expect(200);
          res = res.body;
          expect(res.sort((a, b) => a.offer_id - b.offer_id)).to.be.eql(expectedResult.sort((a, b) => a.offer_id - b.offer_id));
        }
      );
    });
  });
  it('Fetches all meetings if we provide the stud_owner_id', async () => {
    await ApiTools.createUserAndLogin(async (agent, loggedInUser) => {
      await create2UsersStudAndOffer(
        async (dummyMeeting, stud, offer, user, user2) => {
          const nb = 5;
          const studs = [];
          const offers = [];
          const meetings = [];
          for (let i = 0; i < nb; i++) {
            if (i % 2 === 0) {
              dummyStud.stud_owner_id = user.user_id;
              dummyOffer.helper_user_id = user2.user_id;
            } else {
              dummyStud.stud_owner_id = user2.user_id;
              dummyOffer.helper_user_id = user.user_id;
            }
            let tmpStud = await agent.post('/api/v1/stud/').send(dummyStud).expect(200);
            tmpStud = tmpStud.body;
            studs.push(tmpStud);
            dummyOffer.stud_id = tmpStud.stud_id;
            let tmpOffer = await agent.post('/api/v1/offer/').send(dummyOffer).expect(200);
            tmpOffer = tmpOffer.body;
            offers.push(tmpOffer);
            dummyMeeting.offer_id = tmpOffer.offer_id;
            dummyMeeting.creation_timestamp = moment.utc().toISOString();
            meetings.push(await Meeting.createMeeting(dummyMeeting));
          }
          const seekerStuds = studs.filter(s => s.stud_owner_id === user.user_id);
          const expectedResult = seekerStuds.map(stud => {
            const offer = offers.filter(o => o.stud_id === stud.stud_id)[0];
            const meeting = meetings.filter(m => m.offer_id === offer.offer_id)[0];
            return {
              channel_link: meeting.channel_link,
              creation_timestamp: meeting.creation_timestamp,
              helper_user_id: offer.helper_user_id,
              meeting_date: offer.meeting_date,
              offer_id: offer.offer_id,
              stud_id: stud.stud_id,
              stud_owner_id: stud.stud_owner_id
            };
          });
          let res = await agent.get(`/api/v1/meeting/list?limit=${nb}&stud_owner_id=${user.user_id}`).expect(200);
          res = res.body;
          expect(res.sort((a, b) => a.offer_id - b.offer_id)).to.be.eql(expectedResult.sort((a, b) => a.offer_id - b.offer_id));
        }
      );
    });
  });
  it('Fetches meetings relevant to a certain offer', async () => {
    await ApiTools.createUserAndLogin(async (agent, loggedInUser) => {
      await create2UsersStudAndOffer(async (dummyMeeting, stud, offer, user, user2) => {
        const nb = 5;
        const studs = [];
        const offers = [];
        const meetings = [];
        for (let i = 0; i < nb; i++) {
          if (i % 2 === 0) {
            dummyStud.stud_owner_id = user.user_id;
            dummyOffer.helper_user_id = user2.user_id;
          } else {
            dummyStud.stud_owner_id = user2.user_id;
            dummyOffer.helper_user_id = user.user_id;
          }
          let tmpStud = await agent.post('/api/v1/stud/').send(dummyStud).expect(200);
          tmpStud = tmpStud.body;
          studs.push(tmpStud);
          dummyOffer.stud_id = tmpStud.stud_id;
          let tmpOffer = await agent.post('/api/v1/offer/').send(dummyOffer).expect(200);
          tmpOffer = tmpOffer.body;
          offers.push(tmpOffer);
          dummyMeeting.offer_id = tmpOffer.offer_id;
          dummyMeeting.creation_timestamp = moment.utc().toISOString();
          meetings.push(await Meeting.createMeeting(dummyMeeting));
        }
        meetings.forEach(async (meeting) => {
          const offer = offers.filter(o => o.offer_id === meeting.offer_id)[0];
          const stud = studs.filter(s => s.stud_id === offer.stud_id)[0];
          const expectedResult = {
            channel_link: meeting.channel_link,
            creation_timestamp: meeting.creation_timestamp,
            helper_user_id: offer.helper_user_id,
            meeting_date: offer.meeting_date,
            offer_id: offer.offer_id,
            stud_id: stud.stud_id,
            stud_owner_id: stud.stud_owner_id
          };
          let res = await agent.get(`/api/v1/meeting/list?limit=${nb}&offer_id=${offer.offer_id}`).expect(200);
          res = res.body;
          expect(res.sort((a, b) => a.offer_id - b.offer_id)).to.be.eql(expectedResult);
        });
      });
    });
  });
});
