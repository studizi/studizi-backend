/* eslint-disable no-undef */
const { expect } = require('chai');
const chai = require('chai');
chai.use(require('chai-like'));
chai.use(require('chai-things'));
const emailGenerator = require('../../utils/email.generator').emailGenerator;
const ApiTools = require('../../utils/agent.tools');
const usersDataset = require('../../resources/users.dataset');

beforeEach(() => {
  usersDataset.success[2].data.email = emailGenerator();
});

async function createUser (agent, user) {
  return await agent.post('/auth/signup')
    .set('Accept', 'application/json')
    .send(user);
}

async function createUserAndLogin (cb) {
  const agent = await ApiTools.createAgent();
  const copy = { ...usersDataset.success[2].data };
  const createResponse = await createUser(agent, copy);
  const returnedUser = createResponse.body;
  const credentials = {
    email: copy.email,
    password: copy.password
  };
  await agent.post('/auth/login')
    .set('Accept', 'application/json')
    .send(credentials)
    .expect(302);

  await cb(agent, returnedUser);
}

describe('GET /api/v1/user/:user_id', async () => {
  it('should return the correct info about user', async () => {
    await createUserAndLogin(async (agent, returnedUser) => {
      const response = await agent.get('/api/v1/user/' + returnedUser.user_id)
        .set('Accept', 'application/json')
        .expect(200);

      const returnedObj = response.body;
      expect(returnedObj)
        .to.be.an('object');

      expect(returnedObj.user_id)
        .to.be.a('number')
        .and.to.be.greaterThan(0);

      expect(returnedObj)
        .to.eql(returnedUser);
    });
  });

  it('should return the correct info about user using the function ApiTools.createUserAndLogin', async () => {
    await ApiTools.createUserAndLogin(async (agent, returnedUser) => {
      const response = await agent.get('/api/v1/user/' + returnedUser.user_id)
        .set('Accept', 'application/json')
        .expect(200);

      const returnedObj = response.body;
      expect(returnedObj)
        .to.be.an('object');

      expect(returnedObj.user_id)
        .to.be.a('number')
        .and.to.be.greaterThan(0);

      expect(returnedObj)
        .to.eql(returnedUser);
    });
  });

  it('could get the information of another user', async () => {
    await createUserAndLogin(async (agent, returnedUser1) => {
      await createUserAndLogin(async (agent, returnedUser) => {
        const response = await agent.get('/api/v1/user/' + returnedUser1.user_id)
          .set('Accept', 'application/json')
          .expect(200);

        const returnedObj = response.body;
        expect(returnedObj)
          .to.be.an('object');

        expect(returnedObj.user_id)
          .to.be.a('number')
          .and.to.be.greaterThan(0);

        expect(returnedObj)
          .to.eql(returnedUser1);
      });
    });
  });

  it('should return 401 if user does not have a cookie', async () => {
    await ApiTools.check401Unauthorized(async (agent) => {
      return await agent.get('/api/v1/user/' + Number.MAX_SAFE_INTEGER)
        .set('Accept', 'application/json');
    });
  });

  it('should return 404 if user does not exist', async () => {
    await createUserAndLogin(async (agent1, returnedUser) => {
      await ApiTools.check404NotFound(async (agent) => {
        return await agent1.get('/api/v1/user/' + Number.MAX_SAFE_INTEGER)
          .set('Accept', 'application/json');
      });
    });
  });
});

describe('DELETE /api/v1/user', async () => {
  it('should delete user', async () => {
    await createUserAndLogin(async (agent, returnedUser) => {
      const response = await agent.delete('/api/v1/user')
        .set('Accept', 'application/json')
        .expect(200);

      const returnedObj = response.body;
      expect(returnedObj)
        .to.be.an('object');

      expect(returnedObj.user_id)
        .to.be.a('number')
        .and.to.be.greaterThan(0);

      expect(returnedObj)
        .to.eql(returnedUser);

      await ApiTools.check401Unauthorized(async (agent) => {
        return await agent.get('/api/v1/user')
          .set('Accept', 'application/json');
      });
    });
  });
  it('should return 401 if user does not have a cookie', async () => {
    await ApiTools.check401Unauthorized(async (agent) => {
      return await agent.delete('/api/v1/user')
        .set('Accept', 'application/json');
    });
  });
  it('should return 401 if user does not exist any more', async () => {
    await createUserAndLogin(async (agent, returnedUser) => {
      await agent.delete('/api/v1/user')
        .set('Accept', 'application/json')
        .expect(200);

      await ApiTools.check401Unauthorized(async (agent) => {
        return await agent.delete('/api/v1/user')
          .set('Accept', 'application/json');
      });
    });
  });
});

describe('PUT /api/v1/user', async () => {
  it('should update user', async () => {
    await createUserAndLogin(async (agent, returnedUser) => {
      const copy = { ...usersDataset.success[3].data };
      copy.user_id = returnedUser.user_id;
      copy.email = returnedUser.email;
      const response = await agent.put('/api/v1/user')
        .set('Accept', 'application/json')
        .send(copy)
        .expect(200);

      const returnedObj = response.body;
      expect(returnedObj)
        .to.be.an('object');

      expect(returnedObj.user_id)
        .to.be.a('number')
        .and.to.be.greaterThan(0);

      delete returnedObj.email;
      const copy2 = { ...usersDataset.success[3].data };
      delete copy2.password;
      copy2.user_id = returnedUser.user_id;
      copy2.verified = true;
      expect(returnedObj)
        .to.eql(copy2);
    });
  });

  it('should return 400 if user with same email already exist', async () => {
    await createUserAndLogin(async (agent, returnedUser1) => {
      usersDataset.success[2].data.email = emailGenerator();
      await createUserAndLogin(async (agent1, returnedUser2) => {
        const copy = { ...usersDataset.success[3].data };
        copy.user_id = returnedUser2.user_id;
        copy.email = returnedUser1.email;
        await ApiTools.check400BadRequest(async (agent) => {
          return await agent1.put('/api/v1/user')
            .set('Accept', 'application/json')
            .send(copy);
        });
      });
    });
  });

  it('should return 400 if user_id is not the same as cookie user_id', async () => {
    await createUserAndLogin(async (agent1, returnedUser) => {
      const copy = { ...usersDataset.success[3].data };
      copy.user_id = returnedUser.user_id + 1;
      copy.email = returnedUser.email;
      await ApiTools.check400BadRequest(async (agent) => {
        return await agent1.put('/api/v1/user')
          .set('Accept', 'application/json')
          .send(copy);
      });
    });
  });

  it('should return 401 if user does not have a cookie', async () => {
    await ApiTools.check401Unauthorized(async (agent) => {
      return await agent.put('/api/v1/user')
        .set('Accept', 'application/json');
    });
  });

  it('should return 400 if body is empty', async () => {
    await createUserAndLogin(async (agent1, returnedUser) => {
      await ApiTools.check400BadRequest(async (agent) => {
        return await agent1.put('/api/v1/user')
          .set('Accept', 'application/json');
      });
    });
  });

  it('should return 400 if body is not valid', async () => {
    await createUserAndLogin(async (agent1, returnedUser) => {
      for (const [key] of Object.entries(usersDataset.failures[0].data)) {
        const copy = { ...returnedUser };
        await ApiTools.check400BadRequest(async (agent) => {
          copy[key] = usersDataset.failures[0].data[key];
          return await agent1.put('/api/v1/user')
            .set('Accept', 'application/json')
            .send(copy);
        });
      }
    });
  });
});

describe('PUT /api/v1/user/password', async () => {
  it('should update user password', async () => {
    await createUserAndLogin(async (agent, returnedUser) => {
      const copy = { ...usersDataset.success[2].data };
      const body = {
        old_password: copy.password,
        new_password: copy.password + '.'
      };
      const response = await agent.put('/api/v1/user/password')
        .set('Accept', 'application/json')
        .send(body)
        .expect(200);

      const returnedObj = response.body;
      expect(returnedObj)
        .to.be.an('object')
        .and.eql(returnedUser);
    });
  });

  it('should return 403 if old_password is not correct', async () => {
    await createUserAndLogin(async (agent, returnedUser) => {
      const copy = { ...usersDataset.success[2].data };
      const body = {
        old_password: copy.password + 'test',
        new_password: copy.password + '.'
      };
      await ApiTools.check403Forbidden(async () => {
        return await agent.put('/api/v1/user/password')
          .set('Accept', 'application/json')
          .send(body);
      });
    });
  });

  it('should return 400 if passwords are not correct', async () => {
    await createUserAndLogin(async (agent, returnedUser) => {
      const copy = { ...usersDataset.success[2].data };
      let body = {
        old_password: 'test',
        new_password: copy.password + '.'
      };
      await ApiTools.check400BadRequest(async () => {
        return await agent.put('/api/v1/user/password')
          .set('Accept', 'application/json')
          .send(body);
      });
      body = {
        old_password: copy.password,
        new_password: 'test'
      };
      await ApiTools.check400BadRequest(async () => {
        return await agent.put('/api/v1/user/password')
          .set('Accept', 'application/json')
          .send(body);
      });
    });
  });

  it('should return 401 if user does not have a cookie', async () => {
    await ApiTools.check401Unauthorized(async (agent) => {
      return await agent.put('/api/v1/user/password')
        .set('Accept', 'application/json');
    });
  });

  it('should return 400 if body is empty', async () => {
    await createUserAndLogin(async (agent1, returnedUser) => {
      await ApiTools.check400BadRequest(async (agent) => {
        return await agent1.put('/api/v1/user/password')
          .set('Accept', 'application/json');
      });
    });
  });
});
