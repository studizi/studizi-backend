/* eslint-disable no-undef */
const {
  expect
} = require('chai');
const chai = require('chai');
chai.use(require('chai-like'));
chai.use(require('chai-things'));
const ApiTools = require('../../utils/agent.tools');
const usersDataset = require('../../resources/users.dataset');
const studDataset = require('../../resources/studs.dataset');
const Credit = require('../../../database/models/credit.model');
const emailGenerator = require('../../utils/email.generator').emailGenerator;
const moment = require('moment');

beforeEach(() => {
  usersDataset.success[2].data.email = emailGenerator();

  studDataset.success[2].data.deadline = moment()
    .add(1, 'day')
    .toISOString();
});

const parseArrayTstzrange = function (val) {
  // eslint-disable-next-line no-useless-escape
  const outerRegex = /[\(\[]\"[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}\+[0-9]{2}\",\"[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}\+[0-9]{2}\"[\)\]]/g;
  const arrayTstzRange = val.match(outerRegex);

  const innerRegex = /:[0-9]{2}\+[0-9]{2}/g;

  console.log(arrayTstzRange);

  for (let i = 0; i < arrayTstzRange.length; i++) {
    arrayTstzRange[i] = arrayTstzRange[i]
      .replaceAll(innerRegex, '')
      .replaceAll('"', '');
  }

  return arrayTstzRange;
};

async function loginAndCreateStud (cb) {
  await ApiTools.createUserAndLogin(async (agent, returnedUser) => {
    const copy = {
      ...studDataset.success[2].data
    };
    await Credit.addCredit(returnedUser.user_id, 0, 20);
    copy.availability_list = [...studDataset.success[2].data.availability_list];
    const response = await agent
      .post('/api/v1/stud')
      .set('Accept', 'application/json')
      .send(copy)
      .expect(200);
    const returnedStud = response.body;
    await cb(agent, returnedUser, returnedStud, copy);
  });
}

describe('POST /api/v1/stud/', async () => {
  it('should create a new stud', async () => {
    await loginAndCreateStud(
      async (agent, returnedUser, returnedStud, copy) => {
        expect(returnedStud).to.be.an('object');

        expect(returnedStud.stud_id)
          .to.be.a('number')
          .and.to.be.greaterThan(0);

        expect(returnedStud.status)
          .to.be.a('string')
          .that.equal('AVAILABLE');

        expect(returnedStud.stud_owner_id)
          .to.be.a('number')
          .that.equal(returnedUser.user_id);

        copy.stud_owner_id = returnedUser.user_id;
        delete returnedStud.stud_id;
        delete returnedStud.creation_timestamp;
        for (let i = 0; i < copy.availability_list.length; i++) {
          copy.availability_list[i] = parseArrayTstzrange(
            copy.availability_list[i]
          )[0];
        }

        expect(returnedStud).to.eql(copy);
      }
    );
  });
  it('should return 401 if user does not have a cookie', async () => {
    await ApiTools.check401Unauthorized(async agent => {
      return await agent.post('/api/v1/stud').set('Accept', 'application/json');
    });
  });
  it('should return 400 if body is empty', async () => {
    await ApiTools.createUserAndLogin(async (agent1, returnedUser) => {
      await ApiTools.check400BadRequest(async agent => {
        return await agent1
          .post('/api/v1/stud')
          .set('Accept', 'application/json');
      });
    });
  });
  it('should return 400 if body is not valid', async () => {
    const keys = [
      'attachments',
      'availability_list',
      'silver_credit_cost',
      'gold_credit_cost'
    ];
    await ApiTools.createUserAndLogin(async (agent1, returnedUser) => {
      for (const key of keys) {
        const copy = {
          ...studDataset.success[2].data
        };
        copy.stud_owner_id = returnedUser.user_id;
        await ApiTools.check400BadRequest(async agent => {
          copy[key] = studDataset.failures[0].data[key];
          return await agent1
            .post('/api/v1/stud')
            .set('Accept', 'application/json')
            .send(copy);
        });
      }
    });
  });
});

describe('GET /api/v1/stud/:stud_id', async () => {
  it('should get stud', async () => {
    await loginAndCreateStud(async (agent, returnedUser, stud, copy) => {
      const gettedStud = await agent
        .get('/api/v1/stud/' + stud.stud_id)
        .set('Accept', 'application/json')
        .expect(200);

      const returnedStud = gettedStud.body;

      expect(returnedStud)
        .to.be.an('object')
        .and.to.eql(stud);
    });
  });
  it('should get list of studs', async () => {
    await loginAndCreateStud(async (agent, returnedUser, stud, copy) => {
      const studs = [];
      const nb = 4;
      for (let i = 0; i < nb; i++) {
        if (i % 2 === 0) {
          copy.tags = ['vba', 'nodejs'];
        } else {
          copy.tags = ['c', 'c++'];
        }
        const res = await agent
          .post('/api/v1/stud')
          .set('Accept', 'application/json')
          .send(copy);
        studs.push(res.body);
        studs[i].stud_cost = studs[i].silver_credit_cost + studs[i].gold_credit_cost;
        delete studs[i].attachments;
        delete studs[i].availability_list;
        delete studs[i].degree_level;
        delete studs[i].gold_credit_cost;
        // delete studs[i].problem_description;
        delete studs[i].silver_credit_cost;
        // delete studs[i].stud_owner_id;
      }
      let res = await agent
        .get('/api/v1/stud/list')
        .set('Accept', 'application/json')
        .query({
          limit: nb,
          studOWnerId: returnedUser.user_id,
          tags: ['c', 'vba']
        });
      expect(res.body.sort((a, b) => a.stud_id - b.stud_id)).to.be.eql(
        studs.sort((a, b) => a.stud_id - b.stud_id)
      );

      res = await agent
        .get('/api/v1/stud/list')
        .set('Accept', 'application/json')
        .query({
          limit: nb,
          tags: ['c', 'vba']
        });
      expect(res.body.sort((a, b) => a.stud_id - b.stud_id)).to.be.eql(
        studs.sort((a, b) => a.stud_id - b.stud_id)
      );

      res = await agent
        .get('/api/v1/stud/list')
        .set('Accept', 'application/json')
        .query({
          limit: nb,
          tags: ['c']
        });
      expect(res.body.sort((a, b) => a.stud_id - b.stud_id)).to.be.eql(
        studs
          .filter(s => s.tags.includes('c'))
          .sort((a, b) => a.stud_id - b.stud_id)
      );

      res = await agent
        .get('/api/v1/stud/list')
        .set('Accept', 'application/json')
        .query({
          creationTimestamp: studs[nb - 1].creation_timestamp,
          limit: nb - 1
        });
      expect(res.body.sort((a, b) => a.stud_id - b.stud_id)).to.be.eql(
        studs.slice(0, nb - 1).sort((a, b) => a.stud_id - b.stud_id)
      );

      res = await agent
        .get('/api/v1/stud/list')
        .set('Accept', 'application/json')
        .query({
          creationTimestamp: studs[nb - 1].creation_timestamp,
          limit: nb - 2,
          offset: 1
        });
      expect(res.body.sort((a, b) => a.stud_id - b.stud_id)).to.be.eql(
        studs.slice(0, nb - 2).sort((a, b) => a.stud_id - b.stud_id)
      );
    });
  });
  it('should return 401 if user does not have a cookie', async () => {
    await ApiTools.check401Unauthorized(async agent => {
      return await agent
        .get('/api/v1/stud/list')
        .set('Accept', 'application/json');
    });
  });
  it('should return 400 if creationTimestamp is not parsable or offset/limit are not integers or not valid', async () => {
    await ApiTools.createUserAndLogin(async (agent1, returnedUser) => {
      await ApiTools.check400BadRequest(async agent => {
        return await agent1
          .get('/api/v1/stud/list')
          .query({
            creationTimestamp: 'abc'
          })
          .set('Accept', 'application/json');
      });
      await ApiTools.check400BadRequest(async agent => {
        return await agent1
          .get('/api/v1/stud/list')
          .query({
            limit: '-1'
          })
          .set('Accept', 'application/json');
      });
      await ApiTools.check400BadRequest(async agent => {
        return await agent1
          .get('/api/v1/stud/list')
          .query({
            offset: 'abc'
          })
          .set('Accept', 'application/json');
      });
    });
  });
  it('should return 401 if user does not have a cookie', async () => {
    await ApiTools.check401Unauthorized(async agent => {
      return await agent
        .get('/api/v1/stud/' + Number.MAX_SAFE_INTEGER)
        .set('Accept', 'application/json');
    });
  });
  it('should return 404 if stud does not exist', async () => {
    await ApiTools.createUserAndLogin(async (agent1, returnedUser) => {
      await ApiTools.check404NotFound(async agent => {
        return await agent1
          .get('/api/v1/stud/' + Number.MAX_SAFE_INTEGER)
          .set('Accept', 'application/json');
      });
    });
  });
  it('should return 400 if stud_id is not a number', async () => {
    await ApiTools.createUserAndLogin(async (agent1, returnedUser) => {
      await ApiTools.check400BadRequest(async agent => {
        return await agent1
          .get('/api/v1/stud/' + 'abc')
          .set('Accept', 'application/json');
      });
    });
  });
});
describe('Delete /api/v1/stud/:stud_id', async () => {
  it('should delete a stud', async () => {
    await loginAndCreateStud(async (agent, returnedUser, stud, copy) => {
      const gettedStud = await agent
        .delete('/api/v1/stud/' + stud.stud_id)
        .set('Accept', 'application/json')
        .expect(200);

      const returnedStud = gettedStud.body;

      expect(returnedStud)
        .to.be.an('object')
        .and.to.eql(stud);
    });
  });
  it('should return 401 if user does not have a cookie', async () => {
    await ApiTools.check401Unauthorized(async agent => {
      return await agent
        .delete('/api/v1/stud/' + Number.MAX_SAFE_INTEGER)
        .set('Accept', 'application/json');
    });
  });
  it('should return 404 if stud does not exist', async () => {
    await ApiTools.createUserAndLogin(async (agent1, returnedUser) => {
      await ApiTools.check404NotFound(async agent => {
        return await agent1
          .delete('/api/v1/stud/' + Number.MAX_SAFE_INTEGER)
          .set('Accept', 'application/json');
      });
    });
  });
  it('should return 400 if stud_id is not a number', async () => {
    await ApiTools.createUserAndLogin(async (agent1, returnedUser) => {
      await ApiTools.check400BadRequest(async agent => {
        return await agent1
          .delete('/api/v1/stud/' + 'abc')
          .set('Accept', 'application/json');
      });
    });
  });
  it('should return 400 if stud_owner_id is not the same as cookie user_id', async () => {
    await loginAndCreateStud(async (agent, returnedUser, stud, copy) => {
      usersDataset.success[2].data.email = emailGenerator();

      await ApiTools.createUserAndLogin(async (agent2, returnedUser2) => {
        await ApiTools.check404NotFound(async agent => {
          return await agent2
            .delete('/api/v1/stud/' + stud.stud_id)
            .set('Accept', 'application/json');
        });
      });
    });
  });
});

describe('PUT /api/v1/stud', async () => {
  it('should update stud', async () => {
    await loginAndCreateStud(async (agent, returnedUser, stud, copy) => {
      const copy2 = {
        ...studDataset.success[3].data
      };
      copy2.stud_id = stud.stud_id;
      copy2.deadline = moment()
        .add(1, 'day')
        .toISOString();

      const response2 = await agent
        .put('/api/v1/stud/' + copy2.stud_id)
        .set('Accept', 'application/json')
        .send(copy2)
        .expect(200);
      const returnedObj = response2.body;

      for (let i = 0; i < copy2.availability_list.length; i++) {
        copy2.availability_list[i] = parseArrayTstzrange(
          copy2.availability_list[i]
        )[0];
      }

      expect(returnedObj.stud_owner_id)
        .to.be.a('number')
        .that.equal(returnedUser.user_id);

      delete returnedObj.stud_owner_id;
      delete returnedObj.creation_timestamp;
      expect(returnedObj)
        .to.be.an('object')
        .and.to.eql(copy2);
    });
  });
  it('should return 400 if stud_owner_id is not the same as cookie user_id', async () => {
    await loginAndCreateStud(async (agent, returnedUser, stud, copy) => {
      usersDataset.success[2].data.email = emailGenerator();

      await ApiTools.createUserAndLogin(async (agent2, returnedUser2) => {
        const copy2 = {
          ...studDataset.success[3].data
        };
        copy2.stud_id = stud.stud_id;
        copy2.deadline = moment()
          .add(1, 'day')
          .toISOString();
        await ApiTools.check404NotFound(async agent => {
          return await agent2
            .put('/api/v1/stud/' + stud.stud_id)
            .set('Accept', 'application/json')
            .send(copy2);
        });
      });
    });
  });
  it('should return 401 if user does not have a cookie', async () => {
    await ApiTools.check401Unauthorized(async agent => {
      return await agent
        .put('/api/v1/stud/' + Number.MAX_SAFE_INTEGER)
        .set('Accept', 'application/json');
    });
  });
  it('should return 400 if body is empty', async () => {
    await loginAndCreateStud(async (agent1, returnedUser, stud, copy) => {
      await ApiTools.check400BadRequest(async agent => {
        return await agent1
          .put('/api/v1/stud/' + stud.stud_id)
          .set('Accept', 'application/json');
      });
    });
  });
  it('should return 400 if body is not valid', async () => {
    const keys = [
      'attachments',
      'availability_list',
      'silver_credit_cost',
      'gold_credit_cost'
    ];

    await loginAndCreateStud(async (agent1, returnedUser, stud, copy) => {
      for (const key of keys) {
        const copy2 = {
          ...studDataset.success[3].data
        };
        copy2.stud_id = stud.stud_id;
        copy2.deadline = moment()
          .add(1, 'day')
          .toISOString();
        copy2.stud_owner_id = returnedUser.user_id;
        await ApiTools.check400BadRequest(async agent => {
          copy2[key] = studDataset.failures[0].data[key];
          return await agent1
            .put('/api/v1/stud/' + copy2.stud_id)
            .set('Accept', 'application/json')
            .send(copy2);
        });
      }
    });
  });
});
