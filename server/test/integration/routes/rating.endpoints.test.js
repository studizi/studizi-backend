/* eslint-disable no-undef */
const {
  expect
} = require('chai');
const chai = require('chai');
chai.use(require('chai-like'));
chai.use(require('chai-things'));
const dbTools = require('../../../database/tools/execute.sql.order');
const emailGenerator = require('../../utils/email.generator').emailGenerator;
const ResponseModels = require('../../../responses/response.models.json');
const ApiTools = require('../../utils/agent.tools');
const User = require('../../../database/models/user.model');
const Offer = require('../../../database/models/offer.model');
const Meeting = require('../../../database/models/meeting.model');
const Stud = require('../../../database/models/stud.model');
const moment = require('moment');
const usersDataset = require('../../resources/users.dataset.json');
const studsDataset = require('../../resources/studs.dataset.json');
const offersDataset = require('../../resources/offers.dataset.json');
const ratingDataset = require('../../resources/rating.dataset.json');
const meetingDataset = require('../../resources/meeting.dataset.json');

let user, user2, stud, offer, meeting;

const dummyUser = usersDataset.success[0].data;
delete dummyUser.user_id;

const dummyStud = studsDataset.success[2].data;
delete dummyStud.stud_id;

const dummyOffer = offersDataset.success[0].data;
delete dummyOffer.offer_id;

const dummyMeeting = meetingDataset.success[0].data;
delete dummyMeeting.offer_id;

const dummyRating = ratingDataset.success[0].data;
const smartRating = ratingDataset.success[1].data;

beforeEach(() => {
  dummyUser.email = emailGenerator();
});

async function createUsersAndStud (returnedUser) {
  user = await User.getUser(returnedUser.user_id);
  dummyUser.email = emailGenerator();
  user2 = await User.createUser(dummyUser);
  switch (dummyRating.type) {
    case 'SEEKER':
      dummyStud.stud_owner_id = user.user_id;
      break;
    case 'HELPER':
      dummyStud.stud_owner_id = user2.user_id;
      break;
  }
  dummyStud.deadline = moment()
    .add(1, 'day')
    .toISOString();
  dummyStud.status = 'AVAILABLE';
  stud = await Stud.createStud(dummyStud);
}

async function loginAndCreateDummyMeeting (cb) {
  return await ApiTools.createUserAndLogin(async (agent, returnedUser) => {
    await createUsersAndStud(returnedUser);
    dummyOffer.stud_id = stud.stud_id;
    switch (dummyRating.type) {
      case 'SEEKER':
        dummyOffer.helper_user_id = user2.user_id;
        break;
      case 'HELPER':
        dummyOffer.helper_user_id = user.user_id;
        break;
    }
    offer = await Offer.createOffer(dummyOffer);
    dummyMeeting.offer_id = offer.offer_id;
    meeting = await Meeting.createMeeting(dummyMeeting);
    dummyRating.offer_id = meeting.offer_id;

    return await cb(agent, dummyRating);
  });
}

before(async function () {
  await dbTools.initDb();
});

after(async function () {
  await dbTools.deleteDb();
});

describe('POST /api/v1/rating', async () => {
  it('should create a new rating if body is correct', async () => {
    await loginAndCreateDummyMeeting(async (agent, dummyRating) => {
      const response = await agent.post('/api/v1/rating')
        .set('Accept', 'application/json')
        .send(dummyRating)
        .expect(ResponseModels.OK.statusCode);

      const returnedObj = response.body;

      expect(returnedObj)
        .to.be.an('object');

      expect(returnedObj.rating_id)
        .to.be.a('number')
        .and.to.be.greaterThan(0);

      expect(returnedObj.rating)
        .to.be.a('number')
        .and.to.be.greaterThan(0);

      switch (returnedObj.type) {
        case 'SEEKER':
          expect(returnedObj.rated_user_id)
            .to.be.a('number')
            .and.to.equal(dummyOffer.helper_user_id);

          expect(returnedObj.rating_user_id)
            .to.be.a('number')
            .and.to.equal(dummyStud.stud_owner_id);
          break;
        case 'HELPER':
          expect(returnedObj.rated_user_id)
            .to.be.a('number')
            .and.to.equal(dummyStud.stud_owner_id);

          expect(returnedObj.rating_user_id)
            .to.be.a('number')
            .and.to.equal(dummyOffer.helper_user_id);
          break;
      }

      expect(returnedObj.type)
        .to.be.a('string');

      delete returnedObj.rating_id;
      delete returnedObj.rated_user_id;
      delete returnedObj.rating_user_id;
      expect(returnedObj)
        .to.deep.equal(dummyRating);
    });
  });
  it('should return bad request if body is empty', async () => {
    await ApiTools.createUserAndLogin(async (agent, returnedUser) => {
      await ApiTools.check400BadRequest(async (agent2) => {
        return await agent.post('/api/v1/rating')
          .set('Accept', 'application/json');
      });
    });
  });
  it('should return 401 if user does not have a cookie', async () => {
    await ApiTools.check401Unauthorized(async (agent) => {
      return await agent.post('/api/v1/rating')
        .set('Accept', 'application/json');
    });
  });
  it('should return 400 if body is not valid', async () => {
    const keys = ['rating', 'comment', 'type'];
    await loginAndCreateDummyMeeting(async (agent, dummyRating) => {
      for (const a of ratingDataset.failures) {
        const copy = {
          ...dummyRating
        };
        for (const key of keys) {
          copy[key] = a.data[key];
        }
        await ApiTools.check400BadRequest(async (agent2) => {
          return await agent.post('/api/v1/rating')
            .set('Accept', 'application/json')
            .send(copy);
        });
      }
    });
  });
});

describe('DELETE /api/v1/rating/:rating_id', async () => {
  it("should delete a rating by it's rating_id", async () => {
    await loginAndCreateDummyMeeting(async (agent, dummyRating) => {
      const response = await agent.post('/api/v1/rating')
        .set('Accept', 'application/json')
        .send(dummyRating)
        .expect(ResponseModels.OK.statusCode);
      const returnedObj = response.body;

      const response2 = await agent
        .delete('/api/v1/rating/' + returnedObj.rating_id)
        .set('Accept', 'application/json')
        .send()
        .expect(ResponseModels.OK.statusCode);

      const returnedObj2 = JSON.parse(response2.text);

      expect(returnedObj2.rating_id)
        .to.be.a('number')
        .and.to.equal(returnedObj.rating_id);

      switch (returnedObj2.type) {
        case 'SEEKER':
          expect(returnedObj2.rated_user_id)
            .to.be.a('number')
            .and.to.equal(dummyOffer.helper_user_id);

          expect(returnedObj2.rating_user_id)
            .to.be.a('number')
            .and.to.equal(dummyStud.stud_owner_id);
          break;
        case 'HELPER':
          expect(returnedObj2.rated_user_id)
            .to.be.a('number')
            .and.to.equal(dummyStud.stud_owner_id);

          expect(returnedObj2.rating_user_id)
            .to.be.a('number')
            .and.to.equal(dummyOffer.helper_user_id);
          break;
      }

      delete returnedObj2.rating_id;
      delete returnedObj2.rated_user_id;
      delete returnedObj2.rating_user_id;
      expect(returnedObj2)
        .to.deep.equal(dummyRating);
    });
  });
  it('should return 401 if user does not have a cookie', async () => {
    await ApiTools.check401Unauthorized(async (agent) => {
      return await agent.delete('/api/v1/rating/' + Number.MAX_SAFE_INTEGER)
        .set('Accept', 'application/json');
    });
  });
  it('should return 404 if stud does not exist', async () => {
    await ApiTools.createUserAndLogin(async (agent, returnedUser) => {
      await ApiTools.check404NotFound(async (agent2) => {
        return await agent.delete('/api/v1/rating/' + Number.MAX_SAFE_INTEGER)
          .set('Accept', 'application/json');
      });
    });
  });
  it('should return bad request if parameter rating_id is invalid', async () => {
    await ApiTools.createUserAndLogin(async (agent, returnedUser) => {
      await ApiTools.check400BadRequest(async (agent2) => {
        return await agent.delete('/api/v1/rating/' + 'abc')
          .set('Accept', 'application/json');
      });
    });
  });
});

describe('GET /api/v1/rating/:rating_id', async () => {
  it("should return a rating by it's rating_id", async () => {
    await loginAndCreateDummyMeeting(async (agent, dummyRating) => {
      const response = await agent.post('/api/v1/rating')
        .set('Accept', 'application/json')
        .send(dummyRating)
        .expect(ResponseModels.OK.statusCode);

      const returnedObj = response.body;

      const response2 = await agent
        .get('/api/v1/rating/' + returnedObj.rating_id)
        .set('Accept', 'application/json')
        .send()
        .expect(ResponseModels.OK.statusCode);

      const returnedObj2 = JSON.parse(response2.text);

      expect(returnedObj2.rating_id)
        .to.be.a('number')
        .and.to.equal(returnedObj.rating_id);

      switch (returnedObj2.type) {
        case 'SEEKER':
          expect(returnedObj2.rated_user_id)
            .to.be.a('number')
            .and.to.equal(dummyOffer.helper_user_id);

          expect(returnedObj2.rating_user_id)
            .to.be.a('number')
            .and.to.equal(dummyStud.stud_owner_id);
          break;
        case 'HELPER':
          expect(returnedObj2.rated_user_id)
            .to.be.a('number')
            .and.to.equal(dummyStud.stud_owner_id);

          expect(returnedObj2.rating_user_id)
            .to.be.a('number')
            .and.to.equal(dummyOffer.helper_user_id);
          break;
      }

      delete returnedObj2.rating_id;
      delete returnedObj2.rated_user_id;
      delete returnedObj2.rating_user_id;
      expect(returnedObj2)
        .to.deep.equal(dummyRating);
    });
  });
  it('should return 401 if user does not have a cookie', async () => {
    await ApiTools.check401Unauthorized(async (agent) => {
      return await agent.get('/api/v1/rating/' + Number.MAX_SAFE_INTEGER)
        .set('Accept', 'application/json');
    });
  });
  it('should return 404 if stud does not exist', async () => {
    await ApiTools.createUserAndLogin(async (agent, returnedUser) => {
      await ApiTools.check404NotFound(async (agent2) => {
        return await agent.get('/api/v1/rating/' + Number.MAX_SAFE_INTEGER)
          .set('Accept', 'application/json');
      });
    });
  });
  it('should return bad request if parameter rating_id is invalid', async () => {
    await ApiTools.createUserAndLogin(async (agent, returnedUser) => {
      await ApiTools.check400BadRequest(async (agent2) => {
        return await agent.get('/api/v1/rating/' + 'abc')
          .set('Accept', 'application/json');
      });
    });
  });
});

describe('PUT /api/v1/rating', async () => {
  it("should update a rating by it's if body is correct", async () => {
    await loginAndCreateDummyMeeting(async (agent, dummyRating) => {
      const response = await agent.post('/api/v1/rating')
        .set('Accept', 'application/json')
        .send(dummyRating)
        .expect(ResponseModels.OK.statusCode);

      const rating = response.body;

      const copy = { ...smartRating };
      copy.rated_user_id = rating.rated_user_id;
      copy.rating_user_id = rating.rating_user_id;
      copy.offer_id = rating.offer_id;
      copy.rating_id = rating.rating_id;

      const response2 = await agent.put('/api/v1/rating/' + copy.rating_id)
        .set('Accept', 'application/json')
        .send(copy)
        .expect(ResponseModels.OK.statusCode);

      const returnedObj2 = JSON.parse(response2.text);

      expect(returnedObj2)
        .to.deep.equal(copy);
    });
  });
  it('should return 401 if user does not have a cookie', async () => {
    await ApiTools.check401Unauthorized(async (agent) => {
      return await agent.put('/api/v1/rating/' + Number.MAX_SAFE_INTEGER)
        .set('Accept', 'application/json');
    });
  });
  it('should return 400 if body is empty', async () => {
    await loginAndCreateDummyMeeting(async (agent, dummyRating) => {
      const response = await agent.post('/api/v1/rating')
        .set('Accept', 'application/json')
        .send(dummyRating)
        .expect(ResponseModels.OK.statusCode);

      const rating = response.body;
      await ApiTools.check400BadRequest(async (agent2) => {
        return await agent.put('/api/v1/rating/' + rating.rating_id)
          .set('Accept', 'application/json');
      });
    });
  });
  it('should return 400 if body is not valid', async () => {
    const keys = ['rating', 'comment', 'type'];
    await loginAndCreateDummyMeeting(async (agent, dummyRating) => {
      const response = await agent.post('/api/v1/rating')
        .set('Accept', 'application/json')
        .send(dummyRating)
        .expect(ResponseModels.OK.statusCode);

      const rating = response.body;
      for (const a of ratingDataset.failures) {
        const copy = {
          ...rating
        };
        for (const key of keys) {
          copy[key] = a.data[key];
        }
        await ApiTools.check400BadRequest(async (agent2) => {
          return await agent.put('/api/v1/rating/' + rating.rating_id)
            .set('Accept', 'application/json')
            .send(copy);
        });
      }
    });
  });
});
