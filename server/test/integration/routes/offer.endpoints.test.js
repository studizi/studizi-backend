/* eslint-disable no-unused-expressions */
/* eslint-disable no-undef */
const { expect } = require('chai');
const chai = require('chai');
chai.use(require('chai-like'));
chai.use(require('chai-things'));

const apiTools = require('../../utils/agent.tools');
const dbTools = require('../../../database/tools/execute.sql.order');
const SHA256 = require('js-sha256');
const moment = require('moment');

const users = require('../../../database/models/user.model');
const studs = require('../../../database/models/stud.model');
const offers = require('../../../database/models/offer.model');

const usersDataset = require('../../resources/users.dataset');
const studsDataset = require('../../resources/studs.dataset');
const offersDataset = require('../../resources/offers.dataset');

let helper;
let agent;
let seeker;
let stud;
let validOfferBody;
let invalidOfferBody;

async function factoryDummyUser (userBody) {
  const copy = { ...userBody };
  if (copy.hasOwnProperty('user_id')) {
    delete copy.user_id;
  }
  copy.password = SHA256(copy.password);
  const user = await users.createUser(copy);
  user.password = userBody.password;
  return user;
}

async function factoryDummyStud (userId, studBody) {
  const copy = { ...studBody };
  if (copy.hasOwnProperty('stud_id')) {
    delete copy.stud_id;
  }
  copy.stud_owner_id = userId;
  copy.deadline = moment()
    .add(1, 'day')
    .toISOString();
  const stud = await studs.createStud(copy);
  return stud;
}

function factoryDummyOffer (studId, offerBody) {
  const copy = { ...offerBody };
  if (copy.hasOwnProperty('offer_id')) {
    delete copy.offer_id;
  }
  if (copy.hasOwnProperty('helper_user_id')) {
    delete copy.helper_user_id;
  }
  if (copy.hasOwnProperty('status')) {
    delete copy.status;
  }
  copy.stud_id = studId;
  return copy;
}

before(async () => {
  await dbTools.initDb();
  helper = await factoryDummyUser(usersDataset.success[1].data);
  agent = await apiTools.logUserAndReturnAgent(helper.email, helper.password);
  seeker = await factoryDummyUser(usersDataset.success[0].data);
  stud = await factoryDummyStud(seeker.user_id, studsDataset.success[0].data);
  validOfferBody = factoryDummyOffer(
    stud.stud_id,
    offersDataset.success[0].data
  );
});

describe('POST /offer', () => {
  it('Should create offer and return it', async () => {
    const response = await agent
      .post('/api/v1/offer')
      .set('Accept', 'application/json')
      .send(validOfferBody);

    const offer = response.body;
    expect(offer.offer_id)
      .to.be.a('number')
      .and.to.be.greaterThan(0);
    const offerDb = await offers.getOffer(offer.offer_id);
    apiTools.expectSuccess(response, {
      offer_id: offerDb.offer_id,
      stud_id: stud.stud_id,
      helper_user_id: helper.user_id,
      meeting_date: validOfferBody.meeting_date,
      status: 'PENDING',
      creation_timestamp: offerDb.creation_timestamp
    });
    expect(response.body).to.be.eql(offerDb);
  });
  it('Should return bad request if body is empty', async () => {
    const response = await agent
      .post('/api/v1/offer')
      .set('Accept', 'application/json')
      .send({});

    apiTools.expectBadRequest(response);
  });
  it('Should return bad request if body is incorrect', async () => {
    invalidOfferBody = factoryDummyOffer(-1, offersDataset.success[0].data);
    const response = await agent
      .post('/api/v1/offer')
      .set('Accept', 'application/json')
      .send(invalidOfferBody);

    apiTools.expectBadRequest(response);
  });
  it('Should return bad request if body is valid but incoherent (inexistant stud_id)', async () => {
    invalidOfferBody = factoryDummyOffer(
      Number.MAX_SAFE_INTEGER,
      offersDataset.success[0].data
    );
    const response = await agent
      .post('/api/v1/offer')
      .set('Accept', 'application/json')
      .send(invalidOfferBody);

    apiTools.expectBadRequest(response);
  });
  it('should return 401 if user does not have a cookie', async () => {
    const notConnectedAgent = await apiTools.createAgent();
    const response = await notConnectedAgent
      .post('/api/v1/offer')
      .set('Accept', 'application/json');
    apiTools.expectUnauthorized(response);
  });
});

describe('GET /api/v1/offer/:offer_id', () => {
  it('Should return existing offer', async () => {
    const copy = { ...validOfferBody };
    copy.helper_user_id = helper.user_id;
    const offer = await offers.createOffer(copy);

    const response = await agent
      .get('/api/v1/offer/' + offer.offer_id)
      .set('Accept', 'application/json');
    apiTools.expectSuccess(response, offer);
  });
  it('Should return not found for inexistant offer_id', async () => {
    const response = await agent
      .get('/api/v1/offer/' + Number.MAX_SAFE_INTEGER)
      .set('Accept', 'application/json');
    apiTools.expectNotFound(response);
  });
  it('Should return bad request if offer_id is invalid', async () => {
    const response = await agent
      .get('/api/v1/offer/' + 'abc')
      .set('Accept', 'application/json');
    apiTools.expectBadRequest(response);
  });
  it('should return 401 if user does not have a cookie', async () => {
    const notConnectedAgent = await apiTools.createAgent();
    const response = await notConnectedAgent
      .get('/api/v1/offer/' + Number.MAX_SAFE_INTEGER)
      .set('Accept', 'application/json');
    apiTools.expectUnauthorized(response);
  });
});

describe('GET /api/v1/offer/list', () => {
  it('Should return existing offer', async () => {
    const copy = { ...validOfferBody };
    copy.helper_user_id = helper.user_id;
    const nb = 4;
    const list = [];
    copy.meeting_date = '["9999-01-22 14:30:00+00","9999-01-22 15:30:00+00"]';
    for (let i = 0; i < nb; i++) {
      copy.creation_timestamp = moment.utc().toISOString();
      list.push(await offers.createOffer(copy));
    }

    const response = await agent
      .get('/api/v1/offer/list')
      .query({ creationTimestamp: moment.utc().toISOString(), limit: nb })
      .set('Accept', 'application/json');
    apiTools.expectSuccess(
      response,
      list.sort((a, b) => b.offer_id - a.offer_id)
    );
  });
});

describe('DELETE /api/v1/offer/:offer_id', () => {
  it('Should delete existing offer', async () => {
    const copy = { ...validOfferBody };
    copy.helper_user_id = helper.user_id;
    const offer = await offers.createOffer(copy);

    const response = await agent
      .delete('/api/v1/offer/' + offer.offer_id)
      .set('Accept', 'application/json');

    apiTools.expectSuccess(response, offer);

    const offerExists = await offers.offerExists(offer.offer_id);
    expect(offerExists).to.be.false;
  });
  it('Should return not found for inexistant offer_id', async () => {
    const response = await agent
      .delete('/api/v1/offer/' + Number.MAX_SAFE_INTEGER)
      .set('Accept', 'application/json');
    apiTools.expectNotFound(response);
  });
  it('Should return bad request if offer_id is invalid', async () => {
    const response = await agent
      .delete('/api/v1/offer/' + 'abc')
      .set('Accept', 'application/json');
    apiTools.expectBadRequest(response);
  });
  it('should return 401 if user does not have a cookie', async () => {
    const notConnectedAgent = await apiTools.createAgent();
    const response = await notConnectedAgent
      .delete('/api/v1/offer/' + Number.MAX_SAFE_INTEGER)
      .set('Accept', 'application/json');
    apiTools.expectUnauthorized(response);
  });
});

describe('PUT /api/v1/offer', () => {
  it('Should update offer and return it and not update others', async () => {
    const copy = { ...validOfferBody };
    copy.helper_user_id = helper.user_id;
    const offersArray = [];
    let offersDb = await offers.getOffersByStudId(stud.stud_id);

    for (const toDelete of offersDb) {
      offers.deleteOffer(toDelete.offer_id);
    }

    for (let i = 0; i < 3; i++) {
      offersArray.push(await offers.createOffer(copy));
    }
    offersArray[0].status = 'REJECTED';

    const response = await agent
      .put('/api/v1/offer')
      .set('Accept', 'application/json')
      .send(offersArray[0]);

    apiTools.expectSuccess(response, {
      offer_id: offersArray[0].offer_id,
      stud_id: stud.stud_id,
      helper_user_id: helper.user_id,
      meeting_date: validOfferBody.meeting_date,
      status: offersArray[0].status,
      creation_timestamp: offersArray[0].creation_timestamp
    });

    offersDb = await offers.getOffer(offersArray[0].offer_id);
    expect(offersDb).to.be.eql(offersArray[0]);

    offersDb = await offers.getOffersByStudId(offersArray[0].stud_id);
    expect(offersDb.sort((a, b) => a.offer_id - b.offer_id)).to.be.eql(
      offersArray.sort((a, b) => a.offer_id - b.offer_id)
    );
  });

  it('Should update offer and reject others', async () => {
    const copy = { ...validOfferBody };
    copy.helper_user_id = helper.user_id;
    const offersArray = [];
    let offersDb = await offers.getOffersByStudId(stud.stud_id);

    for (const toDelete of offersDb) {
      offers.deleteOffer(toDelete.offer_id);
    }

    for (let i = 0; i < 3; i++) {
      offersArray.push(await offers.createOffer(copy));
      offersArray[i].status = 'REJECTED';
    }

    offersArray[0].status = 'ACCEPTED';

    const response = await agent
      .put('/api/v1/offer')
      .set('Accept', 'application/json')
      .send(offersArray[0]);

    offersArray[0].status = 'SCHEDULED';
    apiTools.expectSuccess(response, {
      offer_id: offersArray[0].offer_id,
      stud_id: stud.stud_id,
      helper_user_id: helper.user_id,
      meeting_date: validOfferBody.meeting_date,
      status: offersArray[0].status,
      creation_timestamp: offersArray[0].creation_timestamp
    });

    offersDb = await offers.getOffersByStudId(stud.stud_id);
    expect(offersDb.sort((a, b) => a.offer_id - b.offer_id)).to.be.eql(
      offersArray.sort((a, b) => a.offer_id - b.offer_id)
    );
  });

  it('Should return bad request if body is empty', async () => {
    const response = await agent
      .put('/api/v1/offer')
      .set('Accept', 'application/json')
      .send({});
    apiTools.expectBadRequest(response);
  });

  it('Should return bad request if body is incorrect', async () => {
    const copy = { ...validOfferBody };
    copy.helper_user_id = helper.user_id;
    const offer = await offers.createOffer(copy);
    offer.status = 'NOT A STATUS';

    const response = await agent
      .put('/api/v1/offer')
      .set('Accept', 'application/json')
      .send(offer);

    apiTools.expectBadRequest(response);
  });

  it('Should return bad request if body is valid but incoherent (inexistant stud_id)', async () => {
    const copy = { ...validOfferBody };
    copy.helper_user_id = helper.user_id;
    const offer = await offers.createOffer(copy);
    offer.stud_id = Number.MAX_SAFE_INTEGER;

    const response = await agent
      .put('/api/v1/offer')
      .set('Accept', 'application/json')
      .send(offer);

    apiTools.expectBadRequest(response);
  });
  it('should return 401 if user does not have a cookie', async () => {
    const notConnectedAgent = await apiTools.createAgent();
    const response = await notConnectedAgent
      .put('/api/v1/offer')
      .set('Accept', 'application/json');
    apiTools.expectUnauthorized(response);
  });
});
