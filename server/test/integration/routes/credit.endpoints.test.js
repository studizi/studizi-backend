/* eslint-disable no-undef */
/* eslint-disable camelcase */
const { expect } = require('chai');
const chai = require('chai');
chai.use(require('chai-like'));
chai.use(require('chai-things'));
const dbTools = require('../../../database/tools/execute.sql.order');
const createAgent = require('../../utils/agent.tools').createAgent;
const emailGenerator = require('../../utils/email.generator').emailGenerator;
const ApiTools = require('../../utils/agent.tools');
const Constants = require('../../../utils/constants.json');
const userDataSet = require('../../resources/users.dataset.json');
const creditDataSet = require('../../resources/credit.dataset.json');
const dummyCredit = { ...creditDataSet.success[0].data };
// TODO USE ERROR METHODS

const dummyCreditWithGold = { ...creditDataSet.success[1].data };

const dummyCreditWithoutSilver = { ...creditDataSet.success[4].data };
beforeEach(() => {
  userDataSet.success[2].data.email = emailGenerator();
});

before(async () => {
  await dbTools.initDb();
});

after(async () => {
  await dbTools.deleteDb();
});

describe('GET /api/v1/credit/', async () => {
  it('expect credit to be found and with correct values', async () => {
    await ApiTools.createUserAndLogin(async (agent, user) => {
      const response = await agent.get('/api/v1/credit/').set('Accept', 'application/json').expect(200);
      expect(response).to.be.a('object');
      expect(response.body.silver_credit).to.be.eql(Constants.credits.initial_silver_credit);
      expect(response.body.gold_credit).to.be.eql(Constants.credits.initial_gold_credit);
    });
  });

  it('expecting 401 if no user connected', async () => {
    await ApiTools.createUserAndLogin(async (agent, user) => {
      const dummyAgent = await createAgent();
      const response = await dummyAgent.get('/api/v1/credit/').set('Accept', 'application/json');
      expect(response.statusCode).to.be.eql(401);
    });
  });
});

describe('put /api/v1/credit/add/:user_id', async () => {
  it('add silver credit and expect values to be updated', async () => {
    await ApiTools.createUserAndLogin(async (agent, user) => {
      const response = await agent.get('/api/v1/credit/').set('Accept', 'application/json').expect(200);
      const addResponse = await agent.put('/api/v1/credit/add/').send(dummyCredit).set('Accept', 'application/json').expect(200);
      expect(addResponse).to.be.a('object');
      expect(addResponse.body.silver_credit).to.be.eql(response.body.silver_credit + 50);
    });
  });

  it('add gold credit and expect values to be updated', async () => {
    await ApiTools.createUserAndLogin(async (agent, user) => {
      const response = await agent.get('/api/v1/credit/').set('Accept', 'application/json').expect(200);
      const addResponse = await agent.put('/api/v1/credit/add/').send(dummyCreditWithoutSilver).set('Accept', 'application/json').expect(200);
      expect(addResponse).to.be.a('object');
      expect(addResponse.body.gold_credit).to.be.eql(response.body.gold_credit + 50);
    });
  });

  it('add silver credit and gold credit and expect values to be updated', async () => {
    await ApiTools.createUserAndLogin(async (agent, user) => {
      const response = await agent.get('/api/v1/credit/').set('Accept', 'application/json').expect(200);
      const addResponse = await agent.put('/api/v1/credit/add/').send(dummyCreditWithGold).set('Accept', 'application/json').expect(200);
      expect(addResponse).to.be.a('object');
      expect(addResponse.body.silver_credit).to.be.eql(response.body.silver_credit + 50);
      expect(addResponse.body.gold_credit).to.be.eql(response.body.gold_credit + 50);
    });
  });

  it('send text and expect values to not be updated', async () => {
    await ApiTools.createUserAndLogin(async (agent, user) => {
      await agent.get('/api/v1/credit/').set('Accept', 'application/json').expect(200);
      const newDummyCredit = { ...creditDataSet.failure[2].data };
      await agent.put('/api/v1/credit/add/').send(newDummyCredit).set('Accept', 'application/json').expect(400);
      const removeResponse = await agent.get('/api/v1/credit/').set('Accept', 'application/json').expect(200);
      expect(removeResponse.body.silver_credit).to.be.eql(Constants.credits.initial_silver_credit);
    });
  });

  it('send text (twice) and expect values to not be updated', async () => {
    await ApiTools.createUserAndLogin(async (agent, user) => {
      await agent.get('/api/v1/credit/').set('Accept', 'application/json').expect(200);
      const newDummyCredit = { ...creditDataSet.failure[3].data };
      await ApiTools.check400BadRequest(async (agent1) => {
        return await agent.put('/api/v1/credit/add/').send(newDummyCredit).set('Accept', 'application/json');
      });
      const removeResponse = await agent.get('/api/v1/credit/').set('Accept', 'application/json').expect(200);
      expect(removeResponse.body.silver_credit).to.be.eql(Constants.credits.initial_silver_credit);
    });
  });
});

describe('put /api/v1/credit/remove/:user_id', async () => {
  it('remove silver credit and expect values to be updated', async () => {
    await ApiTools.createUserAndLogin(async (agent, user) => {
      const response = await agent.get('/api/v1/credit/').set('Accept', 'application/json').expect(200);
      const removeCredit = { ...creditDataSet.success[2].data };
      const removeResponse = await agent.put('/api/v1/credit/remove/').send(removeCredit).set('Accept', 'application/json').expect(200);
      expect(removeResponse).to.be.a('object');
      expect(removeResponse.body.silver_credit).to.be.eql(response.body.silver_credit - 50);
    });
  });

  it('remove gold credit and expect values to be updated', async () => {
    await ApiTools.createUserAndLogin(async (agent, user) => {
      await agent.put('/api/v1/credit/add/').send(dummyCreditWithoutSilver).set('Accept', 'application/json').expect(200);
      const response = await agent.get('/api/v1/credit/').set('Accept', 'application/json').expect(200);
      const removeCredit = { ...creditDataSet.success[4].data };
      const removeResponse = await agent.put('/api/v1/credit/remove/').send(removeCredit).set('Accept', 'application/json').expect(200);
      expect(removeResponse).to.be.a('object');
      expect(removeResponse.body.gold_credit).to.be.eql(response.body.gold_credit - 50);
    });
  });

  it('remove silver credit and gold credit and expect values to be updated', async () => {
    await ApiTools.createUserAndLogin(async (agent, user) => {
      await agent.get('/api/v1/credit/').set('Accept', 'application/json').expect(200);
      const response = await agent.put('/api/v1/credit/add/').send(dummyCreditWithGold).set('Accept', 'application/json').expect(200);
      const removeCreditWithGold = { ...creditDataSet.success[3].data };
      const removeResponse = await agent.put('/api/v1/credit/remove/').send(removeCreditWithGold).set('Accept', 'application/json').expect(200);
      expect(removeResponse).to.be.a('object');
      expect(removeResponse.body.silver_credit).to.be.eql(response.body.silver_credit - 50);
      expect(removeResponse.body.gold_credit).to.be.eql(response.body.gold_credit - 50);
    });
  });

  it('remove silver credit and expect values to not be updated', async () => {
    await ApiTools.createUserAndLogin(async (agent, user) => {
      await agent.get('/api/v1/credit/').set('Accept', 'application/json').expect(200);
      const removeDummyCredit = { ...creditDataSet.failure[0].data };
      await agent.put('/api/v1/credit/remove/').send(removeDummyCredit).set('Accept', 'application/json').expect(400);
      const removeResponse = await agent.get('/api/v1/credit').set('Accept', 'application/json').expect(200);
      expect(removeResponse.body.silver_credit).to.be.eql(Constants.credits.initial_silver_credit);
    });
  });

  it('remove gold credit and expect values to not be updated', async () => {
    await ApiTools.createUserAndLogin(async (agent, user) => {
      await agent.get('/api/v1/credit/').set('Accept', 'application/json').expect(200);
      const removeDummyCredit = { ...creditDataSet.failure[4].data };
      await agent.put('/api/v1/credit/remove/').send(removeDummyCredit).set('Accept', 'application/json').expect(400);
      const removeResponse = await agent.get('/api/v1/credit').set('Accept', 'application/json').expect(200);
      expect(removeResponse.body.gold_credit).to.be.eql(0);
    });
  });

  it('remove silver credit and gold credit and expect values to not be updated', async () => {
    await ApiTools.createUserAndLogin(async (agent, user) => {
      await agent.get('/api/v1/credit/').set('Accept', 'application/json').expect(200);
      const removeDummyCredit = { ...creditDataSet.failure[1].data };
      await agent.put('/api/v1/credit/remove/').send(removeDummyCredit).set('Accept', 'application/json').expect(400);
      const removeResponse = await agent.get('/api/v1/credit/').set('Accept', 'application/json').expect(200);
      expect(removeResponse.body.silver_credit).to.be.eql(Constants.credits.initial_silver_credit);
      expect(removeResponse.body.gold_credit).to.be.eql(Constants.credits.initial_gold_credit);
    });
  });

  it('send text and expect values to not be updated', async () => {
    await ApiTools.createUserAndLogin(async (agent, user) => {
      await agent.get('/api/v1/credit/').set('Accept', 'application/json').expect(200);
      const newDummyCredit = { ...creditDataSet.failure[2].data };
      await agent.put('/api/v1/credit/remove/').send(newDummyCredit).set('Accept', 'application/json').expect(400);
      const removeResponse = await agent.get('/api/v1/credit/').set('Accept', 'application/json').expect(200);
      expect(removeResponse.body.silver_credit).to.be.eql(Constants.credits.initial_silver_credit);
    });
  });

  it('send text (twice) and expect values to not be updated', async () => {
    await ApiTools.createUserAndLogin(async (agent, user) => {
      await agent.get('/api/v1/credit/').set('Accept', 'application/json').expect(200);
      const newDummyCredit = { ...creditDataSet.failure[3].data };
      await agent.put('/api/v1/credit/remove/').send(newDummyCredit).set('Accept', 'application/json').expect(400);
      const removeResponse = await agent.get('/api/v1/credit/').set('Accept', 'application/json').expect(200);
      expect(removeResponse.body.silver_credit).to.be.eql(Constants.credits.initial_silver_credit);
    });
  });
});
