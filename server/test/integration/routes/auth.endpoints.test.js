/* eslint-disable no-undef */
const { expect } = require('chai');
require('dotenv').config({ path: __dirname + '/../../../../.env' });
const chai = require('chai');
chai.use(require('chai-like'));
chai.use(require('chai-things'));
const dbTools = require('../../../database/tools/execute.sql.order');
const emailGenerator = require('../../utils/email.generator').emailGenerator;
const ApiTools = require('../../utils/agent.tools');
const usersDataset = require('../../resources/users.dataset');
const discord = require('../../../services/discord.service');

beforeEach(() => {
  usersDataset.success[2].data.email = emailGenerator();
});

before(async function () {
  await dbTools.initDb();
  await discord.createDiscordClient();
});

after(async function () {
  await require('../../../services/scheduled.jobs').deleteExpiredMeeting();
  discord.closeConnection();
  await dbTools.deleteDb();
});

async function createUser (agent, user) {
  return await agent.post('/auth/signup')
    .set('Accept', 'application/json')
    .send(user);
}

describe('POST /auth/signup', async () => {
  it('should create a new account if body is correct', async () => {
    const agent = await ApiTools.createAgent();
    const copy = { ...usersDataset.success[2].data };
    const response = await createUser(agent, copy);

    expect(response.status)
      .to.equal(200);

    const returnedObj = response.body;

    expect(returnedObj)
      .to.be.an('object');

    expect(returnedObj.user_id)
      .to.be.a('number')
      .and.to.be.greaterThan(0);

    delete returnedObj.user_id;
    delete copy.password;
    copy.verified = true;
    expect(returnedObj)
      .to.eql(copy);
  });

  it('should return 401 if user with same email already exist', async () => {
    await ApiTools.check401Unauthorized(async (agent) => {
      await createUser(agent, usersDataset.success[2].data);
      return await createUser(agent, usersDataset.success[2].data);
    });
  });

  it('should return 400 if body is empty', async () => {
    await ApiTools.check400BadRequest(async (agent) => {
      return await agent.post('/auth/signup')
        .set('Accept', 'application/json');
    });
  });

  it('should return 400 if body is not valid', async () => {
    for (const [key] of Object.entries(usersDataset.success[2].data)) {
      console.log(key);
      const copy = { ...usersDataset.success[2].data };
      await ApiTools.check400BadRequest(async (agent) => {
        copy[key] = usersDataset.failures[0].data[key];
        return await createUser(agent, copy);
      });
    }
  });
  it('should redirect to / if user already connected', async () => {
    const agent = await ApiTools.createAgent();
    const copy = { ...usersDataset.success[2].data };
    await createUser(agent, copy);
    const credentials = {
      email: copy.email,
      password: copy.password
    };
    await agent.post('/auth/login')
      .set('Accept', 'application/json')
      .send(credentials)
      .expect(302);

    const response = await createUser(agent, copy);

    expect(response.status)
      .to.equal(302);

    expect(response.header.location)
      .to.equal('/');
  });
});

describe('POST /auth/login', async () => {
  it('should login and send the correct info about user', async () => {
    const agent = await ApiTools.createAgent();
    const copy = { ...usersDataset.success[2].data };
    await createUser(agent, copy);
    const credentials = {
      email: copy.email,
      password: copy.password
    };
    const response = await agent.post('/auth/login')
      .set('Accept', 'application/json')
      .send(credentials)
      .expect(302);

    expect(response.header.location)
      .to.equal('/');
  });

  it('should return 302 login if credentials are not correct', async () => {
    const agent = await ApiTools.createAgent();
    const copy = { ...usersDataset.success[2].data };
    await createUser(agent, copy);
    const credentials = {
      email: copy.email,
      password: 'not correct'
    };
    const response = await agent.post('/auth/login')
      .set('Accept', 'application/json')
      .send(credentials)
      .expect(302);

    expect(response.header.location)
      .to.equal('/login');
  });

  it('should return 400 if body is empty', async () => {
    const agent = await ApiTools.createAgent();
    const response = await agent.post('/auth/login')
      .set('Accept', 'application/json')
      .expect(302);
    expect(response.header.location)
      .to.equal('/login');
  });

  it('should return 302 /login if credentials are not valid', async () => {
    const agent = await ApiTools.createAgent();
    const credentials = {
      email: 'email',
      password: '"'
    };
    const response = await agent.post('/auth/login')
      .set('Accept', 'application/json')
      .send(credentials)
      .expect(302);

    expect(response.header.location)
      .to.equal('/login');
  });
});

describe('GET /auth/logout', async () => {
  it('should return 200 and logout', async () => {
    const agent = await ApiTools.createAgent();
    const copy = { ...usersDataset.success[2].data };
    await createUser(agent, copy);
    const credentials = {
      email: copy.email,
      password: copy.password
    };
    await agent.post('/auth/login')
      .set('Accept', 'application/json')
      .send(credentials)
      .expect(302);
    await agent.get('/auth/logout')
      .expect(200);
  });
  it('should return 401 if user does not has cookie', async () => {
    const agent = await ApiTools.createAgent();
    await createUser(agent, usersDataset.success[2].data);
    await agent.get('/auth/logout')
      .expect(401);
  });
});

describe('GET /auth/me', async () => {
  it('should return the correct info about user', async () => {
    await ApiTools.createUserAndLogin(async (agent, returnedUser) => {
      const response = await agent.get('/auth/me')
        .set('Accept', 'application/json')
        .expect(200);

      const returnedObj = response.body;
      expect(returnedObj)
        .to.be.an('object');

      expect(returnedObj.user_id)
        .to.be.a('number')
        .and.to.be.greaterThan(0);

      expect(returnedObj)
        .to.eql(returnedUser);
    });
  });

  it('should return 401 if user does not have a cookie', async () => {
    await ApiTools.check401Unauthorized(async (agent) => {
      return await agent.get('/auth/me')
        .set('Accept', 'application/json');
    });
  });
});
