const { expect } = require('chai');
const chai = require('chai');
// eslint-disable-next-line node/no-path-concat


chai.use(require('chai-like'));
chai.use(require('chai-things'));
const request = require('supertest');
const express = require('express');
const SHA256 = require('js-sha256');
const usersDataset = require('../resources/users.dataset');
const User = require('../../database/models/user.model');
require('../../services/email.service').createTransporter();
const app = express();
const ResponseModels = require('../../responses/response.models.json');
const emailGenerator = require('./email.generator').emailGenerator;

require('../../loaders/session.loader')(app);
const passport = require('../../loaders/passport.loader')(app);
require('../../routes/routes')(app, passport);

async function createAgent () {
  const agent = request.agent(app);
  return agent;
}

async function check400BadRequest (cb) {
  const agent = await createAgent();
  const response = await cb(agent);
  expect(response.statusCode).to.equal(
    ResponseModels['Bad Request'].statusCode
  );

  expect(response.body).is.eql(ResponseModels['Bad Request'].errorMessage);
}
async function check401Unauthorized (cb) {
  const agent = await createAgent();

  const response = await cb(agent);
  expect(response.statusCode).to.be.equal(
    ResponseModels.Unauthorized.statusCode
  );

  expect(response.body).is.eql(ResponseModels.Unauthorized.errorMessage);
}

async function check404NotFound (cb) {
  const agent = await createAgent();

  const response = await cb(agent);
  expect(response.statusCode).to.equal(ResponseModels['Not Found'].statusCode);

  expect(response.body).is.eql(ResponseModels['Not Found'].errorMessage);
}

async function check403Forbidden (cb) {
  const agent = await createAgent();

  const response = await cb(agent);
  expect(response.statusCode).to.equal(ResponseModels.Forbidden.statusCode);

  expect(response.body).is.eql(ResponseModels.Forbidden.errorMessage);
}

async function createUserAndLogin (cb) {
  const agent = await createAgent();
  usersDataset.success[2].data.email = emailGenerator();
  const copy = { ...usersDataset.success[2].data };
  copy.password = SHA256(copy.password);
  const returnedUser = await User.createUser(copy);
  const credentials = {
    email: copy.email,
    password: usersDataset.success[2].data.password
  };
  await agent
    .post('/auth/login')
    .set('Accept', 'application/json')
    .send(credentials)
    .expect(302);

  await cb(agent, returnedUser);
}

function expectResponse (res, expectedCode, expectedBody = null) {
  expect(res.statusCode).to.be.eql(expectedCode);
  if (expectedBody !== null) {
    expect(res.body).to.be.eql(expectedBody);
  }
}

function expectBadRequest (res) {
  expectResponse(
    res,
    ResponseModels['Bad Request'].statusCode,
    ResponseModels['Bad Request'].errorMessage
  );
}

function expectUnauthorized (res) {
  expectResponse(
    res,
    ResponseModels.Unauthorized.statusCode,
    ResponseModels.Unauthorized.errorMessage
  );
}

function expectNotFound (res) {
  expectResponse(
    res,
    ResponseModels['Not Found'].statusCode,
    ResponseModels['Not Found'].errorMessage
  );
}
function expectSuccess (res, expectedBody = null) {
  expectResponse(res, ResponseModels.OK.statusCode, expectedBody);
}

async function logUserAndReturnAgent (email, password) {
  const agent = await createAgent();
  const credentials = {
    email: email,
    password: password
  };
  await agent
    .post('/auth/login')
    .set('Accept', 'application/json')
    .send(credentials)
    .expect(302);
  return agent;
}
module.exports = {
  logUserAndReturnAgent,
  expectSuccess,
  expectNotFound,
  expectBadRequest,
  expectUnauthorized,
  createAgent,
  check400BadRequest,
  check403Forbidden,
  check401Unauthorized,
  check404NotFound,
  createUserAndLogin
};
