const emailGenerator = (function () {
  let i = 1;

  return function () {
    return `test${i++}@email.com`;
  };
})();

module.exports = { emailGenerator };
