const winston = require('winston');
const colorizer = winston.format.colorize({
  all: true
});
const levels = require('./log.levels.json');
winston.addColors(levels.colors);
const logger = winston.createLogger({
  level: 'silly',
  levels: levels.levels,
  transports: [
    new winston.transports.Console({
      format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.align(),
        winston.format.simple(),
        winston.format.printf((msg) =>
          colorizer.colorize(
            msg.level,
            `${msg.timestamp} - [${msg.level.toUpperCase()}]: ${msg.message}`
          )
        )
      ),
      colorize: true
    })
  ]
});

/**
 * @param {String} origin
 * @param {Number} level
 * @param {Object} data
 */
function log (origin, level, data = null) {
  const str = `@${origin} ${data}`;
  switch (level) {
    case levels.levels.error:
      logger.error(str);
      break;
    case levels.levels.warn:
      logger.warn(str);
      break;
    case levels.levels.info:
      logger.info(str);
      break;
    case levels.levels.http:
      logger.http(str);
      break;
    case levels.levels.verbose:
      logger.verbose(str);
      break;
    case levels.levels.debug:
      logger.debug(str);
      break;
    case levels.levels.silly:
      logger.silly(str);
      break;
  }
}
module.exports = log;
