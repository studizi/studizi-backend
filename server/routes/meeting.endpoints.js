// @ts-nocheck
/* eslint-disable camelcase */
const express = require('express');
const router = express.Router();
const ResponseSuccess = require('../responses/response.success');
const ErrorMiddleware = require('../middlewares/error.middleware');
const Meeting = require('../database/models/meeting.model');
const ResponseError = require('../responses/response.error');
const { meetingExists } = require('../middlewares/resources.exists');
const { isConnected } = require('../middlewares/is.connected');
const moment = require('moment');
// eslint-disable-next-line no-useless-escape

router.get('/list',
  ErrorMiddleware(async function (req, res, next) {
    let {
      creationTimestamp = moment.utc().toISOString(),
      limit = 10,
      offset = 0,
      offer_id = -1,
      helper_user_id = -1,
      stud_owner_id = -1
    } = req.query;
    limit = parseInt(limit);
    offset = parseInt(offset);
    offer_id = parseInt(offer_id);
    helper_user_id = parseInt(helper_user_id);
    stud_owner_id = parseInt(stud_owner_id);
    creationTimestamp = moment.utc(creationTimestamp).toISOString();

    if (creationTimestamp == null || isNaN(limit) || isNaN(offset)) {
      ResponseError.badRequest(res);
      return;
    }
    if (limit < 0 || offset < 0) {
      ResponseError.badRequest(res);
      return;
    }

    const meetings = await Meeting.getMeetingList(creationTimestamp, limit, offset, offer_id, helper_user_id, stud_owner_id);
    meetings === null ? ResponseError.internalServerError(res) : ResponseSuccess.success(res, meetings);
  }));

router.get(
  '/:offer_id',
  isConnected,
  meetingExists,
  ErrorMiddleware(async function (req, res, next) {
    const offer_id = req.params.offer_id;
    const meeting = await Meeting.getMeetingByOfferId(offer_id);
    meeting == null ? ResponseError.internalServerError(res) : ResponseSuccess.success(res, meeting);
  })
);

router.delete(
  '/:offer_id',
  isConnected,
  meetingExists,
  ErrorMiddleware(async function (req, res, next) {
    const offer_id = req.params.offer_id;
    const meeting = await Meeting.deleteMeeting(offer_id);
    meeting == null ? ResponseError.notFound(res) : ResponseSuccess.success(res, meeting);
  })
);

module.exports = router;
