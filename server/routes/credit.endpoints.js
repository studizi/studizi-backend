/* eslint-disable no-undef */
/* eslint-disable camelcase */
/* eslint-disable prefer-const */
const express = require('express');
const router = express.Router();
const ResponseSuccess = require('../responses/response.success');
const ResponseError = require('../responses/response.error');
const ErrorMiddleware = require('../middlewares/error.middleware');
const Credit = require('../database/models/credit.model');
const { isAValidInstance } = require('../utils/schema.validator');
const schema = require('../database/schemas/credit.schema');
const logger = require('../log/winston.logger');
const logLevels = require('../log/log.levels.json').levels;
const { creditExists } = require('../middlewares/resources.exists');
const handleCreditBody = require('../middlewares/body.handler').handleCreditBody;

router.get(
  '/',
  handleCreditBody,
  creditExists,
  ErrorMiddleware(async function (req, res, next) {
    const user_id = req.user.user_id;
    if (!user_id) {
      ResponseError.notFound(res);
    }
    const credit = await Credit.getCredit(user_id);
    credit == null ? ResponseError.internalServerError(res) : ResponseSuccess.success(res, credit);
  })
);

router.put(
  '/add/',
  handleCreditBody,
  creditExists,
  ErrorMiddleware(async function (req, res, next) {
    const user_id = req.body.user_id;
    const credit_body = req.body;
    if (!isAValidInstance(credit_body, schema)) {
      logger('PUT /credit/add/', logLevels.warn, 'Invalid body');
      ResponseError.badRequest(res);
    }
    const silver_credit = req.body.silver_credit === undefined ? 0 : req.body.silver_credit;
    const gold_credit = req.body.gold_credit === undefined ? 0 : req.body.gold_credit;
    if (silver_credit === 0 && gold_credit === 0) {
      ResponseError.badRequest(res);
    }
    const credit = await Credit.addCredit(user_id, silver_credit, gold_credit);
    if (credit === null) {
      ResponseError.badRequest(res);
    } else {
      ResponseSuccess.success(res, await Credit.getCredit(user_id));
    }
  })
);

router.put(
  '/remove/',
  handleCreditBody,
  creditExists,
  ErrorMiddleware(async function (req, res, next) {
    const user_id = req.body.user_id;
    const credit_body = req.body;
    if (!isAValidInstance(credit_body, schema)) {
      logger('PUT /credit/remove/', logLevels.warn, 'Invalid body');
      ResponseError.badRequest(res);
    }
    const silver_credit = req.body.silver_credit === undefined ? 0 : req.body.silver_credit;
    const gold_credit = req.body.gold_credit === undefined ? 0 : req.body.gold_credit;
    if (silver_credit === 0 && gold_credit === 0) {
      ResponseError.badRequest(res);
    }
    const result = await Credit.removeCredit(user_id, silver_credit, gold_credit);
    if (result === null) {
      ResponseError.badRequest(res);
    } else {
      ResponseSuccess.success(res, await Credit.getCredit(user_id));
    }
  })
);

module.exports = router;
