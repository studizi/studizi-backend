/* eslint-disable camelcase */
const express = require('express');
const router = express.Router();
const ResponseSuccess = require('../responses/response.success');
const ErrorMiddleware = require('../middlewares/error.middleware');
const Rate = require('../database/models/rating.model');
const Stud = require('../database/models/stud.model');
const Offer = require('../database/models/offer.model');
const ResponseError = require('../responses/response.error');
const logger = require('../log/winston.logger');
const logLevels = require('../log/log.levels.json').levels;
const {
  isAValidInstance
} = require('../utils/schema.validator');
const schema = require('../database/schemas/rating.schema.json');
const { offerExists, ratingExists } = require('../middlewares/resources.exists');

router.get(
  '/list',
  // @ts-ignore
  ErrorMiddleware(async (req, res, next) => {
    let {
      offer_id = -1,
      limit = 10,
      offset = 0,
      rating_user_id = -1,
      rated_user_id = -1
    } = req.query;
    limit = parseInt(limit);
    offset = parseInt(offset);
    offer_id = parseInt(offer_id);
    rating_user_id = parseInt(rating_user_id);
    rated_user_id = parseInt(rated_user_id);

    if (limit < 0 || offset < 0) {
      ResponseError.badRequest(res);
      return;
    }
    if (rated_user_id < 0 && rating_user_id < 0 && offer_id < 0) {
      ResponseError.badRequest(res);
      return;

    }
    const ratings = await Rate.getRatingList(offer_id, rating_user_id, rated_user_id, limit, offset);
    // @ts-ignore
    ratings == null ? ResponseError.internalServerError(res) : ResponseSuccess.success(res, ratings);
  })
);

router.post(
  '/',
  offerExists,
  // @ts-ignore
  ErrorMiddleware(async (req, res, next) => {
    const rate = req.body;
    const offer = await Offer.getOffer(rate.offer_id);
    const stud = await Stud.getStud(offer.stud_id);
    switch (rate.type) {
      case 'SEEKER':
        rate.rated_user_id = offer.helper_user_id;
        rate.rating_user_id = stud.stud_owner_id;
        break;
      case 'HELPER':
        rate.rated_user_id = stud.stud_owner_id;
        rate.rating_user_id = offer.helper_user_id;
        break;
    }

    // @ts-ignore
    if (!isAValidInstance(rate, schema)) {
      logger('POST /api/v1/rating', logLevels.warn, 'Invalid body');
      ResponseError.badRequest(res);
      return;
    }

    const newRate = await Rate.createRating(rate);
    if (newRate) {
      // @ts-ignore
      ResponseSuccess.success(res, newRate);
      return;
    }
    ResponseError.internalServerError(res);
  })
);

router.delete(
  '/:rating_id',
  ratingExists,
  // @ts-ignore
  ErrorMiddleware(async (req, res, next) => {
    const rateId = req.params.rating_id;
    const rate = await Rate.getRating(rateId);
    if (rate.rating_user_id !== req.user.user_id) {
      ResponseError.notFound(res);
      return;
    }
    const deletedRate = await Rate.deleteRating(rateId);
    if (rate !== null) {
      // @ts-ignore
      ResponseSuccess.success(res, deletedRate);
      return;
    }
    ResponseError.notFound(res);
  })
);

router.get(
  '/:rating_id',
  ratingExists,
  // @ts-ignore
  ErrorMiddleware(async (req, res, next) => {
    const rateId = req.params.rating_id;
    const rate = await Rate.getRating(rateId);
    if (rate !== null) {
      // @ts-ignore
      ResponseSuccess.success(res, rate);
      return;
    }
    ResponseError.notFound(res);
  })
);

router.put(
  '/:rating_id',
  ratingExists,
  // @ts-ignore
  ErrorMiddleware(async (req, res, next) => {
    const rate = req.body;
    // @ts-ignore
    if (!isAValidInstance(rate, schema)) {
      logger('PUT /updateRate', logLevels.warn, 'Invalid body');
      ResponseError.badRequest(res);
      return;
    }
    const rating = await Rate.getRating(rate.rating_id);
    if (rating.rating_user_id !== req.user.user_id) {
      ResponseError.notFound(res);
      return;
    }
    const updateRate = await Rate.updateRating(rate);
    if (updateRate) {
      // @ts-ignore
      ResponseSuccess.success(res, updateRate);
      return;
    }
    ResponseError.internalServerError(res);
  })
);
module.exports = router;
