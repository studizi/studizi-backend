const { isConnected } = require('../middlewares/is.connected');

module.exports = function (app, passport) {
  const authRouter = require('./auth.endpoints')(passport);
  const offerRouter = require('./offer.endpoints');
  const studRouter = require('./stud.endpoints');
  const userRouter = require('./user.endpoints');
  const creditRouter = require('./credit.endpoints');
  const meetingRouter = require('./meeting.endpoints');
  const ratingRouter = require('./rating.endpoints');
  const uploadRouter = require('./upload.endpoints');
  const logger = require('../log/winston.logger');
  const logLevels = require('../log/log.levels.json').levels;
  app.use(function (req, res, next) {
    logger(req.method + ' ' + req.url, logLevels.http);
    next();
  });

  app.use('/auth', authRouter);
  app.use('/api/v1/upload', uploadRouter);
  app.use(isConnected);
  app.use('/api/v1/offer', offerRouter);
  app.use('/api/v1/stud', studRouter);
  app.use('/api/v1/user', userRouter);
  app.use('/api/v1/credit', creditRouter);
  app.use('/api/v1/meeting', meetingRouter);
  app.use('/api/v1/rating', ratingRouter);
};
