const express = require('express');
const router = express.Router();
const ResponseSuccess = require('../responses/response.success');
const ErrorMiddleware = require('../middlewares/error.middleware');
const ResponseError = require('../responses/response.error');
const awsS3Service = require('../services/aws.s3.service');
const multer = require('multer');
const upload = multer({ dest: 'uploads/' });
const type = upload.single('file');
const User = require('../database/models/user.model');

router.post('/', type, ErrorMiddleware(async (req, res, next) => {
  if (req.user === null) {
    const { email, password } = req.body;
    const user = User.getUserIdIfExist(email, password);
    if (user === null) {
      ResponseError.unauthorized(res);
      return;
    }
  }
  if (req.file === null) {
    ResponseError.badRequest(res);
    return;
  }
  const uploadedFile = await awsS3Service.addFileToS3(req.file);
  if (uploadedFile === null) {
    ResponseError.internalServerError(res);
    return;
  }
  ResponseSuccess.success(res, {
    url: uploadedFile
  });
}));

module.exports = router;
