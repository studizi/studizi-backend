/* eslint-disable no-unused-expressions */
const express = require('express');
const evt = require('../utils/environment');
const router = express.Router();
const ResponseSuccess = require('../responses/response.success');
const ErrorMiddleware = require('../middlewares/error.middleware');
const User = require('../database/models/user.model');
const EmailVerification = require('../database/models/email.verification.model');
const logger = require('../log/winston.logger');
const SHA256 = require('js-sha256');
const logLevels = require('../log/log.levels.json').levels;
const ResponseError = require('../responses/response.error');
const { isAValidInstance } = require('../utils/schema.validator');
const userSchema = require('../database/schemas/user.schema.json');
const { isConnected } = require('../middlewares/is.connected');
const { sendSignupMail } = require('../services/email.service');
module.exports = function (passport) {
  router.get(
    '/logout',
    isConnected,
    ErrorMiddleware(function (req, res, next) {
      req.logout();
      res.clearCookie('connect.sid', {
        path: '/'
      });
      // eslint-disable-next-line node/handle-callback-err
      req.session.destroy(function (err) {
        ResponseSuccess.success(res);
      });
    })
  );

  router.get(
    '/me',
    isConnected,
    ErrorMiddleware(async (req, res, next) => {
      ResponseSuccess.success(res, req.user);
    })
  );

  router.post(
    '/login',
    passport.authenticate('local', {
      successRedirect: '/',
      failureRedirect: '/login'
    })
  );

  router.post(
    '/signup',
    ErrorMiddleware(async (req, res, next) => {
      if (req.user !== undefined) {
        res.redirect('/');
        return;
      }
      const user = req.body;
      if (user.profile_picture == null) {
        user.profile_picture = 'https://picsum.photos/id/11/500/300';
      }
      if (!isAValidInstance(user, userSchema)) {
        logger('POST /auth/signup', logLevels.warn, 'Invalid body');
        ResponseError.badRequest(res);
        return;
      }

      user.password = SHA256(user.password);
      const newUser = await User.createUser(user);
      if (newUser !== null) {
        if (evt('ENV') === 'PROD') {
          const created = await EmailVerification.createEmailVerification(newUser.user_id);
          const url = `${evt('APPLICATION_URL')}/verification/${created.verification_code}`;
          sendSignupMail(user.email, user.first_name, url);
        }
        ResponseSuccess.success(res, newUser);
      } else {
        ResponseError.unauthorized(res);
      }
    })
  );
  router.get(
    '/verification/:verification_code',
    ErrorMiddleware(async (req, res, next) => {
      const verification = await EmailVerification.verificationCodeIsValid(req.params.verification_code);
      if (verification === null) {
        ResponseError.badRequest(res);
        return;
      }
      const user = await User.getUser(verification.user_id);
      user.verified = true;
      const updatedUser = await User.updateUser(user);
      if (updatedUser !== null) {
        await EmailVerification.deleteVerificationCode(updatedUser.user_id);
        ResponseSuccess.success(res, updatedUser);
        return;
      }
      ResponseError.internalServerError(res);
    })
  );

  return router;
};
