/* eslint-disable camelcase */
const express = require('express');
const router = express.Router();
const ResponseSuccess = require('../responses/response.success');
const ErrorMiddleware = require('../middlewares/error.middleware');
const ResponseError = require('../responses/response.error');
const Offers = require('../database/models/offer.model');
const logger = require('../log/winston.logger');
const logLevels = require('../log/log.levels.json').levels;
const responseSuccess = require('../responses/response.success');
const Studs = require('../database/models/stud.model');
const discord = require('../services/discord.service');
const Stud = require('../database/models/stud.model');
const Meeting = require('../database/models/meeting.model');
const email = require('../services/email.service');
const User = require('../database/models/user.model');
const {
  validatesOfferSchema
} = require('../middlewares/body.validates.schema');
const { offerExists } = require('../middlewares/resources.exists');
const { handleOfferBody } = require('../middlewares/body.handler');
const moment = require('moment');

router.put(
  '/',
  offerExists,
  handleOfferBody,
  validatesOfferSchema,
  ErrorMiddleware(async function (req, res, next) {
    const offer_body = req.body;
    const studExists = await Studs.studExists(offer_body.stud_id);
    if (studExists === false) {
      logger('PUT /offer', logLevels.warn, 'Inexistant stud_id');
      ResponseError.badRequest(res);
      return;
    }
    if (offer_body.status === 'ACCEPTED') {
      const toUpdateList = await Offers.getOffersByStudId(offer_body.stud_id);
      if (toUpdateList !== null) {
        for (const toUpdate of toUpdateList) {
          if (toUpdate.offer_id === offer_body.offer_id) {
            continue;
          }
          toUpdate.status = 'REJECTED';
          await Offers.updateOffer(toUpdate);
        }
      }
      const stud = await Stud.getStud(offer_body.stud_id);
      const helperEmail = (await User.getUser(offer_body.helper_user_id)).email;
      const seekerEmail = (await User.getUser(stud.stud_owner_id)).email;
      const channel = await discord.createChannel(`${stud.title}`);
      const meeting = {
        offer_id: offer_body.offer_id,
        channel_link: channel.link,
        channel_id: channel.id
      };
      await Meeting.createMeeting(meeting);
      email.sendInvitationMail(helperEmail, channel.link);
      email.sendInvitationMail(seekerEmail, channel.link);
      offer_body.status = 'SCHEDULED';
      stud.status = 'SCHEDULED';
      const modifiedStud = await Stud.updateStud(stud);
      if (modifiedStud === null) {
        ResponseError.internalServerError(res);
        return;
      }
    }
    const updatedOffer = await Offers.updateOffer(offer_body);
    if (updatedOffer === null) {
      ResponseError.internalServerError(res);
      return;
    }
    responseSuccess.success(res, updatedOffer);
  })
);
router.delete(
  '/:offer_id',
  offerExists,
  ErrorMiddleware(async function (req, res, next) {
    const offerId = req.params.offer_id;
    console.log(offerId);

    const offer = await Offers.deleteOffer(offerId);
    if (offer !== null) {
      ResponseSuccess.success(res, offer);
      return;
    }
    ResponseError.internalServerError(res);
  })
);

router.get(
  '/list',
  ErrorMiddleware(async function (req, res, next) {
    let {
      creationTimestamp = moment.utc().toISOString(),
      limit = 50,
      offset = 0,
      status = ''
    } = req.query;
    limit = parseInt(limit);
    offset = parseInt(offset);
    creationTimestamp = moment.utc(creationTimestamp).toISOString();
    if (creationTimestamp == null || isNaN(limit) || isNaN(offset)) {
      ResponseError.badRequest(res);
      return;
    }
    const offers = await Offers.getOfferList(
      creationTimestamp,
      req.user.user_id,
      limit,
      offset,
      status
    );
    responseSuccess.success(res, offers);
  })
);

router.get(
  '/:offer_id',
  offerExists,
  ErrorMiddleware(async function (req, res, next) {
    const offerId = req.params.offer_id;
    console.log(offerId);

    const offer = await Offers.getOffer(offerId);
    if (offer !== null) {
      ResponseSuccess.success(res, offer);
      return;
    }
    ResponseError.notFound(res);
  })
);

router.post(
  '/',
  handleOfferBody,
  validatesOfferSchema,
  ErrorMiddleware(async function (req, res, next) {
    const offer_body = req.body;

    const studExists = await Studs.studExists(offer_body.stud_id);
    if (studExists === false) {
      logger('POST /offer', logLevels.warn, 'Inexistant stud_id');
      ResponseError.badRequest(res);
      return;
    }
    const offer = await Offers.createOffer(offer_body);
    if (offer !== null) {
      responseSuccess.success(res, offer);
      return;
    }
    ResponseError.internalServerError(res);
  })
);

module.exports = router;
