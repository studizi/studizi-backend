/* eslint-disable camelcase */
/* eslint-disable object-curly-spacing */
const express = require('express');
const router = express.Router();
const ResponseSuccess = require('../responses/response.success');
const ErrorMiddleware = require('../middlewares/error.middleware');
const User = require('../database/models/user.model');
const ResponseError = require('../responses/response.error');
const { isAValidInstance } = require('../utils/schema.validator');
const userSchema = require('../database/schemas/user.schema.json');
const passwordSchema = require('../database/schemas/password.schema.json');
const logLevels = require('../log/log.levels.json').levels;
const logger = require('../log/winston.logger');
const { userExists } = require('../middlewares/resources.exists');
const SHA256 = require('js-sha256');
router.get(
  '/:user_id',
  userExists,
  ErrorMiddleware(async (req, res, next) => {
    const user = await User.getUser(req.params.user_id);
    if (user !== null) {
      ResponseSuccess.success(res, user);
      return;
    }
    ResponseError.internalServerError(res);
  })
);

router.get(
  '/:user_id/rating',
  userExists,
  ErrorMiddleware(async (req, res, next) => {
    const rating = await User.getRatedUser(req.params.user_id);
    if (rating !== null) {
      ResponseSuccess.success(res, rating);
      return;
    }
    ResponseError.internalServerError(res);
  })
);
router.delete(
  '/',
  ErrorMiddleware(async (req, res, next) => {
    const deletedUser = await User.deleteUser(req.user.user_id);
    if (deletedUser !== null) {
      ResponseSuccess.success(res, deletedUser);
    }
    ResponseError.internalServerError(res);
  })
);

router.put(
  '/',
  ErrorMiddleware(async (req, res, next) => {
    const user = req.body;
    if (
      user.user_id !== req.user.user_id ||
      !isAValidInstance(user, userSchema)
    ) {
      logger('PUT /api/v1/user/', logLevels.warn, 'Invalid body');
      ResponseError.badRequest(res);
      return;
    }
    const updatedUser = await User.updateUser(user);
    if (updatedUser !== null) {
      ResponseSuccess.success(res, updatedUser);
    } else {
      ResponseError.badRequest(res);
    }
  })
);

router.put(
  '/password',
  ErrorMiddleware(async (req, res, next) => {
    const user = req.user;
    const {old_password, new_password } = req.body;
    if (
      old_password === new_password ||
      !isAValidInstance({old_password, new_password }, passwordSchema)) {
      logger('PUT /api/v1/user/password', logLevels.warn, 'Invalid body');
      ResponseError.badRequest(res);
      return;
    }
    const user_id = await User.getUserIdIfExist(user.email, SHA256(old_password));
    if (user_id === null) {
      ResponseError.forbidden(res);
      return;
    }
    user.password = SHA256(new_password);
    const updatedUser = await User.updateUser(user);
    if (updatedUser !== null) {
      ResponseSuccess.success(res, updatedUser);
    } else {
      ResponseError.badRequest(res);
    }
  })
);

module.exports = router;
