const express = require('express');
const router = express.Router();
const ResponseSuccess = require('../responses/response.success');
const ErrorMiddleware = require('../middlewares/error.middleware');
const Stud = require('../database/models/stud.model');
const Offer = require('../database/models/offer.model');
const ResponseError = require('../responses/response.error');
const { isAValidInstance } = require('../utils/schema.validator');
const studSchema = require('../database/schemas/stud.schema.json');
const logLevels = require('../log/log.levels.json').levels;
const logger = require('../log/winston.logger');
const { studExists } = require('../middlewares/resources.exists');
const moment = require('moment');

router.post(
  '/',
  ErrorMiddleware(async (req, res, next) => {
    const stud = req.body;
    stud.stud_owner_id = req.user.user_id;
    if (!isAValidInstance(stud, studSchema)) {
      logger('POST /api/v1/stud', logLevels.warn, 'Invalid body');
      ResponseError.badRequest(res);
      return;
    }
    if (stud.silver_credit_cost + stud.gold_credit_cost === 0) {
      ResponseError.badRequest(res);
      return;
    }
    const createdStud = await Stud.createStud(stud);
    if (createdStud !== null) {
      ResponseSuccess.success(res, createdStud);
      return;
    }
    ResponseError.internalServerError(res);
  })
);

router.get(
  '/list',
  ErrorMiddleware(async (req, res, next) => {
    let {
      creationTimestamp = moment.utc().toISOString(),
      limit = 50,
      offset = 0,
      tags = [],
      studOwnerId = -1
    } = req.query;
    if (!Array.isArray(tags)) {
      tags = [tags];
    }
    creationTimestamp = moment.utc(creationTimestamp).toISOString();
    if (creationTimestamp == null || isNaN(limit) || isNaN(offset)) {
      ResponseError.badRequest(res);
      return;
    }

    limit = parseInt(limit);
    offset = parseInt(offset);
    if (limit < 0 || offset < 0) {
      ResponseError.badRequest(res);
      return;
    }

    const list = await Stud.getStudList(
      creationTimestamp,
      limit,
      offset,
      tags,
      studOwnerId
    );

    ResponseSuccess.success(
      res,
      list.map(s => {
        s.stud_cost = s.silver_credit_cost + s.gold_credit_cost;
        delete s.silver_credit_cost;
        delete s.gold_credit_cost;
        return s;
      })
    );
  })
);

router.get(
  '/:stud_id',
  studExists,
  ErrorMiddleware(async (req, res, next) => {
    const stud = await Stud.getStud(req.params.stud_id);
    if (stud !== null) {
      ResponseSuccess.success(res, stud);
    } else {
      ResponseError.notFound(res);
    }
  })
);

router.get(
  '/:stud_id/offer',
  studExists,
  ErrorMiddleware(async (req, res, next) => {
    const offers = await Offer.getOffersByStudId(req.params.stud_id);
    if (offers === null || offers.length === 0) {
      ResponseError.notFound(res);
      return;
    }
    const stud = await Stud.getStud(req.params.stud_id);
    if (stud === null) {
      ResponseError.internalServerError(res);
      return;
    }
    if (req.user.user_id !== stud.stud_owner_id) {
      const offer = offers.filter(offer => offer.helper_user_id === req.user.user_id);
      if (offer.length > 0) {
        ResponseSuccess.success(res, offer[0]);
      } else {
        ResponseError.notFound(res);
      }
    } else {
      ResponseSuccess.success(res, offers);
    }
  })
);

router.delete(
  '/:stud_id',
  studExists,
  ErrorMiddleware(async (req, res, next) => {
    const stud = await Stud.getStud(req.params.stud_id);
    if (stud.stud_owner_id !== req.user.user_id) {
      ResponseError.notFound(res);
      return;
    }
    const deletedStud = await Stud.deleteStud(req.params.stud_id);
    if (deletedStud !== null) {
      ResponseSuccess.success(res, deletedStud);
    } else {
      ResponseError.notFound(res);
    }
  })
);

router.put(
  '/:stud_id',
  studExists,
  ErrorMiddleware(async (req, res, next) => {
    const stud = req.body;
    stud.stud_owner_id = req.user.user_id;
    const gettedStud = await Stud.getStud(req.params.stud_id);
    if (gettedStud.stud_owner_id !== req.user.user_id) {
      ResponseError.notFound(res);
      return;
    }
    if (!isAValidInstance(stud, studSchema)) {
      logger('PUT /api/v1/stud/:stud_id', logLevels.warn, 'Invalid body');
      ResponseError.badRequest(res);
      return;
    }
    if (stud.silver_credit_cost + stud.gold_credit_cost === 0) {
      ResponseError.badRequest(res);
      return;
    }
    const updatedStud = await Stud.updateStud(stud);
    if (updatedStud !== null) {
      ResponseSuccess.success(res, updatedStud);
      return;
    }
    ResponseError.internalServerError(res);
  })
);

module.exports = router;
