## ROUTE TABLES
resource "aws_route_table" "studizi_public_rt" {
  vpc_id = aws_vpc.studizi_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.studizi_igw.id
  }

  tags = {
    Name = "studizi-${var.env}-public-rt"
  }
}

## ASSOCIATIONS
resource "aws_route_table_association" "studizi_rt_association_public" {
  subnet_id      = aws_subnet.studizi_public_subnet.id
  route_table_id = aws_route_table.studizi_public_rt.id
}
