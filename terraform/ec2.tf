data "aws_ami" "studizi_ami" {
  most_recent = true
  filter {
    name   = "tag:Name"
    values = ["studizi-${var.env}-ami"]
  }
  owners = ["self"]
}

data "aws_eip" "studizi-ip-address" {
  id = "eipalloc-088474d710e446fa7"
}

resource "aws_instance" "studizi-ec2" {
  ami                    = data.aws_ami.studizi_ami.id
  instance_type          = "t2.medium"
  key_name               = "studizi_key"
  vpc_security_group_ids = [aws_security_group.studizi_sg.id]
  subnet_id              = aws_subnet.studizi_public_subnet.id
  tags = {
    Name = "studizi-${var.env}-app"
  }
  user_data            = <<-EOF
        #!/bin/bash
        cd /home/ubuntu/studizi-backend/
        if [ -f .env ]; then
          export $(echo $(cat .env | sed 's/#.*//g'| xargs) | envsubst)
        fi
        AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY aws s3 cp s3://studizi/certificates/fullchain.pem ./certificates/
        AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY aws s3 cp s3://studizi/certificates/privkey.pem ./certificates/
        AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY aws s3 cp s3://studizi/certificates/options-ssl-nginx.conf ./certificates/
        AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY aws s3 cp s3://studizi/certificates/ssl-dhparams.pem ./certificates/
        sudo service nginx start
        sudo rm -rf /etc/nginx/sites-available/default
        sudo cp default /etc/nginx/sites-available/default
        sudo service nginx restart
        git checkout PLACE_HOLDER_BRANCH_NAME
        sudo docker run -d --name postgres -v "$PWD/server/database/scripts/init.sql":/docker-entrypoint-initdb.d/init.sql -p $POSTGRES_PORT:5432 -e POSTGRES_USER=$POSTGRES_USER -e POSTGRES_PASSWORD=$POSTGRES_PASSWORD -e POSTGRES_DB=$POSTGRES_DB -d postgres:13.1-alpine
        pm2 start
  EOF
}

resource "aws_eip_association" "eip_assoc" {
  instance_id   = aws_instance.studizi-ec2.id
  allocation_id = data.aws_eip.studizi-ip-address.id
}

output "studizi_instance_ip_address" {
  value = data.aws_eip.studizi-ip-address.public_ip
}
