variable "env" {
  type    = string
  default = "dev"
}

variable "availability_zone" {
  type    = string
  default = "eu-west-1a"
}

variable "region" {
  type    = string
  default = "eu-west-1"
}

variable "access_key" {
  type = string
}

variable "secret_key" {
  type = string
}
