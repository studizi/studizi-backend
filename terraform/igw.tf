resource "aws_internet_gateway" "studizi_igw" {
  vpc_id = aws_vpc.studizi_vpc.id

  tags = {
    Name = "studizi-${var.env}-igw"
  }
}
