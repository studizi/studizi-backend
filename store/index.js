export const state = () => ({
  formTitle: null,
  loading: false,
  userId: null
});

export const mutations = {
  setFormTitle (state, title) {
    state.formTitle = title;
  },
  setLoading (state, loading) {
    state.loading = loading;
  },
  setUserId (state, userId) {
    state.userId = userId;
  }
};

export const getters = {
  getFormTitle (state) {
    return state.formTitle;
  },
  getLoading (state) {
    return state.loading;
  },
  getUserId (state) {
    return state.userId;
  }
};
