image: registry.gitlab.com/gitlab-org/terraform-images/stable:latest
variables:
  TF_ROOT: ${CI_PROJECT_DIR}/terraform
  TF_ADDRESS: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/${CI_PROJECT_NAME}
  TF_VAR_region: ${AWS_DEFAULT_REGION}
  TF_VAR_access_key: ${AWS_ACCESS_KEY_ID}
  TF_VAR_secret_key: ${AWS_SECRET_ACCESS_KEY}
  GIT_DEPTH: 0

cache:
  key: studizi-app
  paths:
    - ${TF_ROOT}/.terraform

before_script:
  - cd ${TF_ROOT}

stages:
  - test
  - build
  - validate
  - prepare
  - deploy
  - destroy

# RUNNER TAGS
.runner_tags: &runner_tags
  tags:
    - gitlab-org

# ENV
.env_test: &env_test
    variables:
      ENV: test
      TF_VAR_env: test

.env_dev: &env_dev
  variables:
    ENV: dev
    TF_VAR_env: dev

.env_prod: &env_prod
  variables:
    ENV: prod
    TF_VAR_env: prod
    CHECKOUT_BRANCH: master

# SCRIPTS
.script_validate: &script_validate
  environment:
    name: "${ENV}"
  script:
    - gitlab-terraform init
    - gitlab-terraform validate

.script_plan: &script_plan
  environment:
    name: "${ENV}"
  script:
    - sed -i "s|PLACE_HOLDER_BRANCH_NAME|${CI_COMMIT_BRANCH}|g" ec2.tf
    - gitlab-terraform plan
    - gitlab-terraform plan-json
  artifacts:
    name: plan
    paths:
      - ${TF_ROOT}/plan.cache
    reports:
      terraform: ${TF_ROOT}/plan.json

.script_apply: &script_apply
  environment:
    name: "${ENV}"
  script:
    - gitlab-terraform apply

.script_destroy: &script_destroy
  environment:
    name: "${ENV}"
  script:
    - gitlab-terraform destroy

.script_build: &script_build
  environment:
    name: "${ENV}"
  script:
    - cd ${CI_PROJECT_DIR}
    - packer build build.json

integration-test:
  stage: test
  image: node:15.10.0-alpine3.12
  environment:
    name: test
  services:
    - postgres:13.1-alpine
  variables:
    POSTGRES_DB: ${POSTGRES_DB}
    POSTGRES_PASSWORD: ${POSTGRES_PASSWORD}
    POSTGRES_USER: ${POSTGRES_USER}
    POSTGRES_HOST_AUTH_METHOD: trust
  script:
    - cd ${CI_PROJECT_DIR}
    - sed -i "s/--reporter spec/--reporter mocha-junit-reporter/g" package.json
    - npm i
    - npm test
  artifacts:
    when: always
    paths:
      - coverage/
    reports:
      junit: ./test-results.xml
  <<: *runner_tags

# BUILD
build:dev:
  stage: build
  image:
    name: hashicorp/packer
    entrypoint:
      - "/usr/bin/env"
      - "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
  except:
    - master
  when: manual
  <<: *env_dev
  <<: *script_build
  <<: *runner_tags

build:prod:
  stage: build
  image:
    name: hashicorp/packer
    entrypoint:
      - "/usr/bin/env"
      - "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
  only:
    refs:
      - master
  when: manual
  <<: *env_prod
  <<: *script_build
  <<: *runner_tags


# VALIDATE
validate:dev:
  stage: validate
  except:
    - master
  when: manual
  <<: *env_dev
  <<: *script_validate
  <<: *runner_tags

validate:prod:
  stage: validate
  only:
    - master
  when: manual
  <<: *env_prod
  <<: *script_validate
  <<: *runner_tags

# PLAN
plan:dev:
  stage: prepare
  dependencies:
    - validate:dev
  except:
    - master
  when: manual
  <<: *env_dev
  <<: *script_plan
  <<: *runner_tags

plan:prod:
  stage: prepare
  dependencies:
    - validate:prod
  only:
    - master
  when: manual
  <<: *env_prod
  <<: *script_plan
  <<: *runner_tags
# APPLY
apply:dev:
  stage: deploy
  dependencies:
    - plan:dev
  except:
    - master
  when: manual
  <<: *env_dev
  <<: *script_apply
  <<: *runner_tags

apply:prod:
  stage: deploy
  dependencies:
    - plan:prod
  only:
    - master
  when: manual
  <<: *env_prod
  <<: *script_apply
  <<: *runner_tags

# DESTROY
destroy:dev:
  stage: destroy
  dependencies:
    - apply:dev
  except:
    - master
  when: manual
  <<: *env_dev
  <<: *script_destroy
  <<: *runner_tags

destroy:prod:
  stage: destroy
  dependencies:
    - apply:prod
  only:
    - master
  when: manual
  <<: *env_prod
  <<: *script_destroy
  <<: *runner_tags
