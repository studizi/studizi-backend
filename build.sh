#! /bin/sh
echo '----------------------Installing docker...----------------------'
sudo apt-get update
sudo apt-get -y install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get install unzip
sudo apt-get -y install docker-ce docker-ce-cli containerd.io
sudo gpasswd -a $USER docker

echo '----------------------Installing AWS-CLI...----------------------'
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install

echo '----------------------Installing NodeJS...----------------------'
wget -qO- https://deb.nodesource.com/setup_15.x | sudo -E bash -
sudo apt install -y nodejs

echo '----------------------Installing Nginx...----------------------'
sudo apt update
sudo apt install -y nginx

echo '----------------------Installing pm2 dependency to launch front and back as processes----------------------'
sudo npm install -g pm2
sudo npm install -g serve

echo '----------------------Cloning the repo...----------------------'
git clone https://oauth:$GITLAB_TOKEN@gitlab.com/studizi/studizi-backend.git
cd studizi-backend
git checkout $CHECKOUT_BRANCH

echo '----------------------Installing node dependencies...----------------------'
npm install
npm run build

echo '----------------------Building env file...----------------------'
echo "BOT_TOKEN=$BOT_TOKEN\n\
EMAIL_ADDRESS=$EMAIL_ADDRESS\n\
EMAIL_PASSWORD=$EMAIL_PASSWORD\n\
ENV=PROD\n\
AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID\n\
AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY\n\
GUILD_ID=$GUILD_ID\n\
POSTGRES_DB=$POSTGRES_DB\n\
POSTGRES_HOST=$POSTGRES_HOST\n\
POSTGRES_PASSWORD=$POSTGRES_PASSWORD\n\
POSTGRES_PORT=$POSTGRES_PORT\n\
POSTGRES_USER=$POSTGRES_USER\n\
APPLICATION_URL=https://www.studizi.ninja\n" > .env
