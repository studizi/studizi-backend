# studizi
## _No More Late Nights_
[![NodeJS Version][node-image]](https://nodejs.org/download/release/v15.6.0/)
[![NPM Version][npm-image]](https://www.npmjs.com/package/npm/v/7.5.3)
[![Docker Version][docker-image]](https://www.docker.com/get-started)
[![Postgres Version][psql-image]](https://www.postgresql.org/download/)
![Discord member][discord-image]

## Why Studizi ?

Due to the CORONA Virus crisis, the world has been forced to move on. Digitalization & remote working set themselves in fields that do not favor such measures. Usually, face-to-face interactions were preferred. Studies is one of them. As engineer apprentices, we witnessed the metamorphosis of our classes. Both schools and students have been facing huge difficulties since the beginning of the health crisis. Despite using telecommunication tools such as Teams or Zoom, schools cannot maintain the “normal” type of relationship they once had, and students are feeling less and less inclined to continue to work from home. 

The link of this platform is [here](https://studizi.ninja) !
___

## Concept

`Studizi` is platform which helps students to interact along courses comprehension & assignments completion.

- Create Stud
- Become helper or seeker
- Discord support
- Earn some prizes

___
## MVP & MMFs
### Account, Security & Permissions

User should be able to log into the app. It implies that the user can create an account with `Passport.js`. It is our middleware responsible for authentication in our Express application. It handles the session cookie creation and request authentication. It is based on a model called ‘Strategy’ (Username & Password, OAuth...).

### Studs, Interactions & Meetings
The user can ask for some questions called `Stud`. Studs can be posted using virtual currency. The Stud owner is called a `seeker`. The seeker provides timespan during which he is free to have a meeting. Another user can propose its help by picking a meeting time (inside the timeranges proposed by the seeker). Once its offer has been accepted, it becomes an `helper`. Both helper & seeker are granted an access to `A Discord channel`, automatically created for the meeting duration.
This channel is private to the seeker & the helper.
Once the meeting is over, they can `rate` each other. The helper is `rewarded` by some virtual currency.
___

## Enhancements
- ### Refactoring
    - We were a bit short on time. We can simplify some logic and clean up our code.
- ### Architecture
    - We didn't get the AWS credits on time, we couldn't   implement what we wanted to do in the beginning, such as elastic load balancing.
- ### Mobile version
    - Our website is not very user friendly for those who are on smartphone... Also it could be improved in term of responsiveness.
- ### Administrator interface
    - For the moment, administration is a bit of a handy-work. For an MVP this is fine but if we need to public, we should have a better administration panel.
- ### Dashboarding
    - We would like to provide the user with some reports on his activity.
- ### Online shop
    - We don't have any sponsors so our shop is a bit empty... 😭
___
## How to make it run?

### Environment variables
You can put all these variables inside a .env file at the root of the project.

- ```BOT_TOKEN```: Token of the Discord's bot

- ```EMAIL_ADDRESS```: Email to use for notifications

- ```EMAIL_PASSWORD```: Email password

- ```GUILD_ID```: Discord server ID

- ```POSTGRES_DB=stdz```: Postgres database

- ```POSTGRES_HOST```: Postgres hostname

- ```POSTGRES_PASSWORD```: Postgres password

- ```POSTGRES_PORT```: Postgres port

- ```POSTGRES_USER```: Postgres user

- ```AWS_ACCESS_KEY_ID```: AWS Access Key

- ```AWS_SECRET_ACCESS_KEY```: AWS Secret Key


### Mount Postgres Database
```sh
docker run -d -p 5432:5432 -e POSTGRES_DB=$POSTGRES_DB -e POSTGRES_USER=$POSTGRES_USER -e POSTGRES_PASSWORD=$POSTGRES_PASSWORD -v $(pwd)/server/database/scripts/init.sql:/docker-entrypoint-initdb.d/init.sql --name postgres postgres:13.1-alpine
```
> Note that this command executes a starting script to create schema and tables.

### Launch the server
To install all dependencies :
```sh
$ npm install
```
To launch tests :
```sh
$ npm t
```

This will launch a server with hot reload at localhost:3000 :
```sh
$ npm run dev
```

Build for production and launch server :
```sh
$ npm run build
$ npm run start
```

Generate static project :
```sh
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

___
## Contributors

###  Mohammad Baraa AL BAKRI <mohammad.baraa.al.bakri@efrei.net> 
<img src="https://cdn4.iconfinder.com/data/icons/miu-flat-social/60/github-512.png" width=25> [@Baraa-ALBAKRI](https://github.com/Baraa-ALBAKRI) 
 
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/GitLab_Logo.svg/1108px-GitLab_Logo.svg.png" width=25> [@Baraa-](https://gitlab.com/Baraa-) 


###  Larry ANDRIAMAMPIANINA <larry.andriamampianina@efrei.net> 
<img src="https://cdn4.iconfinder.com/data/icons/miu-flat-social/60/github-512.png" width=25> [@larryandria](https://github.com/larryandria) 
 
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/GitLab_Logo.svg/1108px-GitLab_Logo.svg.png" width=25> [@larry.andria](https://gitlab.com/larry.andria) 

 
###  Nicolas BENCHIMOL <nicolas.benchimol@efrei.net>
<img src="https://cdn4.iconfinder.com/data/icons/miu-flat-social/60/github-512.png" width=25> [@Bechamelle](https://github.com/Bechamelle) 
 
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/GitLab_Logo.svg/1108px-GitLab_Logo.svg.png" width=25> [@Bechamelle](https://gitlab.com/Bechamelle) 

 
###  Alexandre BEZAIN <alexandre.bezain@efrei.net>
<img src="https://cdn4.iconfinder.com/data/icons/miu-flat-social/60/github-512.png" width=25> [@AlexandreBezain](https://github.com/AlexandreBezain) 
 
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/GitLab_Logo.svg/1108px-GitLab_Logo.svg.png" width=25> [@alexbezain](https://gitlab.com/alexbezain) 

###  Fares KISSOUM <fares.kissoum@efrei.net>
<img src="https://cdn4.iconfinder.com/data/icons/miu-flat-social/60/github-512.png" width=25> [@FaresKi](https://github.com/FaresKi) 
 
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/GitLab_Logo.svg/1108px-GitLab_Logo.svg.png" width=25> [@FaresKi](https://gitlab.com/FaresKi) 


[npm-image]: https://badgen.net/badge/icon/npm?icon=npm&label
[node-image]: https://img.shields.io/badge/NodeJS-15.6%2B-orange
[docker-image]: https://badgen.net/badge/icon/docker?icon=docker&label
[psql-image]: https://badgen.net/badge/icon/postgresql?icon=postgresql&label
[discord-image]: https://badgen.net/discord/members/PJtCPMed