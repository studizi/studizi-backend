# Studizi

# March 9th, 2021 - v1.0.0

## New features

-   Sign up and start asking questions. New users will be provided with free credit. 🆓
-   Manage your profile. We are impatient to know you 😉
-   Meet up on Discord and solve your problems. Simplify tutoring and conversations! 💬 🔊
-   Get credits from the answers that YOU provide! 🤑

## Bugs fixed

We continuously improve our code quality, check our [Gitlab ](https://gitlab.com/studizi/studizi-backend/-/tree/master)Issues to track our progression. 🐞

## What's next

-   More partnerships with different companies to provide you with a large panel of goodies. 🎉
-   Improve our UI. We want you to feel at home on studizi.ninja 🏡
-   Boost the website's performances by enhancing our infrastructure. You know, if we have time we can make wonders 🧙

          