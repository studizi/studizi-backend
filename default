map $sent_http_content_type $expires {
    "text/html"                 epoch;
    "text/html; charset=utf-8"  epoch;
    default                     off;
}

server {
    listen          80;             # the port nginx is listening on
    listen [::]:80 default_server;

    gzip            on;
    gzip_types      text/plain application/xml text/css application/javascript;
    gzip_min_length 1000;

    location / {
        expires $expires;

        proxy_redirect                      off;
        proxy_set_header Host               $host;
        proxy_set_header X-Real-IP          $remote_addr;
        proxy_set_header X-Forwarded-For    $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto  $scheme;
        proxy_read_timeout          1m;
        proxy_connect_timeout       1m;
        proxy_pass                          http://127.0.0.1:3000; # set the address of the Node.js instance here
    }
}

server {
    if ($host ~ ^(?!www\.)(?<domain>.+)$) {
         return 301 https://www.$domain$request_uri;
    }

    if ($scheme ~ ^http$) {
        return 301 https://www.studizi.ninja$request_uri;
    }

    listen          80;             # the port nginx is listening on
    listen [::]:80 ;

    gzip            on;
    gzip_types      text/plain application/xml text/css application/javascript;
    gzip_min_length 1000;

    location / {
        expires $expires;

        proxy_redirect                      off;
        proxy_set_header Host               $host;
        proxy_set_header X-Real-IP          $remote_addr;
        proxy_set_header X-Forwarded-For    $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto  $scheme;
        proxy_read_timeout          1m;
        proxy_connect_timeout       1m;
        proxy_pass                          http://127.0.0.1:3000; # set the address of the Node.js instance here
    }

    server_name www.studizi.ninja studizi.ninja; # managed by Certbot

    listen [::]:443 ssl ipv6only=on; # managed by Certbot
    listen 443 ssl; # managed by Certbot
    ssl_certificate /home/ubuntu/studizi-backend/certificates/fullchain.pem; # managed by Certbot
    ssl_certificate_key /home/ubuntu/studizi-backend/certificates/privkey.pem; # managed by Certbot
    include /home/ubuntu/studizi-backend/certificates/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /home/ubuntu/studizi-backend/certificates/ssl-dhparams.pem; # managed by Certbot



}